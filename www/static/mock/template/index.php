<?php
header('Content-Type: application/json');


function casebook1(){
?>
{
	"casebook_id":1,
	"name":"Example Casebook 1",
	"categories":	[
						{
							"category_ID":1,
							"casebook_ID":1,
							"name":"Patient Information",
							"parent_ID":null,
							"order":1,
							"active":true,
							"displayType":"G",
							"notes":null,
							
							"categories":	[
							
											],
							
							"questions":	[
												{
													"question_ID":1,
													"category_ID":1,
													"order":1,
													"active":true,
													"type":"1",
													"text":"Child's pseudonym",
													"notes":null
												
													
												},
												{
													"question_ID":2,
													"category_ID":1,
													"order":2,
													"active":true,
													"type":"1",
													"text":"Age",
													"notes":null
												
													
												},
												{
													"question_ID":3,
													"category_ID":1,
													"order":3,
													"active":true,
													"type":"1",
													"text":"DOB",
													"notes":null
												
													
												}
											]
						},
						{
							"category_ID":2,
							"casebook_ID":1,
							"name":"Patient Information",
							"parent_ID":1,
							"order":1,
							"active":true,
							"displayType":"G",
							"notes":null,
							
							"categories":	[
							
												{
													"category_ID":3,
													"casebook_ID":1,
													"name":"Family members",
													"parent_ID":2,
													"order":2,
													"active":true,
													"displayType":"G",
													"notes":null,
							
													"categories":	[
																		{
																			"category_ID":4,
																			"casebook_ID":1,
																			"name":"Siblings and Ages",
																			"parent_ID":3,
																			"order":1,
																			"active":true,
																			"displayType":"T",
																			"notes":null,
							
																			"categories":	[
																		
							
																							],
							
																			"questions":	[
																			
																								{
																									"question_ID":5,
																									"category_ID":4,
																									"order":1,
																									"active":true,
																									"type":"1",
																									"text":"Name",
																									"notes":"(false names)"
												
																									
																								},
																								{
																									"question_ID":6,
																									"category_ID":4,
																									"order":2,
																									"active":true,
																									"type":"1",
																									"text":"Age",
																									"notes":"(false names)"
												
																									
																								}
																			
																							]
																		},
																		{
																			"category_ID":5,
																			"casebook_ID":1,
																			"name":"Adults in the household",
																			"parent_ID":3,
																			"order":2,
																			"active":true,
																			"displayType":"T",
																			"notes":null,
							
																			"categories":	[
																		
							
																							],
							
																			"questions":	[
																			
																								{
																									"question_ID":7,
																									"category_ID":5,
																									"order":1,
																									"active":true,
																									"type":"1",
																									"text":"Name",
																									"notes":null
												
																									
																								},
																								{
																									"question_ID":8,
																									"category_ID":5,
																									"order":2,
																									"active":true,
																									"type":"1",
																									"text":"Relationship",
																									"notes":null
												
																									
																								},
																								{
																									"question_ID":10,
																									"category_ID":5,
																									"order":3,
																									"active":true,
																									"type":"1",
																									"text":"Occupation",
																									"notes":null
												
																									
																								},
																								{
																									"question_ID":9,
																									"category_ID":5,
																									"order":4,
																									"active":true,
																									"type":"1",
																									"text":"Notes",
																									"notes":null
												
																									
																								}
																			
																							]
																		}
							
																	],
							
													"questions":	[
																		
																		
													
																	]
												}
							
											],
							
							"questions":	[
												{
													"question_ID":4,
													"category_ID":2,
													"order":1,
													"active":true,
													"text":"Brief summary of condition or disability",
													"notes":null
												
													
												}
												
												
											]
						}

					]
}
<?php
}


function head(){
?>
{
	"result":"OK",
	"data":
<?php
}

function tail(){
?>
}
<?php
}


if($_SERVER["REQUEST_METHOD"]=="GET"){
	$casebook = $_REQUEST["casebook_id"];
	if($casebook==null){
	?>
	{
		"result": "OK",
		"pageSize":10,
		"page":1,
		"total":1,
		"count":1,
		"data":[

	<?php
	//head();
	casebook1();
	//tail();
	?>

	]}
	<?php
	}else if($casebook==1){
		casebook1();
	}else if($casebook<1 || $casebook>2){
	?>
	{
		"result": "FAILED",
		"error": "Casebook not found."
	}
	<?php
	}
}else{
header('Content-Type: application/json');
?>
{
	"result":"OK",
	"data":
<?php

echo file_get_contents("php://input");

?>
}
<?php
}
?>
