<?php
header('Content-Type: application/json');


function user1(){
?>
{
	"user_id":1,
	"privilegeLevel":"S",
	"userName":"billyBob",
	"firstName":"Billy",
	"lastName":"Bob",
	"email":"billy@bob.com",
	"phone":"(123)456-7890",
	"dateLastLogin":"2015-01-20T03:04:49Z"
}
<?php
}

function user2(){
?>
{
	"user_id":2,
	"privilegeLevel":"I",
	"userName":"samwise",
	"firstName":"Samwise",
	"lastName":"Gamgee",
	"email":"samwise@gamgee.com",
	"phone":"(123)456-7890",
	"dateLastLogin":"2015-01-20T03:04:49Z"
}
<?php
}


function head(){
?>
{
	"result":"OK",
	"data":
<?php
}

function tail(){
?>
}
<?php
}


if($_SERVER["REQUEST_METHOD"]=="GET"){
	$user_id = $_REQUEST["user_id"];
	if($user_id==null){
	?>
	{
		"result": "OK",
		"pageSize":10,
		"page":1,
		"total":1,
		"count":1,
		"data":[

	<?php
	user1();
	?>
	,
	<?php
	user2();
	?>
	]}
	<?php
	}else if($user_id==1){
		head();
		user1();
		tail();
	}else if($user_id==2){
		head();
		user2();
		tail();
	}else if($user_id<1 || $user_id>2){
	?>
	{
		"result": "FAILED",
		"error": "Casebook not found."
	}
	<?php
	}
}else{
header('Content-Type: application/json');
?>
{
	"result":"OK",
	"data":
<?php

echo file_get_contents("php://input");

?>
}
<?php
}
?>
