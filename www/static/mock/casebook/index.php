<?php
header('Content-Type: application/json');


function casebook1(){
?>
{
	"userCasebook_id":1,
	"casebook_id":1,
	"user_id":1,
	"dateStarted":"2015-01-05T04:51:39Z",
	"dueDate":"2015-03-05T11:59:59Z",
	"dateSubmitted":null,
	"finalgrade":null,
	"casebook":	{
					"casebook_id":1,
					"name":"Example Casebook 1",
					"categories":	[
										{
											"category_ID":1,
											"casebook_ID":1,
											"name":"Patient Information",
											"parent_ID":null,
											"order":2,
											"active":true,
											"type":"G",
											"notes":null,
											
											"categories":	[
											
															],
											
											"questions":	[
																{
																	"question_ID":1,
																	"category_ID":1,
																	"order":2,
																	"active":true,
																	"type":"1",
																	"text":"Child's pseudonym",
																	"notes":null,
																
																	"answers": [
																					{
																						"answer_id":1,
																						"userCasebook_id":1,
																						"question_id":1,
																						"text":"m.",
																						"feedback":null
																					}
																
																				]
																},
                                                                                                                                {
																	"question_ID":1000,
																	"category_ID":1,
																	"order":4,
																	"active":true,
																	"type":"M",
																	"text":"Current Prescriptions",
																	"notes":null,
																
																	"answers": [
																					{
																						"answer_id":1,
																						"userCasebook_id":1,
																						"question_id":1,
																						"text":"Tylenol",
                                                                                                                                                                                "feedback":""
																					}, 
                                                                                                                                                                        {
																						"answer_id":1000,
																						"userCasebook_id":1,
																						"question_id":1000,
																						"text":"Excedrin",
																						"feedback":"Hello"
																					} 
																
																				]
																},
																{
																	"question_ID":2,
																	"category_ID":1,
																	"order":3,
																	"active":true,
																	"type":"1",
																	"text":"Age",
																	"notes":null,
																
																	"answers": [
																					{
																						"answer_id":2,
																						"userCasebook_id":1,
																						"question_id":2,
																						"text":"3",
																						"feedback":"A"
																					}
																
																				]
																},
																{
																	"question_ID":3,
																	"category_ID":1,
																	"order":1,
																	"active":true,
																	"type":"1",
																	"text":"DOB",
																	"notes":null,
																
																	"answers": [
																					{
																						"answer_id":3,
																						"userCasebook_id":1,
																						"question_id":3,
																						"text":"23/02/2011",
																						"feedback":"B"
																					}
																
																				]
																}
															]
										},
										{
											"category_ID":2,
											"casebook_ID":1,
											"name":"Family Information",
											"parent_ID":1,
											"order":1,
											"active":true,
											"type":"G",
											"notes":null,
											
											"categories":	[
											
																{
																	"category_ID":3,
																	"casebook_ID":1,
																	"name":"Family members",
																	"parent_ID":2,
																	"order":2,
																	"active":true,
																	"type":"G",
																	"notes":null,
											
																	"categories":	[
																						{
																							"category_ID":4,
																							"casebook_ID":1,
																							"name":"Siblings and Ages",
																							"parent_ID":3,
																							"order":1,
																							"active":true,
																							"type":"T",
																							"notes":null,
											
																							"categories":	[
																						
											
																											],
											
																							"questions":	[
																							
																												{
																													"question_ID":5,
																													"category_ID":4,
																													"order":1,
																													"active":true,
																													"type":"1",
																													"text":"Name",
																													"notes":"(false names)",
																
																													"answers": [
																																	{
																																		"answer_id":4,
																																		"userCasebook_id":1,
																																		"question_id":5,
																																		"text":"Only Child",
																																		"feedback":"no"
																																	}
																
																																]
																												},
																												{
																													"question_ID":6,
																													"category_ID":4,
																													"order":2,
																													"active":true,
																													"type":"1",
																													"text":"Age",
																													"notes":"(false names)",
																
																													"answers": [
																																	{
																																		"answer_id":5,
																																		"userCasebook_id":1,
																																		"question_id":6,
																																		"text":"",
																																		"feedback":"Help me help me the monkeys are eating me. zing zong walla walla bing bong."
																																	}
																
																																]
																												}
																							
																											]
																						},
																						{
																							"category_ID":5,
																							"casebook_ID":1,
																							"name":"Adults in the household",
																							"parent_ID":3,
																							"order":2,
																							"active":true,
																							"type":"T",
																							"notes":null,
											
																							"categories":	[
																						
											
																											],
											
																							"questions":	[
																							
																												{
																													"question_ID":7,
																													"category_ID":5,
																													"order":1,
																													"active":true,
																													"type":"1",
																													"text":"Name",
																													"notes":null,
																
																													"answers": [
																																	{
																																		"answer_id":60,
																																		"userCasebook_id":1,
																																		"question_id":7,
																																		"text":"Anny",
																																		"feedback":"Walking"
																																	},
																																	{
																																		"answer_id":61,
																																		"userCasebook_id":1,
																																		"question_id":7,
																																		"text":"Fred",
																																		"feedback":"Running"
																																	}
																
																																]
																												},
																												{
																													"question_ID":8,
																													"category_ID":5,
																													"order":2,
																													"active":true,
																													"type":"1",
																													"text":"Relationship",
																													"notes":null,
																
																													"answers": [
																																	{
																																		"answer_id":62,
																																		"userCasebook_id":1,
																																		"question_id":8,
																																		"text":"Mother",
																																		"feedback":"Yikes!"
																																	},
																																	{
																																		"answer_id":63,
																																		"userCasebook_id":1,
																																		"question_id":8,
																																		"text":"Father",
																																		"feedback":"Doubtful"
																																	}
																
																																]
																												},
																												{
																													"question_ID":10,
																													"category_ID":5,
																													"order":3,
																													"active":true,
																													"type":"1",
																													"text":"Occupation",
																													"notes":null,
																
																													"answers": [
																																	{
																																		"answer_id":66,
																																		"userCasebook_id":1,
																																		"question_id":9,
																																		"text":"Stay at home mum",
																																		"feedback":"Goodbye"
																																	},
																																	{
																																		"answer_id":67,
																																		"userCasebook_id":1,
																																		"question_id":9,
																																		"text":"Farmer",
																																		"feedback":"Well done sir"
																																	}
																
																																]
																												},
																												{
																													"question_ID":9,
																													"category_ID":5,
																													"order":4,
																													"active":true,
																													"type":"1",
																													"text":"Notes",
																													"notes":null,
																
																													"answers": [
																																	{
																																		"answer_id":64,
																																		"userCasebook_id":1,
																																		"question_id":9,
																																		"text":"First year student at Otago",
																																		"feedback":"You are the man"
																																	},
																																	{
																																		"answer_id":65,
																																		"userCasebook_id":1,
																																		"question_id":9,
																																		"text":"",
																																		"feedback":"No, YOU are the man"
																																	}
																
																																]
																												}
																							
																											]
																						}
											
																					],
											
																	"questions":	[
																						
																						
																	
																					]
																}
											
															],
											
											"questions":	[
																{
																	"question_ID":4,
																	"category_ID":2,
																	"order":1,
																	"active":true,
																	"text":"Brief summary of condition or disability",
																	"notes":null,
																
																	"answers": [
																					{
																						"answer_id":1,
																						"userCasebook_id":1,
																						"question_id":1,
																						"text":"Spina bifida level L3-l5 with myelomeningocoele. Closed 24/02/2011. Primary problem is limited walking mobility and lack of sensation in feet.",
																						"feedback":null
																					}
																
																				]
																}
																
																
															]
										}
				
									]
				},
	"user":	{
				"user_id":1,
				"privilegeLevel":"S",
				"userName":"billyBob",
				"firstName":"Billy",
				"lastName":"Bob",
				"email":"billy@bob.com",
				"phone":"(123)456-7890",
				"dateLastLogin":"2015-01-20T03:04:49Z"
			}
}
<?php
}


function casebook2(){
?>
{
	"userCasebook_id":2,
	"casebook_id":1,
	"user_id":34,
	"dateStarted":"2014-01-05T04:51:39Z",
	"dueDate":"2014-03-05T11:59:59Z",
	"dateSubmitted":"2014-03-03T03:03:03Z",
	"finalgrade":"F",
	"casebook":	{
					"casebook_id":1,
					"name":"Example Casebook 1",
					"categories":	[
										{
											"category_ID":1,
											"casebook_ID":1,
											"name":"Patient Information",
											"parent_ID":null,
											"order":1,
											"active":true,
											"type":"G",
											"notes":null,
											
											"categories":	[
											
															],
											
											"questions":	[
																{
																	"question_ID":1,
																	"category_ID":1,
																	"order":1,
																	"active":true,
																	"type":"1",
																	"text":"Child's pseudonym",
																	"notes":null,
																
																	"answers": [
																					{
																						"answer_id":21,
																						"userCasebook_id":2,
																						"question_id":1,
																						"text":"Z Dog",
																						"feedback":null
																					}
																
																				]
																},
																{
																	"question_ID":2,
																	"category_ID":1,
																	"order":2,
																	"active":true,
																	"type":"1",
																	"text":"Age",
																	"notes":null,
																
																	"answers": [
																					{
																						"answer_id":22,
																						"userCasebook_id":2,
																						"question_id":2,
																						"text":"14",
																						"feedback":null
																					}
																
																				]
																},
																{
																	"question_ID":3,
																	"category_ID":1,
																	"order":3,
																	"active":true,
																	"type":"1",
																	"text":"DOB",
																	"notes":null,
																
																	"answers": [
																					{
																						"answer_id":23,
																						"userCasebook_id":2,
																						"question_id":3,
																						"text":"23/02/2001",
																						"feedback":null
																					}
																
																				]
																}
															]
										},
										{
											"category_ID":2,
											"casebook_ID":1,
											"name":"Patient Information",
											"parent_ID":1,
											"order":1,
											"active":true,
											"type":"G",
											"notes":null,
											
											"categories":	[
											
																{
																	"category_ID":3,
																	"casebook_ID":1,
																	"name":"Family members",
																	"parent_ID":2,
																	"order":2,
																	"active":true,
																	"type":"G",
																	"notes":null,
											
																	"categories":	[
																						{
																							"category_ID":4,
																							"casebook_ID":1,
																							"name":"Siblings and Ages",
																							"parent_ID":3,
																							"order":1,
																							"active":true,
																							"type":"T",
																							"notes":null,
											
																							"categories":	[
																						
											
																											],
											
																							"questions":	[
																							
																												{
																													"question_ID":5,
																													"category_ID":4,
																													"order":1,
																													"active":true,
																													"type":"1",
																													"text":"Name",
																													"notes":"(false names)",
																
																													"answers": [
																																	{
																																		"answer_id":24,
																																		"userCasebook_id":2,
																																		"question_id":5,
																																		"text":"Cheech",
																																		"feedback":null
																																	},
																																	{
																																		"answer_id":31,
																																		"userCasebook_id":2,
																																		"question_id":5,
																																		"text":"Tommy",
																																		"feedback":null
																																	},
																																	{
																																		"answer_id":35,
																																		"userCasebook_id":2,
																																		"question_id":5,
																																		"text":"Chong",
																																		"feedback":null
																																	}
																
																																]
																												},
																												{
																													"question_ID":6,
																													"category_ID":4,
																													"order":2,
																													"active":true,
																													"type":"1",
																													"text":"Age",
																													"notes":"(false names)",
																
																													"answers": [
																																	{
																																		"answer_id":25,
																																		"userCasebook_id":2,
																																		"question_id":6,
																																		"text":"12",
																																		"feedback":null
																																	},
																																	{
																																		"answer_id":33,
																																		"userCasebook_id":2,
																																		"question_id":6,
																																		"text":"8",
																																		"feedback":null
																																	},
																																	{
																																		"answer_id":34,
																																		"userCasebook_id":1,
																																		"question_id":6,
																																		"text":"17",
																																		"feedback":null
																																	}
																
																																]
																												}
																							
																											]
																						},
																						{
																							"category_ID":5,
																							"casebook_ID":1,
																							"name":"Adults in the household",
																							"parent_ID":3,
																							"order":2,
																							"active":true,
																							"type":"T",
																							"notes":null,
											
																							"categories":	[
																						
											
																											],
											
																							"questions":	[
																							
																												{
																													"question_ID":7,
																													"category_ID":5,
																													"order":1,
																													"active":true,
																													"type":"1",
																													"text":"Name",
																													"notes":null,
																
																													"answers": [
																																	{
																																		"answer_id":70,
																																		"userCasebook_id":1,
																																		"question_id":7,
																																		"text":"Bill",
																																		"feedback":null
																																	},
																																	{
																																		"answer_id":71,
																																		"userCasebook_id":1,
																																		"question_id":7,
																																		"text":"Mary Jane",
																																		"feedback":null
																																	}
																
																																]
																												},
																												{
																													"question_ID":8,
																													"category_ID":5,
																													"order":3,
																													"active":true,
																													"type":"1",
																													"text":"Relationship",
																													"notes":null,
																
																													"answers": [
																																	{
																																		"answer_id":72,
																																		"userCasebook_id":1,
																																		"question_id":8,
																																		"text":"Father",
																																		"feedback":null
																																	},
																																	{
																																		"answer_id":63,
																																		"userCasebook_id":1,
																																		"question_id":8,
																																		"text":"Step Mom",
																																		"feedback":null
																																	}
																
																																]
																												},
																												{
																													"question_ID":10,
																													"category_ID":5,
																													"order":4,
																													"active":true,
																													"type":"1",
																													"text":"Occupation",
																													"notes":null,
																
																													"answers": [
																																	{
																																		"answer_id":66,
																																		"userCasebook_id":1,
																																		"question_id":9,
																																		"text":"Truck driver",
																																		"feedback":null
																																	},
																																	{
																																		"answer_id":67,
																																		"userCasebook_id":1,
																																		"question_id":9,
																																		"text":"Highschool Teacher",
																																		"feedback":null
																																	}
																
																																]
																												},
																												{
																													"question_ID":9,
																													"category_ID":5,
																													"order":2,
																													"active":true,
																													"type":"1",
																													"text":"Notes",
																													"notes":null,
																
																													"answers": [
																																	{
																																		"answer_id":64,
																																		"userCasebook_id":1,
																																		"question_id":9,
																																		"text":null,
																																		"feedback":null
																																	},
																																	{
																																		"answer_id":65,
																																		"userCasebook_id":1,
																																		"question_id":9,
																																		"text":"",
																																		"feedback":null
																																	}
																
																																]
																												}
																							
																											]
																						}
											
																					],
											
																	"questions":	[
																						
																						
																	
																					]
																}
											
															],
											
											"questions":	[
																{
																	"question_ID":4,
																	"category_ID":2,
																	"order":1,
																	"active":true,
                                                                                                                                        "type":1,
																	"text":"Brief summary of condition or disability",
																	"notes":null,
																
																	"answers": [
																					{
																						"answer_id":1,
																						"userCasebook_id":1,
																						"question_id":1,
																						"text":"Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah Blah Blah Blah. Random crap. Blah Blah Blah ",
																						"feedback":null
																					}
																
																				]
																}
																
																
															]
										}
				
									]
				},
	"user":	{
				"user_id":1,
				"privilegeLevel":"S",
				"userName":"billyBob",
				"firstName":"Billy",
				"lastName":"Bob",
				"email":"billy@bob.com",
				"phone":"(123)456-7890",
				"dateLastLogin":"2015-01-20T03:04:49Z"
			}
}


<?php
}

function head(){
?>
{
	"result":"OK",
	"data":
<?php
}

function tail(){
?>
}
<?php
}

$casebook = $_REQUEST["user_casebook_id"];
if($casebook==null){
?>
{
	"result": "OK",
	"pageSize":10,
	"page":1,
	"total":2,
	"count":2,
	"data":[

<?php
casebook1();
?>
,
<?php
casebook2();
?>
]}
<?php
}else if($casebook==1){
	head();
	casebook1();
	tail();
}else if($casebook==2){
	head();
	casebook2();
	tail();
}else if($casebook<1 || $casebook>2){
?>
{
	"result": "FAILED",
	"error": "User casebook not found."
}
<?php
}
?>
