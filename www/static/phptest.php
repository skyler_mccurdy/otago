<?php
// Connecting, selecting database
$dbconn = pg_connect("host=localhost dbname=OtagoCaseSystem user=ocsAdmin password='Flaming Salamander'")
    or die('Could not connect: ' . pg_last_error());

// Performing SQL query
		$startingCasebook = (1 * 2) - (2 - 1);
		$endingCasebook = $startingCasebook + (2 - 1);
		$query = 'SELECT *
		          FROM Casebook
				  WHERE Casebook_Type_ID >= ' . $startingCasebook . 
		         'AND Casebook_Type_ID <= ' . $endingCasebook;
$result = pg_query($dbconn, $query) or die('Query failed: ' . pg_last_error());

// Printing results in HTML
echo "<table>\n";
while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) {
    echo "\t<tr>\n";
    foreach ($line as $col_value) {
        echo "\t\t<td>$col_value</td>\n";
    }
    echo "\t</tr>\n";
}
echo "</table>\n";

// Work Here--------------------------

$arr = pg_fetch_all($result);

echo "<br><br>";
print_array($arr);

// Return Array

$response = array(
					"result" => "OK",
					"data" => array()
				);

for ($i = 0; $i < count($arr); $i++)
{
	$response["data"][$i]["casebook_id"] = $arr[$i]["casebook_type_id"];
	$response["data"][$i]["casebook_name"] = $arr[$i]["casebook_name"];
	$response["data"][$i]["active"] = $arr[$i]["active"];
	$response["data"][$i]["modified_by"] = $arr[$i]["modified_by"];
	$response["data"][$i]["date_modified"] = $arr[$i]["date_modified"];
}

echo "<br><br>";
print_array($response);

// End Workspace-----------------------
	
// Free resultset
pg_free_result($result);

// Closing connection
pg_close($dbconn);

function print_array($aArray) {
	// Print a nicely formatted array representation:
	echo '<pre>';
	print_r($aArray);
	echo '</pre>';
}
?>