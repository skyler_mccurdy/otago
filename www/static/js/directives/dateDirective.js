otagoApp.directive('dateControl',function(){

	return {
		restrict:'E',
		templateUrl:'/partials/date.html',
	
		scope: {
		
			date:'=',
			editable:'=',
			useTime:'=',
			formControl:'='
		
		},
		
		controller:function($scope,$element,$timeout){
		
			$scope.dateParts = {
					year:0000,
					month:00,
					day:00,
					hour:00,
					minute:00,
					second:00
					};
					
			$scope.updating = true;
			
			$scope.$watch(function(){return $scope.date;},function(){
				updateDaysInMonth();
				
				if($scope.updating){
					return;
				}
				updateParts();
			});
			
			$scope.$watch(function(){return $scope.dateParts;},function(){
				updateDaysInMonth();
				
				if($scope.updating){
					return;
				}
				updateDate();
			},true);
			
			
			function updateParts(){
				$scope.updating=true;
				
				var d = new Date($scope.date);
				if(!$scope.date || !$scope.date.match(/\d\d\d\d-\d\d-\d\dT\d\d:\d\d:\d\dZ/)){
					d = new Date();
				}
				
				$scope.dateParts.year = d.getFullYear();
				$scope.dateParts.month = d.getMonth()+1;
				$scope.dateParts.day = d.getDate();
				$scope.dateParts.hour = d.getHours();
				$scope.dateParts.minute = d.getMinutes();
				$scope.dateParts.second = d.getSeconds();
				
				$timeout(function(){$scope.updating=false;},100);
			}
			
			function updateDate(){
				$scope.updating=true;
				
				var d = new Date($scope.dateParts.year,$scope.dateParts.month-1,$scope.dateParts.day,$scope.dateParts.hour,$scope.dateParts.minute,$scope.dateParts.second);
				
				$scope.date = leftPad(d.getUTCFullYear(),4)+"-"+leftPad((d.getUTCMonth()+1),2)+"-"+leftPad(d.getUTCDate(),2)+"T"+leftPad(d.getUTCHours(),2)+":"+leftPad(d.getUTCMinutes(),2)+":"+leftPad(d.getUTCSeconds(),2)+"Z";
				
				$timeout(function(){
					$scope.updating=false;
					if($scope.dateParts.day > $scope.daysInMonth)
						$scope.dateParts.day = $scope.daysInMonth;
				},100);
			}
			
			function updateDaysInMonth(){
				var d = new Date($scope.dateParts.year,$scope.dateParts.month,0,0,0,0);
				$scope.daysInMonth = d.getDate();
			}
			
			function leftPad(n,len){
				var num = parseInt(n);
				var len = parseInt(len);
				if(isNaN(num)||isNaN(len)){
					return n;
				}
				num = ''+num;
				while(num.length<len){
					num = '0'+num;
				}
				return num;
			}
			
			updateParts();
			updateDaysInMonth();
			
		}
		
	};


});

otagoApp.filter('leftPad',function(){

	return function (n,len){
		var num = parseInt(n);
		var len = parseInt(len);
		if(isNaN(num)||isNaN(len)){
			return n;
		}
		num = ''+num;
		while(num.length<len){
			num = '0'+num;
		}
		return num;
	}
});
