otagoApp.config(['$routeProvider',
	function ($routeProvider) {
	    $routeProvider.
		when('/login', {
		    templateUrl: '/partials/login.html',
		    controller: 'loginCtrl'
		}).
		when('/login/:url', {
		    templateUrl: '/partials/login.html',
		    controller: 'loginCtrl'
		}).
		when('/logout/', {
			templateUrl: '/partials/logout.html',
		    controller: 'logoutCtrl'
		}).
        when('/viewCaseBooks', {
            templateUrl: '/partials/viewCaseBooks.html',
            controller: 'viewCasebooksCtrl'
        }).
		when('/users',{
			templateUrl: '/partials/listUsers.html',
			controller: 'listUsersCtrl'
		}).
		when('/template/:casebook_id', {
		    templateUrl: '/partials/template.html',
		    controller: 'templateCtrl'
		}).
        when('/viewProfile/:user_id', {
            templateUrl: '/partials/viewProfile.html',
            controller: 'viewProfileCtrl'
        }).
        when('/listTemplates', {
            templateUrl: '/partials/listTemplate.html',
            controller: 'listTemplateCtrl'
        }).
		when('/casebook/:casebook_id',{
		        templateUrl: '/partials/casebook.html',
		        controller: 'casebookCtrl'
		}).
		when('/userEdit/:user_id',{
			templateUrl: '/partials/editUser.html',
			controller: 'editUserCtrl'
		}).
		otherwise({
		    redirectTo: '/viewCaseBooks'
		});
	}]);
