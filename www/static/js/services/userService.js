otagoApp.factory('userService',['$http','$location','$interval',function($http,$location,$interval){

	var userService = {
	
		user_id: null,
		userName: 'guest',
		privilegeLevel: null,
		error: null,
		refreshTimer: null,
		
		doRefresh: function(){
		
			$http.get('/api/authentication/refresh/')
				.success(function(data,status,headers,config){
				
					if(data.result=="OK"){
						userService.user_id = data.data.user_id;
						userService.userName = data.data.username;
						userService.firstName = data.data.firstName;
						userService.lastName = data.data.lastName;
						userService.privilegeLevel = data.data.privilege_level;
						userService.email = data.data.email;
						userService.phone = data.data.phone;
						userService.error = null;
						
						if(userService.userName=='guest' && $location.path() != '/login'){
							$location.path('/login');
						}else{
							
						}
						
					}else{
						userService.user_id = null;
						userService.userName = 'guest';
						userService.firstName = 'guest';
						userService.lastName = 'guest';
						userService.privilegeLevel = null;
						userService.email = null;
						userService.phone = null;
						userService.error = data.error;
						$location.path('/login');
					}
				
				}).error(function(data,status,headers,config){
						userService.user_id = null;
						userService.userName = 'guest';
						userService.firstName = 'guest';
						userService.lastName = 'guest';
						userService.privilegeLevel = null;
						userService.email = null;
						userService.phone = null;
						userService.error = data.error;
					$location.path('/login');
				});
		
		},
		
		doLogin: function(username,password,url){
			
			userService.error = null;
			
			$http.post('/api/authentication/login/',
			
				{
					username:username,
					password:password
				
				}
			
			).success(function(data,status,headers,config){
				
				if(data.result=='OK'){
					
					userService.user_id = data.data.user_id;
					userService.userName=data.data.username;
					userService.privilegeLevel=data.data.privilege_level;
					userService.error = null;
					
					if(url)
						$location.path(url);
					else
						$location.path('/');
					
				}else{
					userService.user_id = null;
					userService.userName='guest';
					userService.privilegeLevel=null;
					userService.error = data.error;
					
				}
				
			}).error(function(data,status,headers,config){
				userService.user_id = null;
				userService.userName='guest';
				userService.privilegeLevel=null;
				userService.error = data.error;
				
			});
		
		},
		
		doLogout: function(){
			
			userService.error = null;
			
			$http.post('/api/authentication/logout/',
			
				{
				}
			
			).success(function(data,status,headers,config){
				
				if(data.result=='OK'){
					userService.user_id = null;
					userService.userName = 'guest';
					userService.firstName = 'guest';
					userService.lastName = 'guest';
					userService.privilegeLevel = null;
					userService.email = null;
					userService.phone = null;
					userService.error = data.error;
					$location.path('/login');
					
				}else{
				
					userService.error = data.error;
					
				}
				
			}).error(function(data,status,headers,config){
			
				userService.error = data.error;
				
			});
		
		}
	
	};
	
	if(userService.refreshTimer==null){
		userService.refreshTimer=$interval(function(){
			userService.doRefresh();
		},5000);
	}
	
	
	return userService;
}]);
