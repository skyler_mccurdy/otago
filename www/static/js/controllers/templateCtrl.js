otagoApp.controller('templateCtrl',['$scope', '$routeParams','$http','$interval',function($scope,$routeParams,$http,$interval){

	$scope.groupTemplate='/partials/templateGroup.html';

	$scope.casebook_id = $routeParams.casebook_id;
	
	$scope.orderByPredicate = 'order'
	
	$scope.casebook = null;
	
	$scope.errors = null;
	
	
	
	function addErrors(child,errors){
		if($scope.errors==null)
			$scope.errors = {};
		if(child==null){
			$scope.errors['document']=errors;
		}else{
			$scope.errors['obj_'+child.object_ID]=errors;
		}
	}
	
	function removeErrors(child){
		if($scope.errors != null && $scope.errors.hasOwnProperty('obj_'+child.object_ID)){
			delete $scope.errors['obj_'+child.object_ID];
		}
		if($scope.errors == null || Object.keys($scope.errors).length==0){
			$scope.errors =null;
		}
	}
	
	$scope.$watch(function(scope){
			return (scope.casebook!=null)?scope.casebook.name:null;
		},function(newValue,oldValue){
			if(newValue!=oldValue){
				$http.put('/api/template/',{ casebook_id: $scope.casebook.casebook_id, name: $scope.casebook.name})
					.success(function(data,status,headers,config){
				
						if(data.result=='OK'){
							removeErrors(null);
						}else{
							addErrors(null,data.error);
						}
				
					}).error(function(data,status,headers,config){
							if(data != null && data.hasOwnProperty('error'))
								addErrors(null,data.error);
							else
								addErrors(null,'Error contacting server');
					});
				}
		});
	
	function findLower(child1){
		var parent = findParent(child1);
		
		if(parent){
			var lower = -1;
			for(var i=0;i<parent.children.length;i++){
				if(parent.children[i].order < child1.order){
					if(lower == -1){
						lower = i;
					}else{
						if(parent.children[i].order > parent.children[lower].order){
							lower = i;
						}
					}
				}
			}
			
			return parent.children[lower];
		}
		
		return null;
	}
	
	function findHigher(child1){
		var parent = findParent(child1);
		
		if(parent){
			var higher = -1;
			for(var i=0;i<parent.children.length;i++){
				if(parent.children[i].order > child1.order){
					if(higher == -1){
						higher = i;
					}else{
						if(parent.children[i].order < parent.children[higher].order){
							higher = i;
						}
					}
				}
			}
			
			return parent.children[higher];
		}
		
		return null;
	}
	
	$scope.moveUp = function(child){
		var lower = findLower(child);
		if(lower){
			var tmp = lower.order;
			lower.order = child.order;
			child.order = tmp;
		
		}
	}
	
	$scope.moveDown = function(child){
		var higher = findHigher(child);
		if(higher){
			var tmp = higher.order;
			higher.order = child.order;
			child.order = tmp;
		
		}
	}
	
	$scope.newChild = function(parent,type){
	
		var obj = {};
		
		switch(type){
		case 'group':
			obj['category_id']=-1;
			obj['casebook_id']=$scope.casebook.casebook_id;
			obj['name']='New Category';
			obj['parent_id']=(parent!=null)?parent.category_id:null;
			obj['order']=getHighestOrder((parent!=null)?parent.children:$scope.casebook.categories)+1;
			obj['active']=true;
			obj['type']='G';
			obj['notes']=null;
			obj['categories']=[];
			obj['questions']=[];
			obj['children']=[];
			obj['object_ID']=nextObject_id++;
			break;
		case 'sub-group':
			obj['category_id']=-1;
			obj['casebook_id']=$scope.casebook.casebook_id;
			obj['name']='New Subcategory';
			obj['parent_id']=(parent!=null)?parent.category_id:null;
			obj['order']=getHighestOrder((parent!=null)?parent.children:$scope.casebook.categories)+1;
			obj['active']=true;
			obj['type']='S';
			obj['notes']=null;
			obj['categories']=[];
			obj['questions']=[];
			obj['children']=[];
			obj['object_ID']=nextObject_id++;
			break;
		case 'table':
			obj['category_id']=-1;
			obj['casebook_id']=$scope.casebook.casebook_id;
			obj['name']='New Table';
			obj['parent_id']=(parent!=null)?parent.category_id:null;
			obj['order']=getHighestOrder((parent!=null)?parent.children:$scope.casebook.categories)+1;
			obj['active']=true;
			obj['type']='T';
			obj['notes']=null;
			obj['categories']=[];
			obj['questions']=[];
			obj['children']=[];
			obj['object_ID']=nextObject_id++;
			break;
		case 'single-line':
			obj['question_id']=-1;
			obj['casebook_id']=$scope.casebook.casebook_id;
			obj['text']='New Single Line Question';
			obj['category_id']=(parent!=null)?parent.category_id:null;
			obj['order']=getHighestOrder((parent!=null)?parent.children:$scope.casebook.categories)+1;
			obj['active']=true;
			obj['type']='1';
			obj['notes']=null;
			obj['object_ID']=nextObject_id++;
			break;
		case 'multi-line':
			obj['question_id']=-1;
			obj['casebook_id']=$scope.casebook.casebook_id;
			obj['text']='New Multi Line Question';
			obj['category_id']=(parent!=null)?parent.category_id:null;
			obj['order']=getHighestOrder((parent!=null)?parent.children:$scope.casebook.categories)+1;
			obj['active']=true;
			obj['type']='M';
			obj['notes']=null;
			obj['object_ID']=nextObject_id++;
			break;
		case 'date':
			obj['question_id']=-1;
			obj['casebook_id']=$scope.casebook.casebook_id;
			obj['text']='Date';
			obj['category_id']=(parent!=null)?parent.category_id:null;
			obj['order']=getHighestOrder((parent!=null)?parent.children:$scope.casebook.categories)+1;
			obj['active']=true;
			obj['type']='D';
			obj['notes']=null;
			obj['object_ID']=nextObject_id++;
			break;
		case 'checkbox':
			obj['question_id']=-1;
			obj['casebook_id']=$scope.casebook.casebook_id;
			obj['text']='Yes/No';
			obj['category_id']=(parent!=null)?parent.category_id:null;
			obj['order']=getHighestOrder((parent!=null)?parent.children:$scope.casebook.categories)+1;
			obj['active']=true;
			obj['type']='C';
			obj['notes']=null;
			obj['object_ID']=nextObject_id++;
			break;
		};
	
		if(parent!=null){
			parent.children[parent.children.length]=obj;
			$scope.saveList[$scope.saveList.length]=obj;
			if(obj.hasOwnProperty('question_id')){
				parent.questions[parent.questions.length]=obj;
			}else{
				parent.categories[parent.categories.length]=obj;
			}
			
			setWatches(parent.children[parent.children.length-1]);
		
		}else{
			$scope.casebook.categories[$scope.casebook.categories.length]=obj;
			$scope.casebook.children[$scope.casebook.children.length]=obj;
			$scope.saveList[$scope.saveList.length]=obj;
			setWatches($scope.casebook.categories[$scope.casebook.categories.length-1]);
		}
		
		
		
		
	
	}
	
	function findParent(child,current){
		var parent = (current)?current:$scope.casebook;
		
		for(var i=0;i<parent.children.length;i++){
			if(parent.children[i].object_ID == child.object_ID){
				return parent;
			}else if(parent.children[i].hasOwnProperty('children')){
				var ret = findParent(child,parent.children[i]);
				if(ret)
					return ret;
			}
		}
		
		return null;
	}
	
	function removeObject(child){
	
		var parent = findParent(child,null);
		
		if(parent){
			if(child.hasOwnProperty('question_id')){
				for(var i=0;parent.questions.length;i++){
					if(parent.questions[i].object_ID == child.object_ID){
						parent.questions.splice(i,1);
						break;
					}
				}
			}else{
				for(var i=0;parent.categories.length;i++){
					if(parent.categories[i].object_ID == child.object_ID){
						parent.categories.splice(i,1);
						break;
					}
				}
			}
			for(var i=0;i<parent.children.length;i++){
				if(parent.children[i].object_ID == child.object_ID){
					parent.children.splice(i,1);
					break;
				}
			}
			
		}
	
	}
	
	$scope.remove = function(child){
	
		if(child.hasOwnProperty('question_id')){
			var req = {
				method: 'DELETE',
				url: '/api/template/category/question/',
				headers: {
					'Content-Type':'application/json'
					},
				data: child
			};
			$http(req).success(function(data,status,headers,config){
					if(data.result=='OK'){
						removeObject(child);
						removeErrors(child);
					}else{
						addErrors(child,data.error);
					}
				}).error(function(data,status,headers,config){
					if(data != null && data.hasOwnProperty('error'))
						addErrors(child,data.error);
					else
						addErrors(null,'Error contacting server');
				});
		
		}else{
			var req = {
				method: 'DELETE',
				url: '/api/template/category/',
				headers: {
					'Content-Type':'application/json'
					},
				data: child
			};
			
			$http(req).success(function(data,status,headers,config){
					if(data.result=='OK'){
						removeObject(child);
						removeErrors(child);
					}else{
						addErrors(data.error);
					}
				}).error(function(data,status,headers,config){
					if(data != null && data.hasOwnProperty('error'))
						addErrors(child,data.error);
					else
						addErrors(null,'Error contacting server');
				});
		}
		
		
	}
	
	$scope.active = function(child){
	
		child.active = !child.active;
	}
	
	$scope.load = function(){
	
		$scope.casebook = null;
		$scope.error = null;
	
		$http.get('/api/template/?casebook_id='+$scope.casebook_id).success(function(data,status,headers,config){
		
			if(data.result=="OK"){
				$scope.casebook = data.data;
				
				$scope.casebook.children = [];
				$scope.casebook.children = $scope.casebook.children.concat($scope.casebook.categories);
				
				for(var i=0;i<$scope.casebook.children.length;i++){
					if(!$scope.casebook.children[i].hasOwnProperty("question_id")){
						zipCatagories($scope.casebook.children[i]);
					}
					setWatches($scope.casebook.children[i]);
				}
				
				removeErrors(null);
				
			}else{
				addErrors(null,data.error);
			}
		
		}).error(function(data,status,headers,config){
			if(data != null && data.hasOwnProperty('error'))
				addErrors(null,data.error);
			else
				addErrors(null,'Error contacting server');
		});
	
	}
	
	function zipCatagories(cat){
	
		cat.children = [];
		cat.children = cat.children.concat(cat.questions);
		cat.children = cat.children.concat(cat.categories);
		
		for(var i=0;i<cat.children.length;i++){
			if(!cat.children[i].hasOwnProperty("question_id")){
				zipCatagories(cat.children[i]);
			}
		}
	}
	
	var nextObject_id = 1;
	
	function setWatches(child){
	
	
		if(!child.hasOwnProperty('object_ID'))
			child["object_ID"] = nextObject_id++;
	
		if(child.hasOwnProperty("question_id")){
			$scope.$watch(function(scope){
					return child["text"];
				},function(newValue,oldValue){
					valueChanged(child,"text",newValue,oldValue);				
				});
			
		}else{
			$scope.$watch(function(scope){
					return child["name"];
				},function(newValue,oldValue){
					valueChanged(child,"name",newValue,oldValue);				
				});
		}
		
		$scope.$watch(function(scope){
				return child["notes"];
			},function(newValue,oldValue){
				valueChanged(child,"notes",newValue,oldValue);				
			});
		$scope.$watch(function(scope){
				return child["order"];
			},function(newValue,oldValue){
				valueChanged(child,"order",newValue,oldValue);				
			});
		$scope.$watch(function(scope){
				return child["active"];
			},function(newValue,oldValue){
				valueChanged(child,"active",newValue,oldValue);				
			});
			
		if(!child.hasOwnProperty("question_id")){
			for(var i =0; i<child.children.length;i++){
				setWatches(child.children[i]);
			}
		}
	}
	
	$scope.saveList = [];
	
	function valueChanged(child,member,newValue,oldValue){

		if(newValue!=oldValue){  //filters out the initial value changed when document is loaded
			for(var i=0;i<$scope.saveList.length;i++){
				if($scope.saveList[i].object_ID == child.object_ID){
					$scope.saveList[i] = child;
					return;
				}
			}
			$scope.saveList[$scope.saveList.length] = child;
		}
	}
	
	$interval(function(){
	
		for(var i=0;i<$scope.saveList.length;i++){
		
			if($scope.saveList[i].hasOwnProperty("question_id")){
				saveQuestion($scope.saveList[i]);
			}else{
				saveCategory($scope.saveList[i]);
			}
		
		}
		$scope.saveList = [];
	
	
	},1000);
	
	
	function saveQuestion(question){
	
		if(question.question_id<1){  // new question
		
			$http.post('/api/template/category/question/',question).success(function(data,status,headers,config){
				if(data.result=='OK'){
					question.question_id = data.data.question_id;
					question.order = data.data.order;
					question.active = data.data.active;
					
					removeErrors(question)
				}else{
					addErrors(question,data.error);
				}
			
				}).error(function(data,status,headers,config){
					if(data != null && data.hasOwnProperty('error'))
						addErrors(question,data.error);
					else
						addErrors(null,'Error contacting server');
			
				});	
		
		}else{
			$http.put('/api/template/category/question/',question).success(function(data,status,headers,config){
				if(data.result=='OK'){
					question.order = data.data.order;
					question.active = data.data.active;
					
					removeErrors(question);
				}else{
					addErrors(question,data.error);
				}
			
				}).error(function(data,status,headers,config){
					if(data != null && data.hasOwnProperty('error'))
						addErrors(question,data.error);
					else
						addErrors(question,'Error contacting server');
				});		
		}
	
	}
	
	function saveCategory(category){
		if(category.category_id<1){  // new category
				$http.post('/api/template/category/',category).success(function(data,status,headers,config){
					if(data.result=='OK'){
						category.category_id = data.data.category_id;
						category.order = data.data.order;
						category.active = data.data.active;
						
						removeErrors(category);
					}else{
				
						if(data != null && data.hasOwnProperty('error'))
							addErrors(category,data.error);
						else
							addErrors(null,'Error contacting server');
					}
			
					}).error(function(data,status,headers,config){
			
			
					});	
		
			}else{
				$http.put('/api/template/category/',category).success(function(data,status,headers,config){
					if(data.result=='OK'){
						category.order = data.data.order;
						category.active = data.data.active;
						
						removeErrors(category);
					}else{
						addErrros(category.data.error);
					}
			
					}).error(function(data,status,headers,config){
						if(data != null && data.hasOwnProperty('error'))
							addErrors(category,data.error);
						else
							addErrors(null,'Error contacting server');
			
					});		
			}
	
	}
	
	function getHighestOrder(list){
	
		if(list.length==0)
			return 1;
			
		var highest = list[0].order;
		for(var i=1;i<list.length;i++){
			if(list[i].order>highest)
				highest=list[i].order;
		}
		
		return highest;
	
	}
	
	
	$scope.load();


}]);
