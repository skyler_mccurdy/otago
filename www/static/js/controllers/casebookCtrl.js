//otagoApp.controller('casebookCtrl',function($scope, $http, $timeout)
otagoApp.controller('casebookCtrl', ['$scope', '$routeParams', '$http', '$timeout', '$q', 'userService', function($scope, $routeParams, $http, $timeout, $q, userService)
{
/*COMMENTS...
 * 
 *      Each category object will have an 'items' array which will hold both categories and questions
 *              This is so both categories and questions can be sorted via angular   
 *              types: M 1 T G
 * 
 * 
 *      answers must have either...
 *              an 'order' attribute
 *              or be allowed an empty string
 *          With the way tables are built using angular...
 *              answers are pulled out of the database in ascending order by primary key if no order is specified.
 * 
 *PROBLEMS... 
 * 
 *      Need to insert code to send data to database.
 *			Notes in the save function explain what needs to be written.
 *      
 *      Need to handle user type/permissions
 *                      Instructors cannot access answer fields
 *                          Handled in updateFeedback()     privilegeLevel gotten from service... conditional currently uses static assigned 'user'.
 *                      student's cannot access feedback fields
 *                          Have an inline angular directive in the textarea...
 *								ng-disabled="privilegeLevel"    privilegeLevel gotten from service... use an inline conditional
 *      
 *      Input field's formatting for conditions...
 *			Conditions...
				feedback exists for field...
				save pending
				saved
				problemSaving
 *			bootstrap
 *				Use ng-class="alert alert-success" role="alert"
 *							 "alert alert-info"
 *							 "alert alert-warning"
 *							 "alert alert-danger"
 *			Set a $scope variable to hold save status and have a conditional in the input class attributes???
 *      
 *		
 * IF student
 *      The textarea used for feeback needs to be disabled for student's... (casebook.html)
 * If Admin
 *      The same code that determines whether the feedback button shows up needs to have a condition added...
 *          If student...
 * Table is having problems with $parent.$index again with the feedback dialog boxes
 *
 * The "active" attribute needs to be taken into account when dealing with JSON output form the server.
 * 
 * casebook.html is using the following bootstrap...
 *      Need to be using CSS -> Forms -> Validation States
 *      Accordions
 *      Glyphicons
 */

            $scope.f_timerObject;
            $scope.f_saveDelay = 500;
            
            $scope.f_casebook_id;
            $scope.f_category_id;
            $scope.f_question_id;
            $scope.f_answer_id;
            $scope.f_answer;//Updating our answerFeedback with whatever has focus.

	$scope.loading = true;



	$scope.currentUser = userService;


    $http.get('/api/casebook/?userCasebook_ID=' + $routeParams.casebook_id).success(function(data)
    {
        $scope.loading=true;
        $scope.casebook = data.data;
        
        for(var i = 0 ; i < $scope.casebook.casebook.categories.length ; i++)
        {
            //The first layer of categories still needs to be sorted...
            sort($scope.casebook.casebook.categories[i], "G");

        }
        console.log($scope.casebook);
        
        for(var i = 0 ; i < $scope.casebook.casebook.categories.length ; i++)
        {
            addWatches($scope.casebook.casebook.categories[i]);
        }
        $timeout(function(){$scope.loading=false;},500);
	
    });
    
    
    function addWatches(item){
    	switch(item.type){
    		case 'G':
    		case 'S':
	    		for(var i=0; i < item.items.length; i++){
    				addWatches(item.items[i]);
    			}
    		break;
    		case 'T':
    			for(var i=0; i < item.questions.length; i++){
    				addWatches(item.questions[i]);
    			}
    			addTableWatch(item);
    		break;
    		case '1':
    		case 'M':
    		case 'D':
    		case 'C':
    			if(item.answers.length==0){
    				item.answers[0] = { answer_id: null, feedback:null, question_id: item.question_id, text: null, userCasebook_id: $scope.casebook.userCasebook_id };
    			}
    			for(var a=0;a<item.answers.length;a++){
					function w(answer){
						$scope.$watch(function(){ return answer.text; },function(){saveAnswer(answer);},true);
					};w(item.answers[a]);
					function wf(answer){
						$scope.$watch(function(){ return answer.feedback; },function(){saveFeedback(answer);},true);
					};wf(item.answers[a]);
    			}
    		break;
    	}
    }
    
    function addTableWatch(table){
    	
    	$scope.$watch(function(){return table;},function(){
    	
    		if(table.questions.length==0)
    			return;
    		
    		for(var a=0;a<table.questions[0].answers.length-1;a++){
    			var hasData = false;
    			for(var q=0;q<table.questions.length;q++){
    				if(table.questions[q].answers[a].text != null && table.questions[q].answers[a].text.length > 0)
    					hasData=true;
    			}
    			if(!hasData){
    				deleteRow(table,a);
    				a--;
    			}
    		}
    		
    		var hasData = false;
    		for(var q=0;q<table.questions.length;q++){
    			if(table.questions[q].answers.length==0){
    				hasData=true;
    			}else{
					if(table.questions[q].answers[table.questions[q].answers.length-1].text != null && table.questions[q].answers[table.questions[q].answers.length-1].text.length > 0)
						hasData=true;
    			}
    		}
    		
    		if(hasData)
	    		addRow(table);
    	
    	},true);
    }
    
    function deleteRow(table,index){
    	for(var q=0;q<table.questions.length;q++){
    		table.questions[q].answers.splice(index,1);
    	}
    }
    
    function addRow(table){
    	for(var q=0;q<table.questions.length;q++){
    		table.questions[q].answers[table.questions[q].answers.length] = {
    						answer_id: null,
    						question_id: table.questions[q].question_id,
    						userCasebook_id: $scope.casebook.userCasebook_id,
    						text:null,
    						feedback:null
    						};
    		
			function w(answer){
				$scope.$watch(function(){ return answer.text; },function(){saveAnswer(answer);},true);
			};w(table.questions[q].answers[table.questions[q].answers.length-1]);
			function wf(answer){
				$scope.$watch(function(){ return answer.feedback; },function(){saveFeedback(answer);},true);
			};wf(table.questions[q].answers[table.questions[q].answers.length-1]);
    	}
    	
    }
    
    function saveAnswer(answer){
    	if(!$scope.loading || answer.answer_id==null){
			if(answer.saving){
				//answer.saveAgain=true;
				return;
			}
			
			
			answer.saving = $timeout(function(){
			
				if(answer.answer_id == null || answer.answer_id == 'N'){
					answer.answer_id == null;
					$http.post('/api/casebook/question/',answer).success(function(data){
						if(data.result=='OK'){
							answer.answer_id = data.data.answer_id;
							answer.error = null;
							if(answer.saveAgain){
								answer.saving=false;
								answer.saveAgain=false;
								saveAnswer(answer);
							}
						}else{
							answer.error = data.error;
							$timeout(function(){saveAnswer(answer);},1000);
						}
						answer.saving=null;
						answer.saveAgain=false;
					}).error(function(data){
						if(data.result=='FAILED'){
							answer.error = data.error;
						}else{
							answer.error = 'Error contacting server';
						}
						answer.saving=null;
						answer.saveAgain=false;
						
					});
				}else{
					$http.put('/api/casebook/question/',answer).success(function(data){
						if(data.result=='OK'){
							answer.error = null;
							if(answer.saveAgain){
								answer.saving=false;
								answer.saveAgain=false;
								saveAnswer(answer);
							}
						}else{
							answer.error = data.error;
							
						}
						answer.saving=null;
						answer.saveAgain=false;
					}).error(function(data){
						if(data.result=='FAILED'){
							answer.error = data.error;
						}else{
							answer.error = 'Error contacting server';
						}
						answer.saving=null;
						answer.saveAgain=false;
						
					});
				}
			},1000);
    	}
    }
    
    
    function saveFeedback(answer){
    	if(!$scope.loading){
			
			if(answer.savingFeedback){
				answer.saveFeedbackAgain=true;
				return;
			}
			
			
			answer.savingFeedback = $timeout(function(){
			
				
				$http.put('/api/casebook/feedback/',answer).success(function(data){
					if(data.result=='OK'){
						answer.error = null;
						answer.newFeedback=true;
						if(answer.saveAgain){
							answer.savingFeedback=false;
							answer.saveFeedbackAgain=false;
							saveFeedback(answer);
						}
					}else{
						answer.error = data.error;
						
					}
					answer.savingFeedback=null;
					answer.saveFeedbackAgain=false;
				}).error(function(data){
					if(data.result=='FAILED'){
						answer.error = data.error;
					}else{
						answer.error = 'Error contacting server';
					}
					answer.savingFeedback=null;
					answer.saveFeedbackAgain=false;
					
				});
				
			},1000);
    	}
    }
    

    
    function sort(category, previousType)
    {

        if(category.type == "1" || category.type == "M")
        {
        	
            return;
        }
    
    
    
        /*********************************************************************************
        * Creating an items array and filling it with categories and questions.
        *       Used javascript concat() method isntead...
        */

       category.items = [];
       category.items = category.categories.concat(category.questions);
    
        
        


        for(var i = 0 ; i < category.items.length ; i++)//Recursively calling itself.
        {

            
            switch(category.items[i].type)//displayType will need to be changed to Type once its fixed.
            {
            	case 'S':
                case 'G':
                    sort(category.items[i], category.items[i].type);
                    break;
                case 'T'://Any questions found under this category are sorted as table headers.
  
                    tableSort(category.items[i]);
                    break;

            }
        }
    }
    
    
    
    
    

    
    
    
    
    
    
    
    
    
    
    
    function tableSort(category)//Only questions/answers encountered here... no categories
    {

        
        /*********************************************************************************
        * Creating a 'rows' array to hold our data by row instead of by column...
        *       Tables are created by row... our table data is currently in columns...
        *       
        * Creating a totalRows array to keep track of how many rows we'll have in our table...
        *       Tables have as many rows as there are the largest number of answers in a question.
        */
        category.rows = [];
        var totalRows = 0;
        
 
        
        
        /*********************************************************************************
        * Finding how many rows our table will have.
        */
        for(var i = 0 ; i < category.questions.length ; i++)
        {
            
            if(totalRows < category.questions[i].answers.length)
            {
                totalRows = category.questions[i].answers.length;//Finding how many answers we should have.
            }
        }

        /*********************************************************************************
        * Adding one additional blank row at the end of the table for the user to insert new answers.
        */
        totalRows++;
        
        
        /*********************************************************************************
        * Creating answers containing empty strings.
        *       Needed in casebook.html to ensure empty input fields in our tables.
        */
        for(var it = 0 ; it < category.questions.length ; it++)
        {
            for(var i = 0 ; i < totalRows; i++)
            {
                if(!category.questions[it].answers[i])//If the current answer does not exist...
                {
                    
                    category.questions[it].answers[i] = 
                    {
                        answer_id: null,
                        userCasebook_id: $scope.casebook.userCasebook_id,
                        question_id: category.questions[it].question_id,
                        text: "",
                        feedback: null
                    }
                    
                    function w(answer){
						$scope.$watch(function(){ return answer.text; },function(){saveAnswer(answer);},true);
					};w(category.questions[it].answers[i]);

                }
            }
        }
        
        
        
        /*********************************************************************************
        * Inserting answers into our rows.
        */
        for(var it = 0 ; it < totalRows ; it++)//First row equates to the first answer in each question.
        {
            category.rows[it] = [];//Creating a new arraw
            for(var i = 0 ; i < category.questions.length ; i++)//Iterate through each column
            {           
                category.rows[it][i] = category.questions[i].answers[it];
            }
        }
        

    }
}]);



