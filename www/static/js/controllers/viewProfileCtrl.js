otagoApp.controller('viewProfileCtrl', ['$scope', '$routeParams', '$http', '$interval', function ($scope, $routeParams, $http, $interval) {


    //gets parameters from url
    $scope.user_id = $routeParams.user_id;

    $scope.user = null;

    $scope.dataHasChanged = false;

    $scope.errors = null;

    function addErrors(child, errors) {
        //alert(errors);
        if ($scope.errors == null) {
            $scope.errors = {};
        }
        if (child == null) {
            $scope.errors['document'] = errors;
        } else {
            $scope.errors['document'] = (child + " is not valid");
        }

    }

    function removeErrors() {
        $scope.errors = null;
    }


    $scope.load = function () {

        $http.get('/api/user/?user_id=' + $scope.user_id).success(function (data) {
            $scope.user = data.data;

            //alert($scope.user.user_id);

            setWatches($scope.user);


        });


    }


    //alert($scope.user.user_id);
    function setWatches(user) {


        //watching change in First Name
        $scope.$watch(function (scope) {
            //$scope.dataHasChanged = false;

            console.log("This is being watched = " + user.firstName);
            return user.firstName;
        },

        function (newValue, oldValue) {
            if (newValue !== oldValue) {
                //checking my values
                console.log(newValue + " old-> " + oldValue);
                console.log(user.firstName + " " + newValue);
                console.log(user.firstName + " this is changed?");

                //change newValue and call "Put" function
                user.firstName = newValue;
                $http.put('/api/user/', { user_id: user.user_id, firstName: user.firstName })
                .success(function (data, status, headers, config) {
                    if (data.result == 'OK') {
                        removeErrors();
                        $scope.dataHasChanged = true;
                    }
                });
            }
        });

        //watching change in Last Name
        $scope.$watch(function (scope) {
            console.log("This is being watched = " + user.lastName);
            return user.lastName;
        },

        function (newValue, oldValue) {
            if (newValue !== oldValue) {
                //checking my values
                console.log(newValue + " old-> " + oldValue);
                console.log(user.lastName + " " + newValue);
                console.log(user.lastName + " this is changed?");

                //change newValue and call "Put" function
                user.lastName = newValue;
                $http.put('/api/user/', { user_id: user.user_id, lastName: user.lastName })
                .success(function (data, status, headers, config) {
                    removeErrors();
                    $scope.dataHasChanged = true;
                });
            }
        });

        //watching change in E-mail
        $scope.$watch(function (scope) {
            console.log("This is being watched = " + user.email);
            if (user.email == null) {
                addErrors("e-mail", " is not valid");
            }
            return user.email;
        },

        function (newValue, oldValue) {

            if (newValue !== oldValue) {
                //checking my values
                console.log(newValue + " old-> " + oldValue);
                console.log(user.email + " " + newValue);
                console.log(user.email + " this is changed?");

                //change newValue and call "Put" function
                user.email = newValue;
                $http.put('/api/user/', { user_id: user.user_id, email: user.email })
                .success(function (data, status, headers, config) {
                    removeErrors();
                    $scope.dataHasChanged = true;

                }).error(function (data, status, headers, config) {
                    addErrors(user.email, data.error);
                });
            }
        });

        //watching change in phone
        $scope.$watch(function (scope) {
            console.log("This is being watched = " + user.phone);
            return user.phone;
        },

        function (newValue, oldValue) {
            if (newValue !== oldValue) {
                //checking my values
                console.log(newValue + " old-> " + oldValue);
                console.log(user.phone + " " + newValue);
                console.log(user.phone + " this is changed?");

                //change newValue and call "Put" function
                user.phone = newValue;
                $http.put('/api/user/', { user_id: user.user_id, phone: user.phone })
                .success(function (data, status, headers, config) {
                    removeErrors();
                    $scope.dataHasChanged = true;
                }).error(function (data, status, headers, config) {
                    addErrors(user.phone, data.error);
                });
            }
        });


    }

    //validates password
    $("input[type=password]").keyup(function () {

        var ucase = new RegExp("[A-Z]+");
        var lcase = new RegExp("[a-z]+");
        var num = new RegExp("[0-9]+");
        var isValid = true;
        $('#changePassword').prop('disabled', true);


        //$('#changePassword').prop('disabled', true);

        if ($("#password1").val().length >= 8) {
            $("#8char").removeClass("glyphicon-remove");
            $("#8char").addClass("glyphicon-ok");
            $("#8char").css("color", "#00A41E");
        } else {
            $("#8char").removeClass("glyphicon-ok");
            $("#8char").addClass("glyphicon-remove");
            $("#8char").css("color", "#FF0004");
            isValid = false;
        }

        if (ucase.test($("#password1").val())) {
            $("#ucase").removeClass("glyphicon-remove");
            $("#ucase").addClass("glyphicon-ok");
            $("#ucase").css("color", "#00A41E");
        } else {
            $("#ucase").removeClass("glyphicon-ok");
            $("#ucase").addClass("glyphicon-remove");
            $("#ucase").css("color", "#FF0004");
            isValid = false;

        }

        if (lcase.test($("#password1").val())) {
            $("#lcase").removeClass("glyphicon-remove");
            $("#lcase").addClass("glyphicon-ok");
            $("#lcase").css("color", "#00A41E");
        } else {
            $("#lcase").removeClass("glyphicon-ok");
            $("#lcase").addClass("glyphicon-remove");
            $("#lcase").css("color", "#FF0004");
            isValid = false;

        }

        if (num.test($("#password1").val())) {
            $("#num").removeClass("glyphicon-remove");
            $("#num").addClass("glyphicon-ok");
            $("#num").css("color", "#00A41E");
        } else {
            $("#num").removeClass("glyphicon-ok");
            $("#num").addClass("glyphicon-remove");
            $("#num").css("color", "#FF0004");
            isValid = false;

        }

        if ($("#password1").val() == $("#password2").val()) {
            $("#pwmatch").removeClass("glyphicon-remove");
            $("#pwmatch").addClass("glyphicon-ok");
            $("#pwmatch").css("color", "#00A41E");




        } else {
            $("#pwmatch").removeClass("glyphicon-ok");
            $("#pwmatch").addClass("glyphicon-remove");
            $("#pwmatch").css("color", "#FF0004");
            isValid = false;

        }

        if (isValid) {
            $('#changePassword').prop('disabled', false);

            //calling put function
            $(document).ready(function () {
                $("#changePassword").click(function () {

                    var passwordP = $("#password2").val();
                    $http.put('/api/user/', { user_id: $scope.user.user_id, password: passwordP })
                .success(function (data, status, headers, config) {
                    console.log("password has changed");

                });
                });
            });
        } //end if


    });








    $scope.load();


    //interval function
    $interval(function () {

        $scope.dataHasChanged = false;

    }, 2000);


}]);

