otagoApp.controller('listTemplateCtrl', ['$scope', '$routeParams', '$http', '$interval', '$timeout', '$location', function ($scope, $routeParams, $http, $interval, $timeout, $location) {

    $scope.page = 1;
    $scope.pageSize = 10;
    $scope.lastPage = 1;
    $scope.orderBy = '';
    $scope.updating=null;
	$scope.name=null;

    $scope.error = null;
    $scope.templates = null;
	
	
	$scope.$watch(function(){return $scope.page;},function(){getTemplates();});
	$scope.$watch(function(){return $scope.orderBy;},function(){getTemplates();});
	$scope.$watch(function(){return $scope.pageSize;},function(){page=1;getTemplates();});
	$scope.$watch(function(){return $scope.name;},function(){page=1;getTemplates();});

	function getTemplates(){
	
		if($scope.updating)
			return;

		$scope.updating=$timeout(function(){	
			$http.get('/api/template/? '+
				'page='+$scope.page+
				'&pageSize='+$scope.pageSize+
				'&orderBy='+$scope.orderBy+
				'&name='+$scope.name
				).success(function(data){
					if(data.result=='OK'){
						$scope.templates = data.data;
						$scope.error=null;
						$scope.lastPage = parseInt(data.total/data.pageSize) + ((data.total%data.pageSize)?1:0);
					}else{
						$scope.error=data.error;
						$scope.templates=null;
						$scope.lastPage=1;
					}
					$scope.updating=null;
				}).error(function(data){
					if(data.result=='FAILED')
						$scope.error=data.error;
					else
						$scope.error='Error contacting server';
					$scope.templates=null;
					$scope.lastPage=1;
					$scope.updating=null;
				});
			},100);
	
	}
	
	$scope.sortBy = function(columnName){
		var parts = ['',''];
		if($scope.orderBy){
			parts = $scope.orderBy.split(':');
		}
		if(parts[0] == columnName){
			if(parts[1] == 'desc'){
				parts[1]='asc';
			}else{
				parts[1]='desc';
			}
		}else{
			parts[0]=columnName;
			parts[1]='asc';
		}
		$scope.orderBy=parts[0]+':'+parts[1];
	}
	
	$scope.openTemplate = function (template_id) {
        $location.path("/template/" + template_id);
    };
    
    
    $scope.create = {};
    $scope.create.name=null;
    $scope.create.error=null;
    $scope.create.createTemplate = function(){
    	
    	$http.post('/api/template/',$scope.create)
    		.success(function(data){
    			if(data.result=='OK'){
    				$scope.create.error=null;
    				$scope.create.name=null;
    				$scope.showCreate=null;
    				$scope.openTemplate(data.data.casebook_id);
    			}else{
    				$scope.create.error=data.error;
    			}
    		})
    		.error(function(data){
    			if(data.result=='FAILED'){
    				$scope.create.error = data.error;
    			}else{
    				$scope.create.error='Error contacting server';
    			}
    		});
    }
    
    $scope.assign = {};
    $scope.assign.error = null;
    $scope.assign.user_id = null;
    $scope.assign.username = null;
    $scope.assign.dueDate = null;
    $scope.assign.users = null;
    $scope.assign.search = null;
    $scope.assign.updating = null;
    
    
    $scope.$watch(function(){return $scope.assign.search;},function(){getUsers();});
    
    function getUsers(){
    
    	if($scope.assign.updating)
    		return;
    
    	$scope.assign.updating = $timeout(function(){
    		$http.get('/api/user/?'+
    			'page=1&pageSize=10'+
    			'&search='+encodeURIComponent($scope.assign.search)
    			).success(function(data){
    				if(data.result=='OK'){
    					$scope.assign.users=data.data;
    					$scope.assign.error=null;
    				}else{
    					$scope.assign.error=data.error;
    					$scope.assign.users=null;
    				}
    				$scope.assign.updating=null;
    			}).error(function(data){
    				if(data.error){
    					$scope.assign.error=data.error;
    					$scope.assign.users=null;
    				}else{
    					$scope.assign.error="Error contacting server";
    					$scope.assign.users=null;
    				}
    				$scope.assign.updating=null;
    			});
    	
    	},100);
    
    }
    
    $scope.assign.assignCasebook = function(){
    	
    	$http.post('/api/casebook/',{ user_ID: $scope.assign.user_id, casebook_ID: $scope.assign.template_id, dueDate: $scope.assign.dueDate})
    		.success(function(data){
    			if(data.result=='OK'){
    				$scope.assign.error=null;
    				$scope.assign.username=null;
    				$scope.assign.user_id=null;
    				$scope.assign.template_id=null;
    				$scope.assign.dueDate=null;
    				$scope.showAssign=null;
    			}else{
    				$scope.assign.error=data.error;
    			}
    		})
    		.error(function(data){
    			if(data.error){
    				$scope.assign.error = data.error;
    			}else{
    				$scope.assign.error='Error contacting server';
    			}
    		});
    }

}]);
