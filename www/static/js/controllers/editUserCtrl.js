otagoApp.controller('editUserCtrl', ['$scope', '$routeParams', '$http', '$interval','$timeout','$location', function ($scope, $routeParams, $http, $interval, $timeout, $location) {

    //recieved user_id from listUsers.js
    $scope.sentUserID = $routeParams.user_id;
    //sentUserID
	
	$scope.password = null;
	
    $scope.errorDeclaration = '';
    $scope.incomplete = false;
    $scope.userNameError = false;
    $scope.PLerror = false;
    $scope.FNerror = false;
    $scope.LNerror = false;
    $scope.emailError = false;
    $scope.phoneError = false;
    $scope.passwordError = false;
    
    $scope.successSubmit = false;

	$scope.loading = true;

    //default database request
    $http.get('api/user/?user_id=' + $scope.sentUserID)
        .success(function (data) {
            $scope.user = data;
            $scope.data = data.data;
            $scope.numRecords = $scope.user.total;//for user with lastPage()
            if (data.result != 'OK') {
                $scope.error = data.error;
                $scope.user = '';
                $scope.data = '';
                $scope.errorDeclaration = 'The user was not found.';
            }
            else
            {
            	$scope.predicateUserName = data.data.userName;
                $scope.predicatePL = data.data.privilegeLevel;
                $scope.predicateFN = data.data.firstName;
                $scope.predicateLN = data.data.lastName;
                $scope.predicateEmail = data.data.email;
                $scope.predicatePhone = data.data.phone;
            }
            $timeout(function(){$scope.loading=false;},100);
        })
        .error(function (data) {
            $scope.user = '';
            $scope.error = data.error;
            $scope.errorDeclaration = 'The user was not found.';
    });
    
    $scope.$watch(function(){return $scope.successSubmit;},function(){
    	if($scope.successSubmit){
    		$timeout(function(){$scope.successSubmit=false;},3000);
    	}
    });
    
    $scope.$watch('predicateUserName',function() {$scope.test();$scope.saveUser();});
    $scope.$watch('predicatePL',function() {$scope.test();$scope.saveUser();});
    $scope.$watch('predicateFN', function() {$scope.test();$scope.saveUser();});
    $scope.$watch('predicateLN', function() {$scope.test();$scope.saveUser();});
    $scope.$watch('predicateEmail', function() {$scope.test();$scope.saveUser();});
    $scope.$watch('predicatePhone', function() {$scope.test();$scope.saveUser();});
    $scope.$watch('password', function() {$scope.test();});
    $scope.$watch('newPassword', function() {$scope.test();});
    $scope.$watch('confirmPassword', function() {$scope.test();});

    $scope.test = function() {
      $scope.incomplete = false;
      if (!$scope.predicateUserName ||
          !$scope.predicatePL ||
          !$scope.predicateFN || 
          !$scope.predicateLN || 
          !$scope.predicateEmail || 
          !$scope.predicatePhone) {
           $scope.incomplete = true;
      }
      
      var match = /^[A-Z0-9_\-\.]{1,30}$/i.test($scope.predicateUserName);
      if (!match) {
    	  $scope.userNameError = true;
      }
      else {
    	  $scope.userNameError = false;
      }
      
      var match = /^[SI]$/i.test($scope.predicatePL);
      if (!match) {
    	  $scope.PLerror = true;
      }
      else {
    	  $scope.PLerror = false;
      }

      match = /^[a-zA-Z]{1,30}$/i.test($scope.predicateFN);
      if (!match) {
    	  $scope.FNerror = true;
      }
      else {
    	  $scope.FNerror = false;
      }
      
      match = /^[a-zA-Z]{1,30}$/i.test($scope.predicateLN);
      if (!match) {
    	  $scope.LNerror = true;
      }
      else {
    	  $scope.LNerror = false;
      }
      
      match = /^[A-Z0-9\.]{1,100}@[A-Z0-9\.\-]{1,97}\.[A-Z]{2,3}$/i.test($scope.predicateEmail);
      if (!match) {
    	  $scope.emailError = true;
      }
      else {
    	  $scope.emailError = false;
      }
      
      match = /^[\d \(\)\-+]{3,50}$/.test($scope.predicatePhone);
      if (!match) {
    	  $scope.phoneError = true;
      }
      else {
    	  $scope.phoneError = false;
      }
      
      match = /^$^|[\w\d\s\.!@#$%^&*()_\-+={}\[\]?\,\/`~<>\\\\\'"]{4,30}$/i.test($scope.password);
      if (!match) {
              $scope.passwordError = true;
      }
      else {
    	  $scope.passwordError = false;
      }
      
      match = /^$^|[\w\d\s\.!@#$%^&*()_\-+={}\[\]?\,\/`~<>\\\\\'"]{4,30}$/i.test($scope.newPassword);
      if (!match) {
              $scope.newPasswordError = true;
      }
      else {
    	  $scope.newPasswordError = false;
      }
      
      match = /^$^|[\w\d\s\.!@#$%^&*()_\-+={}\[\]?\,\/`~<>\\\\\'"]{4,30}$/i.test($scope.confirmPassword);
      if (!match) {
              $scope.confirmPasswordError = true;
      }
      else {
    	  $scope.confirmPasswordError = false;
      }
    };
    
    $scope.saveUser = function () {
    
    	if($scope.loading){
    		return;
    	}
    
    	if($scope.userNameError || $scope.PLerror || $scope.FNerror || $scope.LNerror || $scope.emailError || $scope.phoneError || $scope.passwordError){
    		return;
    	}
    
        $http.put('api/user/?user_id=' + $scope.sentUserID, {
	    "user_id":$scope.sentUserID, 
    	"userName":$scope.predicateUserName, 
    	"password":$scope.password, 
    	"privilegeLevel":$scope.predicatePL, 
    	"firstName":$scope.predicateFN, 
    	"lastName":$scope.predicateLN, 
    	"email":$scope.predicateEmail, 
    	"phone":$scope.predicatePhone
    	})
	    .success(function(data, status, headers, config) {
	        if (data.result != 'OK') {
	            $scope.error = data.error;
	            $scope.errorDeclaration = 'The user could not be updated.'
	        }
	        else {
	        	$scope.successSubmit = true;
	        }
	    })
	    .error(function(data, status, headers, config) {
	    	$scope.errorDeclaration = 'The user could not be updated.'
	    });
    };
    
    $scope.deleteUser = function() {
    	$http.delete('api/user', {
    	"user_id":$scope.sentUserID
    	})
    	  .success(function(response, status, headers, config){
    		  
    	  })
    	  .error(function(response, status, headers, config){
    		  
    	  });
    	}
}]);

