﻿otagoApp.controller('viewCasebooksCtrl', ['$scope', '$routeParams', '$http', '$interval', '$timeout', '$location', 'userService', function ($scope, $routeParams, $http, $interval, $timeout, $location, userService) {

    // --------------------------------default/empty values for variables----------------------------------------TODO: change url format

    $scope.page = 1;//current page
    $scope.lastPage = 1;
    $scope.pageSize = 10;//number of table rows per page
    $scope.predicateOrder = '';//sort by order

    $scope.errorDeclaration = '';//anouncement that there was an error
    $scope.error = '';//array of errors
    $scope.casebook = '';//throw away cached data
    $scope.data = '';//throw away cached data
    
    $scope.currentUser = userService;

    $scope.errorFlag = false;

    $scope.errorOccured = function (data, status, headers, config) {
        $scope.casebook = '';//ERASE ANY PARTIAL DATA RETURNED
        $scope.data = '';//erase any partial data returned
        $scope.error = data.error;//GET THE ERROR ARRAY
        $scope.errorDeclaration = 'An Error Occured...Sorry About That.';//SET THE ERROR DECLARATION SO IT WILL SHOW IN THE HTML
        $scope.status = status;
        $scope.headers = headers;
        $scope.config = config;
        $scope.errorFlag = true;
    };

    $scope.setDropDown = function () {
        $scope.firstPageBool = true;
        $scope.lastPageBool = false;
        $scope.pageCounter = $scope.numRecords / $scope.sizePage;
       // $scope.array = [];
        var i = 1;
        $scope.array[i-1] = i;
        while ($scope.array.length < $scope.pageCounter) {
            i++;
            $scope.array[i-1] = i;
        }
        if ($scope.array.length == 1)
        {
            $scope.lastPageBool = true;
        }
            
    };

	$scope.openCasebook = function(casebook_id){
		$location.path('/casebook/'+casebook_id);
	}
    
	$scope.sortBy = function(columnName){
    	var parts = ['',''];
    	if($scope.predicateOrder){
	    	var parts = $scope.predicateOrder.split(':');
    	}
    	
    	if(parts[0]==columnName){
    		if(parts[1]=='desc')
    			parts[1]='asc';
			else
				parts[1]='desc';
    	}else{
    		parts[0]=columnName;
    		parts[1]='asc';
    	}
    	$scope.predicateOrder=parts[0]+':'+parts[1];
    }
    
    $scope.$watch(function(){return $scope.predicateOrder;},function(){$scope.httpGet();});
    $scope.$watch(function(){return $scope.page;},function(){$scope.httpGet();});
    $scope.$watch(function(){return $scope.pageSize;},function(){$scope.httpGet();});
    
    $scope.$watch(function(){return $scope.search;},function(){
    	$scope.page=1;
    	$scope.httpGet();
	});

	$scope.updating = null;
    //get new data with parameters passed in from the HTML
    $scope.httpGet = function () {

		if($scope.updating)
			return;

		$scope.updating = $timeout(function(){
		    $http.get('/api/casebook/?' +
		        'order=' + $scope.predicateOrder +
		        '&page=' + $scope.page +
		        '&pageSize=' + (($scope.pageSize>0)?$scope.pageSize:1) +
		        '&search=' + encodeURIComponent($scope.search)
		        ).success(function (data, status, headers, config) {
		            
		            if (data.result != 'OK') {
		                $scope.errorOccured(data, status, headers, config);
		            }
		            else {
		                $scope.casebook = data;
		                $scope.data = data.data;
		                $scope.page = data.page;
		                $scope.lastPage = parseInt(data.total/data.pageSize) + ((data.total%data.pageSize)?1:0);
		            }
		            $scope.updating=null;
		        })
		        .error(function (data, status, headers, config) {
		            $scope.errorOccured(data, status, headers, config);
		            $scope.updating=null;
		        });
        },100);
    };
    


}]);

