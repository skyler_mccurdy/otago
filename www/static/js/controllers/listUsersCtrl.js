﻿otagoApp.controller('listUsersCtrl', ['$scope', '$routeParams', '$http', '$interval', '$timeout', '$location', function ($scope, $routeParams, $http, $interval, $timeout, $location) {

    
    $scope.page = 1;//current page
    $scope.pageSize = 10;
    $scope.predicateOrder ='';//sorting order
    $scope.error=null;
    $scope.updating=null;
    $scope.users=null;
    


    
    //pass user_id from table row to editUser.js via route.js
    $scope.editUser = function (user_id) {
        $location.path("/userEdit/" + user_id);
    };

    //redirect to createUser.js via route.js
    $scope.createUser = function () {
        $location.path("/userCreate/");
    };

	$scope.$watch(function(){return $scope.page;},function(){$scope.httpGet();});    
	$scope.$watch(function(){return $scope.pageSize;},function(){page=1;$scope.httpGet();});    
	$scope.$watch(function(){return $scope.search;},function(){page=1;$scope.httpGet();});    
	$scope.$watch(function(){return $scope.predicateOrder;},function(){$scope.httpGet();});    
	
	$scope.sortBy = function(columnName){
    	var parts = ['',''];
    	if($scope.predicateOrder){
	    	var parts = $scope.predicateOrder.split(':');
    	}
    	
    	if(parts[0]==columnName){
    		if(parts[1]=='desc')
    			parts[1]='asc';
			else
				parts[1]='desc';
    	}else{
    		parts[0]=columnName;
    		parts[1]='asc';
    	}
    	$scope.predicateOrder=parts[0]+':'+parts[1];
    }

    $scope.httpGet = function () {
    	
    	if($scope.updating)
    		return;
    		
        $scope.updating= $timeout(function(){
        	$http.get('/api/user/?'+
        			'&page=' + $scope.page +
        			'&pageSize=' + $scope.pageSize + 
		            '&order=' + $scope.predicateOrder + 
		            '&search=' + encodeURIComponent($scope.search)
		            
				)
		        .success(function (data) {
		        	if(data.result=='OK'){
		        		$scope.users=data.data;
		        		$scope.error=null;
		        		$scope.lastPage = parseInt(data.total/data.pageSize) + ((data.total%data.pageSize)?1:0);
		        	}else{
		        		$scope.users=null;
		        		$scope.error=data.error;
		        	}
		        	$scope.updating=null;
		            
		        })
		        .error(function (data) {
		            if(data.error){
		            	$scope.users=null;
		            	$scope.error=data.error;
		            }else{
		            	$scope.users=null;
		            	$scope.error="Error contacting server";
		            }
		            $scope.updating=null;
		        });
			},100);
    };
    
    
    $scope.create = {};
    $scope.create.user = {
    	userName: null,
		privilegeLevel:'S',
		password: null,
	    confirmPassword: null,
		firstName: null,
		lastName: null,
		email: null,
		phone: null
		};
    $scope.create.errors = {};
    $scope.create.updating = null;
    
    $scope.$watch(function(){return $scope.showCreate;},function(){
    		if(!$scope.showCreate){
    			$scope.create.user = {
					userName: null,
					privilegeLevel:'S',
					password: null,
					confirmPassword: null,
					firstName: null,
					lastName: null,
					email: null,
					phone: null
					};
    		}
    	});
    
    
    $scope.create.createUser = function(){
    	if($scope.create.updating)
    		return null;
    		
    	
    	
    	if($scope.create.hasErrors)
    		return;
    		
    	$scope.create.updating = $timeout(function(){
    		$http.post('/api/user/',$scope.create.user)
    			.success(function(data){
    				if(data.result=='OK'){
    					$scope.create.errors = {};
    					$scope.create.user = {
								userName: null,
								privilegeLevel:'S',
								password: null,
								confirmPassword: null,
								firstName: null,
								lastName: null,
								email: null,
								phone: null
								};
						$scope.showCreate=false;
						$scope.httpGet();
    				}else{
    					$scope.create.errors=data.error;
    				}
    				$scope.create.updaing=null;
    			
    			})
    			.error(function(data){
    				if(data.error){
    					$scope.create.errors = data.error;
    				}else{
    					$scope.create.errors['general'] = 'Unable to contact server';
    				}
    			
    				$scope.create.updaing=null;
    			});
    	},100);
    
    }
    
    $scope.$watch(function(){return $scope.create.user;},function(){validateUser();},true);
    
	function validateUser() {
	
		var validations = {
				'userName': {
								test: function(value){
										return /^[A-Z0-9_\-\.]{1,30}$/i.test(value);
									},
								error:'1 to 30 numbers, letters, _, ., and -'
							},
				'privilegeLevel':{
								test: function(value){
										return /^[SI]$/i.test(value);
									},
								error:'Must be Student or Instructor'
							},
				'firstName':{
								test: function(value){
										return /^[^ -\+/:-@[-^{-~]{1,30}$/i.test(value);
									},
								error:'1 to 30 letters and numbers only'
							},
				'lastName':{
								test: function(value){
										return /^[^ -\+/:-@[-^{-~]{1,30}$/i.test(value);
									},
								error:'1 to 30 letters and numbers only'
							},
				'email':{
								test: function(value){
										return /^[A-Z0-9\.]{1,100}@[A-Z0-9\.\-]{1,97}\.[A-Z]{2,3}$/i.test(value);
									},
								error:'Invalid e-mail address'
							},
				'phone':{
								test: function(value){
										return /^[\d \(\)\-+]{3,50}$/.test(value);
									},
								error:'Invalid phone number'
							},
				'password':{
								test: function(value){
										return /^[\w\d\s\.!@#$%^&*()_\-+={}\[\]?\,\/`~<>\\\\\'"]{4,30}$/i.test(value);
									},
								error:'4 to 30 numbers, letters, punctuation, and spaces'
							},
				'confirmPassword':{
								test: function(value){
										return $scope.create.user.password == value;
									},
								error:'Must match password'
							},
							
		
			};
		
		$scope.create.hasErrors = false;
		for(var key in validations){
			if(!$scope.create.user[key] || !validations[key].test($scope.create.user[key])){
				$scope.create.errors[key] = validations[key].error;
				$scope.create.hasErrors=true;
			}else{
				$scope.create.errors[key] = null;
			}
		}
    };

}]);

