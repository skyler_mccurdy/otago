otagoApp.controller('refreshCtrl',
	
	['$scope','$location','$interval','userService',

	function($scope,$location,$interval,userService){

		$scope.user = userService;
		userService.doRefresh();
	}]);
