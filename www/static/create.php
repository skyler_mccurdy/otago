<!DOCTYPE html>
<html lang="en" ng-app="otagoApp">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">

    <title>University of Otago -- Create Casebook</title>

   		<script src="/js/external/jquery.min.js"></script>
		<script src="/js/external/bootstrap.min.js"></script>
		<script src="/js/external/angular.min.js"></script>
		<script src="/js/external/angular-route.js"></script>

		<link rel="stylesheet" href="/css/bootstrap.min.css" />
		<link rel="stylesheet" href="/css/bootstrap-theme.css" />

		<link rel="stylesheet" href="/css/site.css" />
  </head>
  <body>
          <h1 class="page-header">Create Casebook</h1>
			<div class="container">
            <form name="create" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                <div class="form-group">
                    <label for="casebookName">Casebook Name</label>
                    <input type="text" class="form-control" id="casebookName" placeholder="Enter casebook name">
                </div>
                <div class="form-group">
                    <label class="control-label" for="description">Description</label>
                    <textarea class="form-control" id="description" placeholder="Enter casebook description"></textarea>
                </div>
                <div class="row">
                <div class="control-group" id="fields">
                    <label class="control-label" for="question1">Add Questions</label>
                    <div class="controls"> 
                        <form role="form" autocomplete="off">
                            <div class="entry input-group col-xs-3">
                                <input class="form-control" name="questions[]" type="text" placeholder="Type question" />
                                <span class="input-group-btn">
                                    <button class="btn btn-success btn-add" type="button">
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </button>
                                </span>
                            </div>
                        </form>
                    <br>
                    </div>
                </div>
            </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
  </body>
</html>