<!DOCTYPE html>
<html lang="en" ng-app="otagoApp">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">

    <title>University of Otago -- Student Casebook System</title>

   		<script src="/js/external/jquery.min.js"></script>
		<script src="/js/external/bootstrap.min.js"></script>
		<script src="/js/external/angular.min.js"></script>
		<script src="/js/external/angular-route.js"></script>
		<script src="/js/external/ui-bootstrap-tpls-0.12.0.min.js"></script>

		<link rel="stylesheet" href="/css/bootstrap.min.css" />
		<!--<link rel="stylesheet" href="/css/bootstrap-theme.css" />-->

		<link rel="stylesheet" href="/css/site.css" />

		<script src="/js/app.js"></script>
		
		<script src="/js/services/userService.js"></script>
		
		<script src="/js/directives/dateDirective.js"></script>
		
		<script src="/js/controllers/refreshCtrl.js"></script>

		<script src="/js/controllers/navBarCtrl.js"></script>
		<script src="/js/controllers/loginCtrl.js"></script>

		<script src="/js/controllers/logoutCtrl.js"></script>

		<script src="/js/controllers/viewCasebooksCtrl.js"></script>
		<script src="/js/controllers/listTemplateCtrl.js"></script>
		<script src="/js/controllers/viewProfileCtrl.js"></script>
		<script src="/js/controllers/templateCtrl.js"></script>
		<script src="/js/controllers/listUsersCtrl.js"></script>
		<script src="/js/controllers/editUserCtrl.js"></script>

		<script src="/js/controllers/casebookCtrl.js"></script>

		<script src="/js/route.js"></script>

  </head>

  <body>

	<div ng-controller="refreshCtrl">
		
			<nav class="navbar" ng-controller="navBarCtrl">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#/"><img src ="../pictures/logo.png" alt="logo goes here" class="img-rounded" style="height:200%;margin-top:-10px"/></a>

					</div>
					<div id="navbar" class="navbar-collapse collapse">
					
						<ul class="nav navbar-nav">
	                		<li ><a href="#/viewCaseBooks">Casebooks</a></li>
	                		<li ng-show="user.privilegeLevel =='I'"><a href="#/listTemplates">Templates</a></li>
	                		<li ng-show="user.privilegeLevel =='I'"><a href="#/users">Users</a></li>
						</ul>
					
						<ul class="nav navbar-nav navbar-right">
							<li><a href="#/viewProfile/{{user.user_id}}">{{user.firstName}} {{user.lastName}}</a></li>
							<li><a href="#/logout">Logout</a></li>
						</ul>
	                
					</div>
				</div>
			</nav>


	</div>

	<div ng-view>
	
	</div>

  </body>
</html>
