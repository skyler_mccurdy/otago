<?php

header('Content-Type: application/json');

$user = array();

try{


	if(!verifyToken()){
		http_response_code(401);
		
		echo toJson(array(
						"result"=> "FAILED",
						"error" => "Must be logged in"
					));
					
		return;
		
	}

	$parameters = defineParameters();
	getParameters($parameters);
	$errors = validate($parameters);
	if(count($errors)>0){
		http_response_code(400);
		echo toJson(array(
						"result" => "FAILED",
						"error" => $errors
					));

	}else{
		$response = null;
		switch($_SERVER['REQUEST_METHOD']){
		case "GET":
			$response=GET($parameters);
			http_response_code(200);
			break;
		case "POST":
			$response=POST($parameters);
			http_response_code(201);
			break;
		case "PUT":
			$response=PUT($parameters);
			http_response_code(200);
			break;
		case "DELETE":
			$response=DELETE($parameters);
			http_response_code(200);
			break;
		default:
			$response = array(
							"result" => "FAILED",
							"error" => "Invalid method call"
							);
		}
	
		
		echo toJson($response);
	}
}catch(Exception $e){
	http_response_code(500);
	echo toJson(array(
					"result" => "FAILED",
					"error" => array(
										"exception" => $e->getMessage()
									)
				));
}

function getParameters(&$parameters){

	if($_SERVER['REQUEST_METHOD']=="GET"){
		foreach($parameters as $key => $value){
			if(array_key_exists($key,$_REQUEST) and $_REQUEST[$key]!==null){
				$parameters[$key] = $_REQUEST[$key];
			}
		}
	}
else{
		$inputJSON = file_get_contents('php://input');
		$input= fromJson( $inputJSON);
		foreach($parameters as $key => $value){
			if(array_key_exists($key,$input) and $input[$key]!==null){
				$parameters[$key] = $input[$key];
			}
		}
	
	}
}

function verifyToken(){

	global $sql_connectionString;
	global $user;

	$db = null;
	
	try{

		$token = null;

		if(isset($_COOKIE["X-XSRF-TOKEN"])){
			$token = $_COOKIE["X-XSRF-TOKEN"];
		}
		
		if(isset(getallheaders()["Application-Authorization"])){
			$token = getallheaders()["Application-Authorization"];
		}


		if($token){
		
			$db = pg_connect($sql_connectionString);
			if(!$db){
				throw new Exception("Error connecting to database");
			}
	
			$query = 'select p.person_id,user_name,first_name,last_name,privilege_level '.
						' from "token" as t '.
						' join person as p on t.person_id = p.person_id '.
						' where token = $1 and expires > now();';
			$result = pg_query_params($db,$query,array($token));
			if(!$result){
				return false;
			}
	
			$row = pg_fetch_assoc($result);
			pg_free_result($result);
		
			$user["user_id"] = $row["person_id"];
			$user["username"] = $row["user_name"];
			$user["firstName"] = $row["first_name"];
			$user["lastName"] = $row["last_name"];
			$user["privilegeLevel"] = $row["privilege_level"];
		
			return true;
		}
	
		return false;
	
	}finally{
		if($db){
			pg_close($db);
		}
	}

}

?>
