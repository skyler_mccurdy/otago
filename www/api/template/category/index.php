<?php
/**
*	Used as a template for API end points
*
*	Copy this file to new location and adjust include paths
*
*/
include "../../../secure.php";
include "../../validation.php";
include "../../json.php";
include "../../framework.php";

/**
*	Define the allowed parameters and their default values
*
*	Parameters to GET requests will be filled from query parameters
*	Parameters to all other requests will be loaded from a JSON body
*
*/
function defineParameters(){
	switch($_SERVER['REQUEST_METHOD']){
	case "GET":
		return array(
				"casebook_id" => null,
				"category_id" => null,
				"name" => null,
				"page" => null,
				"pageSize" => null,
				"orderBy" => "order,name"
			);
		break;
	case "POST":
		return array(
		        "casebook_id" => null,
				"name" => null,
				"type" => null,
				"parent_id" => null,
				"order" => null,
				"notes" => null,
				"active" => true
			);
		break;
	case "PUT":
		return array(
		        "casebook_id" => null,
		        "category_id" => null,
				"name" => null,
				"type" => null,
				"parent_id" => null,
				"order" => null,
				"notes" => null,
				"active" => null
			);
		break;
	case "DELETE":
		return array(
		        "casebook_id" => null,
		        "category_id" => null
			);
		break;
	default:
		return null;
	}
}

/**
*	Validates the parameters
*
*	Should validate parameters using functions from validation.php
*	Returns an array of errors
*
*/
function validate(&$parameters){
	$errors = array();

	switch($_SERVER['REQUEST_METHOD']){
		case "GET":
			if($parameters["casebook_id"] != null and !Validation\Template::casebook_id($parameters["casebook_id"])){
				$errors["casebook_id"]="invalid casebook id";
			}
			if(!Validation\Template::casebookName($parameters["name"])){
				$errors["name"]="invalid casebook name";
			}
			if(!Validation\Template::page($parameters["page"])){
				$errors["page"]="invalid page number";
			}
			if(!Validation\Template::pageSize($parameters["pageSize"])){
				$errors["pageSize"]="invalid page size";
			}
			break;
		case "POST":
			if($parameters["casebook_id"] != null and !Validation\Template::casebook_id($parameters["casebook_id"])){
				$errors["casebook_id"]="invalid casebook id";
			}
			if($parameters["name"] != null and !Validation\Template::casebookName($parameters["name"])){
				$errors["name"]="invalid casebook name";
			}
			if($parameters["type"] != null and !Validation\Template::type($parameters["type"])){
				$errors["type"]="invalid display type";
			}
			if(!Validation\Template::casebook_id($parameters["parent_id"])){
				$errors["parent_id"]="invalid parent id";
			}
			if(!Validation\Template::order($parameters["order"])){
				$errors["order"]="invalid category order";
			}
			if(!Validation\Template::notes($parameters["notes"])){
				$errors["notes"]="invalid notes";
			}
			if(!Validation\Template::active($parameters["active"])){
				$errors["active"]="invalid active state";
			}
			break;
		case "PUT":
			if($parameters["casebook_id"] != null and !Validation\Template::casebook_id($parameters["casebook_id"])){
				$errors["casebook_id"]="invalid casebook id";
			}
			if($parameters["category_id"] != null and !Validation\Template::casebook_id($parameters["category_id"])){
				$errors["category_id"]="invalid category id";
			}
			if($parameters["name"] != null and !Validation\Template::casebookName($parameters["name"])){
				$errors["name"]="invalid casebook name";
			}
			if($parameters["type"] != null and !Validation\Template::type($parameters["type"])){
				$errors["type"]="invalid display type";
			}
			if(!Validation\Template::casebook_id($parameters["parent_id"])){
				$errors["parent_id"]="invalid parent id";
			}
			if($parameters["order"] != null and !Validation\Template::order($parameters["order"])){
				$errors["order"]="invalid category order";
			}
			if(!Validation\Template::notes($parameters["notes"])){
				$errors["notes"]="invalid notes";
			}
			if($parameters["active"] != null and !Validation\Template::active($parameters["active"])){
				$errors["active"]="invalid active state";
			}
			break;
		case "DELETE":
			if($parameters["category_id"] != null and !Validation\Template::casebook_id($parameters["category_id"])){
				$errors["category_id"]="invalid category id";
			}
			break;
		default:
			return $errors["call"]="Invalid method call";
	}
}

/**
*	Processes GET Requests
*
*/
function GET($parameters){
	global $sql_connectionString;
	
	$response = array(
			"result" => "OK"
	);
		
	$db = pg_connect($sql_connectionString);
	if(!$db){
		throw new Exception("Error connecting to database");
	}
	try{
		//returns a single casebook if id parameter is specified or a list of casebook templates if not
		if ($parameters["category_id"]){
			// Performing SQL query
			$query = 'select '.
								'category_id, '.
								'casebook_type_id, '.
								'category_name, '.
								'parent_category_id, '.
								'group_order, '.
								'active, '.
								'display_type, '.
								'notes '.
						" from \"question_category\" where Category_ID=$1 ";
			$result = pg_query_params($db,$query,array($parameters["category_id"]));
			// return error if needed
			if(!$result){
				throw new Exception("Category not found");
			}
			
			$row = pg_fetch_assoc($result);
			pg_free_result($result);
				
			if($row == null){
				return array(
						"result" => "FAILED",
						"error" => "Category not found"
				);
			}
			// return array
			$response["data"] = buildCategory($db,$row);
		}
		else {
			if ($parameters["page"] == null)
			{
				$parameters["page"] = 1;
			}
			if ($parameters["pageSize"] == null)
			{
				$parameters["pageSize"] = 10;
			}
			$startingCategory = ($parameters["page"] * $parameters["pageSize"]) - ($parameters["pageSize"] - 1);
			$endingCategory = $startingCategory + ($parameters["pageSize"] - 1);
			$response["total"] = $endingCategory - $startingCategory;
			
			// Performing SQL query
			$query = 'select '.
								'category_id, '.
								'casebook_type_id, '.
								'category_name, '.
								'parent_category_id, '.
								'group_order, '.
								'active, '.
								'display_type, '.
								'notes '.
						" from \"question_category\" where active = true ";
			$whereStatement = "";
			$whereParams = array();

			if ($parameters["casebook_id"]){
				$whereParams[] = $parameters["casebook_id"];
				$whereStatement = " AND casebook_type_id = $1 ";
			}
			
			if ($parameters["name"]){
				$whereParams[] = "%".$parameters["name"]."%";
				$whereStatement .= " AND category_name ilike $".(count($whereParams)). " ";
			}
			
			if($parameters["orderBy"] !=null){
				$orderBys = explode(",",$parameters["orderBy"]);
				if(count($orderBys)>0){
					$whereStatement = $whereStatement . " order by ". addOrderBy($orderBys[0]) ." ";
					for($i=1;$i<count($orderBys);$i++){
						$whereStatement = $whereStatement . ", ".addOrderBy($orderBys[$i])." ";
					}
				}
			}
				
			if($parameters["page"] != null || $parameters["pageSize"]){
				$page = ($parameters["page"]!=null)?$parameters["page"]:1;
				$pageSize = ($parameters["pageSize"]!=null)?$parameters["pageSize"]:10;
			
				$result["page"] = $page;
				$result["pageSize"] = $pageSize;
			
				$whereStatement = $whereStatement . " limit $".(count($whereParams)+1)." offset $".(count($whereParams)+2)." ";
				$page = ($page-1) * $pageSize;
				$whereParams[] = $pageSize;
				$whereParams[] = $page;
			}
				
			$result = pg_query_params($db,$query.$whereStatement,$whereParams);
			if(!$result){
				throw new Exception("Invalid username or password");
			}
				
			$response["count"] = pg_num_rows($result);
				
			$response["data"]=array();
			while($row = pg_fetch_assoc($result)){
				$response["data"][] = buildCategory($db,$row);
			}
				
			pg_free_result($result);
		}
	} 
	finally {
		pg_close($db);
	}
	
	
	return $response;
}

/**
*	Processes POST Requests
*
*/
function POST($parameters){
	global $sql_connectionString;
	global $user;
	
	$response = array(
			"result" => "OK",
			"data" => array()
	);
		
	$db = pg_connect($sql_connectionString);
	if(!$db){
		throw new Exception("Error connecting to database");
	}
	try{
		if($user["privilegeLevel"] != "I"){
			http_response_code(301);
			return array(
					"result" => "OK",
					"error" => "User does not have the required permissions"
			);
		}
		
		if ($parameters["order"] == null){
			$query = "SELECT max(group_order)
                      FROM Question_Category
					  WHERE Casebook_Type_ID = $1";
			$result = pg_query_params($db,$query,array($parameters["casebook_id"]));
			if(!$result){
				throw new Exception("Internal Server Error");
			}
			$row = pg_fetch_array($result);
			$parameters["order"] = $row[0] + 1;
			pg_free_result($result);
		}
		
		// Insert into database
		$query = "INSERT INTO Question_Category (Casebook_Type_ID, Category_Name, Parent_Category_ID, Group_Order, Active, Display_Type, Notes, Modified_By, Date_Modified)
	              VALUES ($1, $2, $3, $4, $5, $6, $7, $8, now())"
				  . ' returning casebook_type_id ';
		$result = pg_query_params($db,$query,array($parameters["casebook_id"], $parameters["name"], $parameters["parent_id"], $parameters["order"], $parameters["active"], $parameters["type"], $parameters["notes"], $user["username"]));
		if(!$result){
			throw new Exception("Internal Server Error");
		}	
		$row = pg_fetch_assoc($result);
		pg_free_result($result);
		
		if($row == null){
			return array(
					"result" => "FAILED",
					"error" => "Could not insert category"
			);
		}
		// Performing SQL query
			$query = 'select '.
								'category_id, '.
								'casebook_type_id, '.
								'category_name, '.
								'parent_category_id, '.
								'group_order, '.
								'active, '.
								'display_type, '.
								'notes '.
						" from \"question_category\" where Category_Name=$1 ";
			$result = pg_query_params($db,$query,array($parameters["name"]));
		// return error if needed
		if(!$result){
			throw new Exception("Internal server error");
		}
			
		$row = pg_fetch_assoc($result);
		pg_free_result($result);
				
		if($row == null){
			return array(
					"result" => "FAILED",
					"error" => "Category not found"
			);
		}
		// return array
		$response["data"] = buildCategory($db,$row);	
	}
	finally {
		pg_close($db);
	}
	
	
	return $response;
}

/**
*	Processes PUT Requests
*
*/
function PUT($parameters){
	global $sql_connectionString;
	global $user;
	
	$response = array(
			"result" => "OK",
			"data" => array()
	);
	
	$db = pg_connect($sql_connectionString);
	if(!$db){
		throw new Exception("Error connecting to database");
	}
	try{
		if($user["privilegeLevel"] != "I"){
			http_response_code(301);
			return array(
					"result" => "OK",
					"error" => "User does not have the required permissions"
			);
		}
		
		// First select row to update
			$query = 'select '.
								'category_id, '.
								'casebook_type_id, '.
								'category_name, '.
								'parent_category_id, '.
								'group_order, '.
								'active, '.
								'display_type, '.
								'notes '.
						" from \"question_category\" where Category_ID=$1 ";
			$result = pg_query_params($db,$query,array($parameters["category_id"]));
		// return error if needed
		if(!$result){
			throw new Exception("Category not found");
		}
			
		$row = pg_fetch_assoc($result);
		pg_free_result($result);
		
		// Update casebook
		$query = 'update "question_category" set '.
		         'casebook_type_id = $2, '.
			     'category_name = $3, '.
			     'parent_category_id = $4, '.
			     'group_order = $5, '.
		         'active = $6, '.
		         'display_type = $7, '.
                 'notes = $8, '.
		         'modified_by = $9, '.
            	 'date_modified = now() '.
		         'where category_id = $1 ';
		$result = pg_query_params($db,$query,array($parameters["category_id"], 
				                                   $parameters["casebook_id"], 
				                                   $parameters["name"], 
				                                   $parameters["parent_id"], 
				                                   $parameters["order"], 
				                                   (($parameters["active"])?1:0), 
				                                   $parameters["type"], 
				                                   $parameters["notes"],
												   $user["username"]
		));
		if(!$result){
			throw new Exception("Internal server error");
		}
		$rowsAffected = pg_affected_rows($result);
		pg_free_result($result);
		
		if($rowsAffected!=1){
			return array(
					"result" => "FAILED",
					"error" => "Could not update category"
			);
		}
		
		// Return Updated Casebook
			$query = 'select '.
								'category_id, '.
								'casebook_type_id, '.
								'category_name, '.
								'parent_category_id, '.
								'group_order, '.
								'active, '.
								'display_type, '.
								'notes '.
						" from \"question_category\" where Category_ID=$1 ";
			$result = pg_query_params($db,$query,array($parameters["category_id"]));
		// return error if needed
		if(!$result){
			throw new Exception("Internal server error");
		}
			
		$row = pg_fetch_assoc($result);
		pg_free_result($result);
	
		if($row == null){
			return array(
					"result" => "FAILED",
					"error" => "Category not found"
			);
		}
		// return array
		$response["data"] = buildCategory($db,$row);
	}
	finally {
		pg_close($db);
	}
	
	
	return $response;
}


/**
*	Processes DELETE Requests
*
*/
function DELETE($parameters){

	global $sql_connectionString;
	global $user;
		
	$db = pg_connect($sql_connectionString);
	if(!$db){
		throw new Exception("Error connecting to database");
	}
	try{
		if($user["privilegeLevel"] != "I"){
			http_response_code(301);
			return array(
					"result" => "OK",
					"error" => "User does not have the required permissions"
			);
		}
		
		$query = 'delete from "question_category" where category_id = $1 ';
		$result = pg_query_params($db,$query,array(
				$parameters["category_id"],
		));
		if(!$result){
			throw new Exception("Internal server error");
		}
		$rowsAffected = pg_affected_rows($result);
		pg_free_result($result);
	
		if($rowsAffected!=1){
			return array(
					"result" => "FAILED",
					"error" => "Could not delete category"
			);
		}
	
		return array(
				"result" => "OK"
		);
	
	
	}finally{
		pg_close($db);
	}
}

function addOrderBy($orderBy){
	$parts = explode(":",$orderBy,2);
	$allowed = array(
			"category_id" => "category_id",
			"casebook_id" => "casebook_type_id",
			"name" => "category_name",
			"parent_id" => "parent_category_id",
			"order" => "group_order",
			"active" => "active",
			"type" => "display_type",
			"notes" => "notes"
	);
	
	if(array_key_exists($parts[0],$allowed)){
		return " ".$allowed[$parts[0]]." ";
	}

	throw new Exception("Invalid order");
}

function buildCategory($db, $db_casebook){

	$category = array(
			        "category_id" => intval($db_casebook["category_id"]),
					"casebook_id" => intval($db_casebook["casebook_type_id"]),
					"name" => $db_casebook["category_name"],
                    "parent_id" => ($db_casebook["parent_category_id"]==null)?null:intval($db_casebook["parent_category_id"]),
					"order" => intval($db_casebook["group_order"]),
					"active" => $db_casebook["active"]=="t",
					"type" => $db_casebook["display_type"],
					"notes" => $db_casebook["notes"],
					"categories" => getCategoriesFor($db,intval($db_casebook["casebook_type_id"]),intval($db_casebook["category_id"])),
					"questions" => getQuestionsFor($db,intval($db_casebook["category_id"]))
					);
	
	return $category;
}

function getCategoriesFor($db,$casebookType_ID,$parent_ID){

	$categories = array();
	
	$query = 'select '.
						'category_id, '.
						'casebook_type_id, '.
						'category_name, '.
						'parent_category_id, '.
						'group_order, '.
						'active, '.
						'display_type, '.
						'notes '.
				" from \"question_category\" where active = true and Casebook_Type_ID=$1 ";
	if($parent_ID == null){
		$query = $query . " and parent_category_id is null ";
	}else{
		$query = $query . " and parent_category_id = $2";
	}
	$params = array(
						$casebookType_ID
					);
	if($parent_ID!=null){
		$params[1] = $parent_ID;
	}
	$result = pg_query_params($db,$query,$params);
	if(!$result){
		throw new Exception("Casebook Type not found");
	}
	
	while($row = pg_fetch_assoc($result)){
			$categories[] = array(
									"category_id" => intval($row["category_id"]),
									"casebook_id" => intval($row["casebook_type_id"]),
									"name" => $row["category_name"],
									"parent_id" => ($row["parent_category_id"]==null)?null:intval($row["parent_category_id"]),
									"order" => intval($row["group_order"]),
									"active" => $row["active"]=="t",
									"type" => $row["display_type"],
									"notes" => $row["notes"],
									"categories" => getCategoriesFor($db,$casebookType_ID,intval($row["category_id"])),
									"questions" => getQuestionsFor($db,intval($row["category_id"]))
								);
	}
	
	pg_free_result($result);
	
	return $categories;

}

function getQuestionsFor($db,$category_ID){
	$questions = array();
	
	$query = 'select '.
						'question_id, '.
						'category_id, '.
						'question_order, '.
						'active, '.
						'question_text, '.
						'notes '.
						
				" from \"question_text\" where active = true and category_id = $1";
	
	$result = pg_query_params($db,$query,array($category_ID));
	if(!$result){
		throw new Exception("Casebook Type not found");
	}
	
	while($row = pg_fetch_assoc($result)){
			$questions[] = array(
									"question_id" => intval($row["question_id"]),
									"category_id" => intval($row["category_id"]),
									"order" => intval($row["question_order"]),
									"active" => $row["active"]=="t",
									"text" => $row["question_text"],
									"notes" => $row["notes"]
								);
	}
	
	pg_free_result($result);
	
	return $questions;
}
?>
