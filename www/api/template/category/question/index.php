<?php
/**
 *	Used as a template for API end points
 *
 *	Copy this file to new location and adjust include paths
 *
 */
include "../../../../secure.php";
include "../../../validation.php";
include "../../../json.php";
include "../../../framework.php";

/**
 * Define the allowed parameters and their default values
 *
 * Parameters to GET requests will be filled from query parameters
 * Parameters to all other requests will be loaded from a JSON body
 */
function defineParameters() {
	switch ($_SERVER ['REQUEST_METHOD']) {
		case "GET" :
			return array (
					"casebook_type_id" => null,
					"category_id" => null,
					"question_id" => null,
					"page" => null,
					"pageSize" => null,
					"orderBy" => "order" 
			);
			break;
		case "POST" :
			return array (
					"category_id" => null,
					"text" => null,
					"order" => null,
					"notes" => null,
					"active" => true 
			);
			break;
		case "PUT" :
			return array (
					"question_id" => null,
					"category_id" => null,
					"text" => null,
					"order" => null,
					"notes" => null,
					"active" => null,
					"type" => null,
			);
			
			break;
		case "DELETE" :
			return array (
					"question_id" => null 
			);
			
			break;
		default :
			return null;
	}
}

/**
 * Validates the parameters
 *
 * Should validate parameters using functions from validation.php
 * Returns an array of errors
 */
function validate(&$parameters) {
	$errors = array ();
	
	switch ($_SERVER ['REQUEST_METHOD']) {
		case "GET" :
			
			if ($parameters ["category_id"] != null and ! Validation\CasebookQuestion::category_ID ( $parameters ["category_id"] )) {
				$errors ["category_id"] = "invalid user category id";
			}
			if ($parameters ["question_id"] != null and ! Validation\CasebookQuestion::question_ID ( $parameters ["question_id"] )) {
				$errors ["question_id"] = "invalid question id";
			}
			if ($parameters ["page"] != null and ! Validation\Generic::page ( $parameters ["page"] )) {
				$errors ["page"] = "invalid page number";
			}
			if ($parameters ["pageSize"] != null and ! Validation\Generic::pageSize ( $parameters ["pageSize"] )) {
				$errors ["pageSize"] = "invalid page size";
			}
			return $errors;
			break;
		case "POST" :
			if ($parameters ["category_id"] != null and ! Validation\CasebookQuestion::category_ID ( $parameters ["category_id"] )) {
				$errors ["category_id"] = "invalid user category id";
			}
			
			if (! Validation\CasebookQuestion::order ( $parameters ["order"] )) {
				$errors ["order"] = "invalid order";
			}
			
			if (! Validation\CasebookQuestion::active ( $parameters ["active"] )) {
				$errors ["active"] = "invalid active";
			}
			
			if (! Validation\CasebookQuestion::text ( $parameters ["text"] )) {
				$errors ["text"] = "invalid text";
			}
			
			if (! Validation\CasebookQuestion::notes ( $parameters ["notes"] )) {
				$errors ["notes"] = "invalid notes";
			}
			
			return $errors;
			break;
		case "PUT" :
			
			if (! Validation\CasebookQuestion::category_ID ( $parameters ["category_id"] )) {
				$errors ["category_id"] = "invalid user category id";
			}
			if (! Validation\CasebookQuestion::question_ID ( $parameters ["question_id"] )) {
				$errors ["question_id"] = "invalid question id";
			}
			
			if (! Validation\CasebookQuestion::order ( $parameters ["order"] )) {
				$errors ["order"] = "invalid order";
			}
			
			if (! Validation\CasebookQuestion::active ( $parameters ["active"] )) {
				$errors ["active"] = "invalid active";
			}
			
			if (! Validation\CasebookQuestion::type ( $parameters ["type"] )) {
				$errors ["type"] = "invalid type";
			}
			
			if (! Validation\CasebookQuestion::text ( $parameters ["text"] )) {
				$errors ["text"] = "invalid text";
			}
			
			if (! Validation\CasebookQuestion::notes ( $parameters ["notes"] )) {
				$errors ["notes"] = "invalid notes";
			}
			
			return $errors;
			break;
		case "DELETE" :
			if ($parameters ["question_id"] != null and ! Validation\CasebookQuestion::question_ID ( $parameters ["question_id"] )) {
				$errors ["question_id"] = "invalid question id";
			}
			return $errors;
			break;
		default :
			return $errors ["call"] = "Invalid method call";
	}
	
	return $errors;
}

/**
 * Processes GET Requests
 */
function GET($parameters) {
	//connect to data base
	global $sql_connectionString; 
	$dbconn = pg_connect ( $sql_connectionString ) or die ( 'Could not connect: ' . pg_last_error () );
	if (! $dbconn) {
		throw new Exception ( "Error connecting to database" );
	}
	
	
	//initialize array
	$response = array (
			"result" => "OK",
			"data" => array () 
	);
	try {
		// if question_id given returns a single question
		if ($parameters ["question_id"]) {
			$response ["data"] = getQuestion ( $dbconn, $parameters ["question_id"] );
			
			// if category_id given returns a questions					
		} else if ($parameters ["category_id"]) {
			
			$response ["data"] = getQuestionsFor ( $dbconn, $parameters ["category_id"] );
		} 
		
		
		//else if casebook_id is given return all questions for casebook
		else {
			$query = 'SELECT ' .
			 'casebook_type_id, ' .
			 'category_id ' .
			 'FROM "question_category" ';
			
			$whereStatement = "";
			$whereParams = array ();
			
			//makes where parameters for "Select Statement"
			if ($parameters ["casebook_type_id"] != null) {
				if (strlen ( $whereStatement ) > 0) {
					$whereStatement = $whereStatement . " and ";
				}
				$whereStatement = $whereStatement . " casebook_type_id = $" . (count ( $whereParams ) + 1) . " ";
				$whereParams [] = $parameters ["casebook_type_id"];
			}
			
			if (strlen ( $whereStatement ) > 0) {
				$whereStatement = " where " . $whereStatement;
			}
			
			if($parameters["order"] !=null){
				$orderBys = explode(",",$parameters["order"]);
				if(count($orderBys)>0){
					$whereStatement = $whereStatement . " order by ". addOrderBy($orderBys[0]) ." ";
					for($i=1;$i<count($orderBys);$i++){
						$whereStatement = $whereStatement . ", ".addOrderBy($orderBys[$i])." ";
					}
				}
			}
			
			//if want to show limited pages, then specify "page" or "pageSize" in parameters
			if ($parameters ["page"] != null || $parameters ["pageSize"]) {
				$page = ($parameters ["page"] != null) ? $parameters ["page"] : 1;
				$pageSize = ($parameters ["pageSize"] != null) ? $parameters ["pageSize"] : 10;
				$result ["page"] = $page;
				$result ["pageSize"] = $pageSize;
				
				$whereStatement = $whereStatement . " limit $" . (count ( $whereParams ) + 1) . " offset $" . (count ( $whereParams ) + 2) . " ";
				$page = ($page - 1) * $pageSize;
				$whereParams [] = $pageSize;
				$whereParams [] = $page;
			}
			
			//pass in query parameters
			$result = pg_query_params ( $dbconn, $query . $whereStatement, $whereParams );
			
			//populates data by callin Build function
			while ( $row = pg_fetch_assoc ( $result ) ) {
				$response ["data"] [] = Build ( $dbconn, $row );
			}
		}
		
		//catch errors
	} catch ( Exception $e ) {
		echo 'Caught exception: ', $e->getMessage (), "\n";
		
		//close connection
	} finally{
		pg_close ( $dbconn );
	}
	
	return $response;
}

/**
 * Processes POST Requests
 */
function POST($parameters) {
	global $user;
	//connect database
	global $sql_connectionString;
	$dbconn = pg_connect ( $sql_connectionString );	
	if (! $dbconn) {
		throw new Exception ( "Error connecting to database" );
	}
	//initialize array
	$response = array (
			"result" => "OK",
			"data" => array () 
	);
	try {
		
		if($parameters["order"] == null){
			$query  = "select max(question_order) + 1 from question_text where category_id = $1";
			$result = pg_query_params ( $dbconn, $query, array (
					$parameters ["category_id"]
			) );
		
		
			if (! $result) {
				throw new Exception ( "Internal server error" );
			}
			$row = pg_fetch_array ( $result );
			$parameters["order"] = $row[0];
			pg_free_result ( $result );
			//select max(question_order) + 1 from question_text where category_id = $1
		}
		
		//query statement
		$query = 'INSERT INTO "question_text" ' .
		 ' (category_id, question_order, active, question_text, notes, display_type, modified_by,date_modified) ' .
		 "VALUES ( $1, $2, $3, $4, $5, 'M',$6, now() ) " .
		 ' returning question_id ';
		
		//parameters to pass in, for query statements...example $1 = $parameters ["category_id"]	
		$result = pg_query_params ( $dbconn, $query, array (
				$parameters ["category_id"],
				$parameters ["order"],
				$parameters ["active"],
				$parameters ["text"],
				$parameters ["notes"], 
				$user["username"]
		) );

		if (! $result) {
			throw new Exception ( "Internal server error" );
		}
		$row = pg_fetch_assoc ( $result );
		pg_free_result ( $result );
		
		//check if category_id exists
		if ($row == null or ! array_key_exists ( "question_id", $row )) {
			return array (
					"result" => "FAILED",
					"error" => "Could not insert question" 
			);
		}
		
		
		//select statement
		$query = 'SELECT ' .
		 'question_id, ' .
		 'category_id, ' .
		 'question_order, ' .
		 'active,'.
		 'question_text, ' .
		 'notes, ' .
		 'display_type '.
		 'FROM "question_text" where question_id=$1';
		
		$result = pg_query_params ( $dbconn, $query, array (
				$row ["question_id"] 
		) );
		
		if (! $result) {
			throw new Exception ( "Internal server error" );
		}
		
		$row = pg_fetch_assoc ( $result );
		$response ["data"] = array(
			"question_id" => intval($row["question_id"]),
			"category_id" => intval($row["category_id"]),
			"order" => intval($row["question_order"]),
			"active" => $row["active"] == "t",
			"text" => $row["question_text"],
			"notes" => $row["notes"],
			"type" => $row["display_type"]
		
		);
		
		
		pg_free_result ( $result );
		
		
	} catch ( Exception $e ) {
		echo 'Caught exception: ', $e->getMessage (), "\n";
	} finally{
		pg_close ( $dbconn );
	}
	
	return $response;
}

/**
 * Processes PUT Requests
 */
function PUT($parameters) {
	global $user;
	
	//connect to database
	global $sql_connectionString;
	$dbconn = pg_connect ( $sql_connectionString );
	if (! $dbconn) {
		throw new Exception ( "Error connecting to database" );
	}
	
	$response = array (
			"result" => "OK",
			"data" => array () 
	);
	
	try {
		
		//Select statement
		$query = 'SELECT ' .
		 'question_id, ' .
		 'category_id, ' .
		 'question_order, ' .
		 'active,'.
		 'question_text, ' .
		 'notes, ' .
		 'display_type '.
		 'FROM "question_text" where question_id=$1';
		
		$result = pg_query_params ( $dbconn, $query, array (
				$parameters ["question_id"] 
		) );
		
		// return error if needed
		if (! $result) {
			throw new Exception ( "question type not found" );
		}
		
		$row = pg_fetch_assoc ( $result );
		pg_free_result ( $result );
		
		
		$query = 'update "question_text" set ' .
		 'category_id = $1, ' .
		 'question_order = $2, ' .
		 'active = $3, ' .
		 'question_text = $4, ' .
		 'notes = $5, ' .
		 'display_type = $6, ' .
		 'modified_by = $7 ' .
		 'where question_id = $8 ';
		
		// $result = pg_query($dbconn,$query);
		$result = pg_query_params ( $dbconn, $query, array (
				$parameters ["category_id"],
				$parameters ["order"],
				((!$parameters ["active"])?0:1),
				$parameters ["text"],
				$parameters ["notes"],
				$parameters ["type"],
				$user["username"],
				$parameters ["question_id"] 
		) );
		
		if (! $result) {
			throw new Exception ( "Internal server error" );
		}
		
		//check if anything changed
		$rowsAffected = pg_affected_rows ( $result );
		pg_free_result ( $result );	
		if ($rowsAffected != 1) {
			return array (
					"result" => "FAILED",
					"error" => "Could not insert question" 
			);
		}
		
		//select statement
		$query = 'SELECT ' .
		 'question_id, ' .
		 'category_id, ' .
		 'question_order, ' .
		 'active,'.
		 'question_text, ' .
		 'notes, ' .
		 'display_type '.
		 'FROM "question_text" where question_id=$1';
		
		$result = pg_query_params ( $dbconn, $query, array (
				$row ["question_id"] 
		) );
		
		if (! $result) {
			throw new Exception ( "Internal server error" );
		}
		
		//populate data using BUILD function (located below)
		$row = pg_fetch_assoc ( $result );
		$response ["data"] = array(
			"question_id" => intval($row["question_id"]),
			"category_id" => intval($row["category_id"]),
			"order" => intval($row["question_order"]),
			"active" => $row["active"] == "t",
			"text" => $row["question_text"],
			"notes" => $row["notes"],
			"type" => $row["display_type"]
		
		);
		
		
		pg_free_result ( $result );
		
		//catch errors
	} catch ( Exception $e ) {
		echo 'Caught exception: ', $e->getMessage (), "\n";
		
		//close connection
	} finally{
		pg_close ( $dbconn );
	}
	return $response;
}

/**
 * Processes DELETE Requests
 */
function DELETE($parameters) {
	//connect to database
	global $sql_connectionString;
	$dbconn = pg_connect ( $sql_connectionString );

	if (! $dbconn) {
		throw new Exception ( "Error connecting to database" );
	}
	
	try {
		
		//delete query 
		$query = 'delete from "question_text" where question_id = $1 ';
		// pass in parameters...example $1 = $parameters ["question_id"] 
		$result = pg_query_params ( $dbconn, $query, array (
				$parameters ["question_id"] 
		) );
		if (! $result) {
			throw new Exception ( "Internal server error" );
		}
		
		//check if anything changed
		$rowsAffected = pg_affected_rows ( $result );
		pg_free_result ( $result );
		if ($rowsAffected != 1) {
			return array (
					"result" => "FAILED",
					"error" => "Could not delete specified question" 
			);
		}		
		return array (
				"result" => "OK" 
		);
		
		//catch errors
	} catch ( Exception $e ) {
		echo 'Caught exception: ', $e->getMessage (), "\n";
		
		//close connection
	} finally{
		pg_close ( $dbconn );
	}
	
	return $response;
}

//this function gets all questions and category for given casebook_type_id
function Build($db, $casebook_type_id) {
	
	//initialzie array with questions as an array also
	$build = array (
			"casebook_type_id" => intval ( $casebook_type_id ["casebook_type_id"] ),
			"category_id" => intval ( $casebook_type_id ["category_id"] ),
			"questions" => array () 
	);
	
	//get questions with getQuestionFor function (located below)
	$build ["questions"] = getQuestionsFor ( $db, intval ( $casebook_type_id ["category_id"] ) );
	
	return $build;
}

//this function returns a single question with given question_id
function getQuestion($db, $question_id) {
	
	//select statement
	$query = 'SELECT ' .
	 'question_id, ' .
	 'category_id ' .
	 'FROM "question_text" where question_id=$1';
	
	//pass query parameters... example $1 = $question_id
	$result = pg_query_params ( $db, $query, array (
			$question_id 
	) );
	
	if (! $result) {
		throw new Exception ( "question not found" );
	}
	
	//check if question exists
	$row = null;
	if (! ($row = pg_fetch_assoc ( $result ))) {
		throw new Exception ( "question not found" );
	}
	
	pg_free_result ( $result );
	
	//return parameters
	return array (
			"question_id" => intval ( $row ["question_id"] ),
			"category_id" => intval ( $row ["category_id"] ) 
	);
}
function getQuestionsFor($db, $category_id) {
	//select statement for category_id
	$query = 'SELECT ' .
	 'category_id, ' .
	 'question_id ' .
	 'FROM "question_text" where category_id=$1';
	
	//this is for returning empty null because if $category_id exists in DATABASE table "question_category"
	//and $category id does not exist in DATABASE table "question_text"
	if ($category_id == null) {
		return array ();
	}
	
	//pass parameters...example $1 = $category_id
	$result = pg_query_params ( $db, $query, array (
			$category_id 
	) );
	if (! $result) {	
		throw new Exception ( "category not found" );
	}
	$row = null;
	
	//populates data
	while ( $row = pg_fetch_assoc ( $result ) ) {
		$questions [] = array (
				"category_id" => intval ( $row ["category_id"] ),
				"question_id" => intval ( $row ["question_id"] ) 
		);
	}
	
	pg_free_result ( $result );
	
	return $questions;
}

?>
