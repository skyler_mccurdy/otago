<?php
/**
*	Used as a template for API end points
*
*	Copy this file to new location and adjust include paths
*
*/
include "../../secure.php";
include "../validation.php";
include "../json.php";
include "../framework.php";

/**
*	Define the allowed parameters and their default values
*
*	Parameters to GET requests will be filled from query parameters
*	Parameters to all other requests will be loaded from a JSON body
*
*/
function defineParameters(){
	switch($_SERVER['REQUEST_METHOD']){
	case "GET":
		return array(
				"casebook_id" => null,
				"name" => null,
				"page" => null,
				"pageSize" => null,
				"orderBy" => "name"
			);
		break;
	case "POST":
		return array(
				"name" => null
			);
		break;
	case "PUT":
		return array(
				"casebook_id" => null,
				"name" => null
			);
		break;
	case "DELETE":
		return array(
				"casebook_id" => null
			);
		break;
	default:
		return null;
	}
}

/**
*	Validates the parameters
*
*	Should validate parameters using functions from validation.php
*	Returns an array of errors
*
*/
function validate(&$parameters){
	$errors = array();

	switch($_SERVER['REQUEST_METHOD']){
		case "GET":
			if(!Validation\Template::casebook_id($parameters["casebook_id"])){
				$errors["casebook_id"]="invalid casebook id";
			}
			if(!Validation\Template::casebookName($parameters["name"])){
				$errors["name"]="invalid casebook name";
			}
			if(!Validation\Template::page($parameters["page"])){
				$errors["page"]="invalid page number";
			}
			if(!Validation\Template::pageSize($parameters["pageSize"])){
				$errors["pageSize"]="invalid page size";
			}
			break;
		case "POST":
			if($parameters["name"] != null and !Validation\Template::casebookName($parameters["name"])){
				$errors["name"]="invalid casebook name";
			}
			break;
		case "PUT":
			if($parameters["casebook_id"] != null and !Validation\Template::casebook_id($parameters["casebook_id"])){
				$errors["casebook_id"]="invalid casebook id";
			}
			if($parameters["name"] != null and !Validation\Template::casebookName($parameters["name"])){
				$errors["name"]="invalid casebook name";
			}
			break;
		case "DELETE":
			if($parameters["casebook_id"] != null and !Validation\Template::casebook_id($parameters["casebook_id"])){
				$errors["casebook_id"]="invalid casebook id";
			}
			break;
		default:
			return $errors["call"]="Invalid method call";
	}
}

/**
*	Processes GET Requests
*
*/
function GET($parameters){
	global $sql_connectionString;
	global $user;
	
	$response = array(
			"result" => "OK"
	);
		
	$db = pg_connect($sql_connectionString);
	if(!$db){
		throw new Exception("Error connecting to database");
	}
	try{
	
		if($user["privilegeLevel"] != "I"){
			http_response_code(301);
			return array(
					"result" => "OK",
					"error" => "User does not have the required permissions"
			);
		}
	
		//returns a single casebook if id parameter is specified or a list of casebook templates if not
		if ($parameters["casebook_id"]){
			// Performing SQL query
			$query = 'select '.
						'Casebook_Type_ID, '.
						'Casebook_Name '.
				        ' from "casebook" where Casebook_Type_ID=$1';
			$result = pg_query_params($db,$query,array($parameters["casebook_id"]));
			// return error if needed
			if(!$result){
				throw new Exception("Casebook type not found");
			}
			
			$row = pg_fetch_assoc($result);
			pg_free_result($result);
				
			if($row == null){
				return array(
						"result" => "FAILED",
						"error" => "Casebook type not found"
				);
			}
			// return array
			$response["data"] = buildCasebook($db,$row);
		}
		else {
		

			$query = 'select '.
						'Casebook_Type_ID, '.
						'Casebook_Name '.
				' from "casebook"';
			$whereStatement = "";
			$whereParams = array();

			if ($parameters["name"] != null && $parameters["name"] != "" && $parameters["name"] != "undefined" && $parameters["name"] != "null"){
				$whereStatement .= " casebook_name ilike $".(count($whereParams)+1)." ";
				$whereParams[] = "%".$parameters["name"]."%";
			}
			
			if(strlen($whereStatement) > 0){
				$whereStatement = " where " . $whereStatement;
			}
			
			$result = pg_query_params($db,"select count(*) ".
				' from "casebook" '.
				$whereStatement,$whereParams);
			if(!$result){
				throw new Exception("Internal Server Error");
			}
			
			$response["total"] = intval(pg_fetch_array($result)[0]);
			
			pg_free_result($result);
			
			
			if($parameters["orderBy"] !=null){
				$orderBys = explode(":",$parameters["orderBy"]);
				if(count($orderBys)>0){
					if ($orderBys[0] == "name")
					{
						if($orderBys[1]=='desc'){
							$whereStatement = $whereStatement . " order by Casebook_Name desc";
						}else{
							$whereStatement = $whereStatement . " order by Casebook_Name ";
						}
					}
					else
						throw new Exception("Invalid order");
				}
			}
			
			if( ($parameters["page"] != null && $parameters["page"] != "" && $parameters["page"] != "undefined") 
				|| ($parameters["pageSize"] != null && $parameters["pageSize"] != "" && $parameters["pageSize"] != "undefined")){
				$page = ($parameters["page"]>=1)?$parameters["page"]:1;
				$pageSize = ($parameters["pageSize"]>=1)?$parameters["pageSize"]:10;
			
				$response["page"] = intval($page);
				$response["pageSize"] = intval($pageSize);
			
				$whereStatement = $whereStatement . " limit $".(count($whereParams)+1)." offset $".(count($whereParams)+2)." ";
				$page = ($page-1) * $pageSize;
				$whereParams[] = $pageSize;
				$whereParams[] = $page;
			}
				
			$result = pg_query_params($db,$query.$whereStatement,$whereParams);
			if(!$result){
				throw new Exception("Invalid username or password");
			}
				
			$response["count"] = pg_num_rows($result);
				
			$response["data"]=array();
			while($row = pg_fetch_assoc($result)){
				$response["data"][] = buildCasebook($db,$row);
			}
				
			pg_free_result($result);
		}
	} 
	finally {
		pg_close($db);
	}
	
	
	return $response;
}

/**
*	Processes POST Requests
*
*/
function POST($parameters){
	global $sql_connectionString;
	global $user;
	
	$response = array(
			"result" => "OK",
			"data" => array()
	);
		
	$db = pg_connect($sql_connectionString);
	if(!$db){
		throw new Exception("Error connecting to database");
	}
	try{
		if($user["privilegeLevel"] != "I"){
			http_response_code(301);
			return array(
					"result" => "OK",
					"error" => "User does not have the required permissions"
			);
		}
		// Insert into database
		$query = "INSERT INTO Casebook (Casebook_Name, Active, Modified_By, Date_Modified)
	              VALUES ($1, TRUE, $2, now())"
				  . ' returning casebook_type_id ';
		$result = pg_query_params($db,$query,array($parameters["name"], $user["username"]));
		if(!$result){
			return array(
					"result" => "FAILED",
					"error" => "Template already exists"
			);
		}	
		$row = pg_fetch_assoc($result);
		pg_free_result($result);
		
		if($row == null or !array_key_exists("casebook_type_id",$row)){
			return array(
					"result" => "FAILED",
					"error" => "Could not insert user casebook"
			);
		}
		// Performing SQL query
		$query = 'select '.
				'Casebook_Type_ID, '.
				'Casebook_Name '.
				' from "casebook" where Casebook_Name=$1';
		$result = pg_query_params($db,$query,array($parameters["name"]));
		// return error if needed
		if(!$result){
			throw new Exception("Internal server error");
		}
			
		$row = pg_fetch_assoc($result);
		pg_free_result($result);
				
		if($row == null){
			return array(
					"result" => "FAILED",
					"error" => "Casebook not found"
			);
		}
		// return array
		$response["data"] = buildCasebook($db,$row);	
	}
	finally {
		pg_close($db);
	}
	
	
	return $response;
}

/**
*	Processes PUT Requests
*
*/
function PUT($parameters){
	global $sql_connectionString;
	global $user;
	
	$response = array(
			"result" => "OK",
			"data" => array()
	);
	
	$db = pg_connect($sql_connectionString);
	if(!$db){
		throw new Exception("Error connecting to database");
	}
	try{
		if($user["privilegeLevel"] != "I"){
			http_response_code(301);
			return array(
					"result" => "OK",
					"error" => "User does not have the required permissions"
			);
		}
		// First select row to update
		$query = 'select '.
				'Casebook_Type_ID, '.
				'Casebook_Name '.
				' from "casebook" where Casebook_Type_ID=$1';
		$result = pg_query_params($db,$query,array($parameters["casebook_id"]));
		// return error if needed
		if(!$result){
			throw new Exception("Casebook type not found");
		}
			
		$row = pg_fetch_assoc($result);
		pg_free_result($result);
		
		// Update casebook
		$query = 'update "casebook" set '.
				'casebook_name = $1, '.
				'modified_by = $3 '.
				'where casebook_type_id = $2 ';
		$result = pg_query_params($db,$query,array(
				$parameters["name"],
				$parameters["casebook_id"],
				$user["username"]
				));
		if(!$result){
			throw new Exception("Internal server error");
		}
		$rowsAffected = pg_affected_rows($result);
		pg_free_result($result);
		
		if($rowsAffected!=1){
			return array(
					"result" => "FAILED",
					"error" => "Could not update casebook"
			);
		}
		
		// Return Updated Casebook
		$query = 'select '.
				'Casebook_Type_ID, '.
				'Casebook_Name '.
				' from "casebook" where Casebook_Type_ID=$1';
		$result = pg_query_params($db,$query,array($parameters["casebook_id"]));
		// return error if needed
		if(!$result){
			throw new Exception("Internal server error");
		}
			
		$row = pg_fetch_assoc($result);
		pg_free_result($result);
	
		if($row == null){
			return array(
					"result" => "FAILED",
					"error" => "Casebook not found"
			);
		}
		// return array
		$response["data"] = buildCasebook($db,$row);
	}
	finally {
		pg_close($db);
	}
	
	
	return $response;
}


/**
*	Processes DELETE Requests
*
*/
function DELETE($parameters){

	global $sql_connectionString;
	global $user;
		
	$db = pg_connect($sql_connectionString);
	if(!$db){
		throw new Exception("Error connecting to database");
	}
	try{
		if($user["privilegeLevel"] != "I"){
			http_response_code(301);
			return array(
					"result" => "OK",
					"error" => "User does not have the required permissions"
			);
		}
		$query = 'delete from "casebook" where casebook_type_id = $1 ';
		$result = pg_query_params($db,$query,array(
				$parameters["casebook_id"],
		));
		if(!$result){
			throw new Exception("Internal server error");
		}
		$rowsAffected = pg_affected_rows($result);
		pg_free_result($result);
	
		if($rowsAffected!=1){
			return array(
					"result" => "FAILED",
					"error" => "Could not delete casebook"
			);
		}
	
		return array(
				"result" => "OK"
		);
	
	
	}finally{
		pg_close($db);
	}
}

function buildCasebook($db, $db_casebook){

	$casebook = array(
					"casebook_id" => intval($db_casebook["casebook_type_id"]),
					"name" => $db_casebook["casebook_name"],
					"categories" => array()
					);
	
	$casebook["categories"] = getCategoriesFor(
			$db,
			$casebook["casebook_id"],
			null
	);
	
	return $casebook;
}

function getCategoriesFor($db,$casebookType_ID,$parent_ID){

	$categories = array();
	
	$query = 'select '.
						'category_id, '.
						'casebook_type_id, '.
						'category_name, '.
						'parent_category_id, '.
						'group_order, '.
						'active, '.
						'display_type, '.
						'notes '.
				" from \"question_category\" where Casebook_Type_ID=$1 ";
	if($parent_ID == null){
		$query = $query . " and parent_category_id is null ";
	}else{
		$query = $query . " and parent_category_id = $2";
	}
	$params = array(
						$casebookType_ID
					);
	if($parent_ID!=null){
		$params[1] = $parent_ID;
	}
	$result = pg_query_params($db,$query,$params);
	if(!$result){
		throw new Exception("Casebook Type not found");
	}
	
	while($row = pg_fetch_assoc($result)){
			$categories[] = array(
									"category_id" => intval($row["category_id"]),
									"casebook_id" => intval($row["casebook_type_id"]),
									"name" => $row["category_name"],
									"parent_id" => ($row["parent_category_id"]==null)?null:intval($row["parent_category_id"]),
									"order" => intval($row["group_order"]),
									"active" => $row["active"]=="t",
									"type" => $row["display_type"],
									"notes" => $row["notes"],
									"categories" => getCategoriesFor($db,$casebookType_ID,intval($row["category_id"])),
									"questions" => getQuestionsFor($db,intval($row["category_id"]))
								);
	}
	
	pg_free_result($result);
	
	return $categories;

}

function getQuestionsFor($db,$category_ID){
	$questions = array();
	
	$query = 'select '.
						'question_id, '.
						'category_id, '.
						'question_order, '.
						'display_type, '.
						'active, '.
						'question_text, '.
						'notes '.
						
				" from \"question_text\" where category_id = $1";
	
	$result = pg_query_params($db,$query,array($category_ID));
	if(!$result){
		throw new Exception("Casebook Type not found");
	}
	
	while($row = pg_fetch_assoc($result)){
			$questions[] = array(
									"question_id" => intval($row["question_id"]),
									"category_id" => intval($row["category_id"]),
									"order" => intval($row["question_order"]),
									"type" => $row["display_type"],
									"active" => $row["active"]=="t",
									"text" => $row["question_text"],
									"notes" => $row["notes"]
								);
	}
	
	pg_free_result($result);
	
	return $questions;
}
?>
