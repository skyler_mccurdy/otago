<?php
/**
*	Used as a template for API end points
*
*	Copy this file to new location and adjust include paths
*
*/
require "../../../secure.php";
include "../../validation.php";
include "../../json.php";
include "../../framework.php";


/**
*	Define the allowed parameters and their default values
*
*	Parameters to GET requests will be filled from query parameters
*	Parameters to all other requests will be loaded from a JSON body
*
*/
function defineParameters(){
	switch($_SERVER['REQUEST_METHOD']){
	case "PUT":
		return array(
				"answer_id" => null,
				"feedback" => null
			);
		break;
	default:
		return null;
	}
}

/**
*	Validates the parameters
*
*	Should validate parameters using functions from validation.php
*	Returns an array of errors
*
*/
function validate(&$parameters){
	$errors = array();

	
	switch($_SERVER['REQUEST_METHOD']){
	case "PUT":
	
			if($parameters["answer_id"] != null and !Validation\Answer::answer_ID($parameters["answer_id"])){
				$errors["answer_id"]="invalid answer id";
			}
			if($parameters["feedback"] != null and !Validation\Answer::feedback($parameters["feedback"])){
				$errors["feedback"]="invalid feedback";
			}
	
			return $errors;
		break;
	default:
		return $errors["call"]="Invalid method call";
	}

}

/**
*	Processes PUT Requests
*
*/
function PUT($parameters){

	global $sql_connectionString;
	global $user;

	$response = array(
						"result" => "OK",
						"data" => array()
					);
					
					
					
					
	$db = pg_connect($sql_connectionString);
	if(!$db){
		throw new Exception("Error connecting to database");
	}				
	try{
		if($user["privilegeLevel"] != "I"){
			http_response_code(301);
			return array(
					"result" => "OK",
					"error" => "User does not have the required permissions"
			);
		}
	
		$query = 'select '.
							'answer_id, '.
							'user_casebook_id, '.
							'question_id, '.
							'answer_text, '.
							'feedback '.
					' from "answer" where answer_id=$1';
		$result = pg_query_params($db,$query,array($parameters["answer_id"]));
		if(!$result){
			throw new Exception("Answer not found");
		}
		$row = pg_fetch_assoc($result);
		pg_free_result($result);
		
		$query = 'update "answer" set '.
							'feedback = $2, '.
							'feedback_modified_by = $3, '. 
							'feedback_modified_on = now() '.
							'where answer_id = $1 ';
		$result = pg_query_params($db,$query,array(
												$parameters["answer_id"],
												$parameters["feedback"],
											    $user["username"]
												));
		if(!$result){
			throw new Exception("Internal server error");
		}
		$rowsAffected = pg_affected_rows($result);
		pg_free_result($result);
		if($rowsAffected!=1){
			return array(
							"result" => "FAILED",
							"error" => "Could not update feedback"
						);
		}
		
		$query = 'select '.
							'answer_id, '.
							'user_casebook_id, '.
							'question_id, '.
							'answer_text, '.
							'feedback '.
					' from "answer" where answer_id=$1';
		$result = pg_query_params($db,$query,array($parameters["answer_id"]));
		if(!$result){
			throw new Exception("Internal server error");
		}
		$row = pg_fetch_array($result);
		pg_free_result($result);
		
		if($row == null){
			return array(
							"result" => "FAILED",
							"error" => "Answer not found"
						);
		}
		
		$response["data"] = array(
								"answer_id" => $row[0],
								"userCasebook_id" => $row[1],
				                "question_id" => $row[2],
				                "text" => $row[3],
				                "feedback" => $row[4]
				            );
	
		return $response;
	}finally{
		pg_close($db);
	}
	
	return $response;
}

?>
