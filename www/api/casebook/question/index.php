<?php
/**
 *	Used as a template for API end points
 *
 *	Copy this file to new location and adjust include paths
 *
 */
include "../../../secure.php";
include "../../validation.php";
include "../../json.php";
include "../../framework.php";

/**
 * Define the allowed parameters and their default values
 *
 * Parameters to GET requests will be filled from query parameters
 * Parameters to all other requests will be loaded from a JSON body
 */
function defineParameters() {
	switch ($_SERVER ['REQUEST_METHOD']) {
		case "GET" :
			return array (
					"userCasebook_id" => null,
					"answer_id" => null,
					"question_id" => null,
					"page" => null,
					"pageSize" => null, 
					"orderBy" => "order"
			);
			break;
		case "POST" :
			return array (
					"userCasebook_id" => null,
					"question_id" => null,
					"text" => "" 
			);
			break;
		case "PUT" :
			return array (
					"answer_id" => null,
					"text" => "" 
			);
			break;
		case "DELETE" :
			return array (
					"answer_id" => null 
			);
			break;
		default :
			return null;
	}
}

/**
 * Validates the parameters
 *
 * Should validate parameters using functions from validation.php
 * Returns an array of errors
 */
function validate(&$parameters) {
// 	var_dump($parameters);
	$errors = array ();
	
	switch ($_SERVER ['REQUEST_METHOD']) {
		case "GET" :
			// if ($parameters ["userCasebook_id"] != null and ! Validation\Casebook::userCasebook_id ( $parameters ["userCasebook_id"] )) {
			// $errors ["userCasebook_id"] = "invalid user user casebook id";
			// }
			if ($parameters ["userCasebook_id"] != null and ! Validation\Answer::userCasebook_id ( $parameters ["userCasebook_id"] )) {
				$errors ["userCasebook_id"] = "invalid user user casebook id";
			}
			if ($parameters ["answer_id"] != null and ! Validation\Answer::answer_ID ( $parameters ["answer_id"] )) {
				$errors ["answer_id"] = "invalid user answer id";
			}
			
			if ($parameters ["question_id"] != null and ! Validation\Answer::question_ID ( $parameters ["question_id"] )) {
				$errors ["question_id"] = "invalid user question id";
			}
			
			if ($parameters ["page"] != null and ! Validation\Generic::page ( $parameters ["page"] )) {
				$errors ["page"] = "invalid page number";
			}
			if ($parameters ["pageSize"] != null and ! Validation\Generic::pageSize ( $parameters ["pageSize"] )) {
				$errors ["pageSize"] = "invalid page size";
			}
			return $errors;
			break;
		case "POST" :
			if ($parameters ["userCasebook_id"] == null or ! Validation\Answer::userCasebook_id ( $parameters ["userCasebook_id"] )) {
				$errors ["userCasebook_id"] = "invalid user user casebook id";
			}
			if ($parameters ["question_id"] == null or ! Validation\Answer::question_ID ( $parameters ["question_id"] )) {
				$errors ["question_id"] = "invalid user question id";
			}
			if ($parameters ["text"] != null and ! Validation\Answer::answer_text ( $parameters ["text"] )) {
 				$errors ["text"] = "invalid answer";
			}
			
			return $errors;
			break;
		
		case "PUT" :
			
			if ($parameters ["answer_id"] != null and ! Validation\Answer::answer_ID ( $parameters ["answer_id"] )) {
				$errors ["answer_id"] = "invalid user answer id";
			}
			if ($parameters ["text"] != null and ! Validation\Answer::answer_text ( $parameters ["text"] )) {
				$errors ["text"] = "invalid answer";
			}
			
			return $errors;
			break;
		case "DELETE" :
			
			if ($parameters ["answer_id"] != null and ! Validation\Answer::answer_ID ( $parameters ["answer_id"] )) {
				$errors ["answer_id"] = "invalid user answer id";
			}
			
			return $errors;
			break;
		
		default :
			return $errors ["call"] = "Invalid method call";
	}
	
	return $errors;
}

/**
 * Processes GET Requests
 */
function GET($parameters) {
		global $sql_connectionString;
	
	
	$dbconn = pg_connect ($sql_connectionString )
	 or die ( 'Could not connect: ' . pg_last_error () );
	
	if (! $dbconn) {
		throw new Exception ( "Error connecting to database" );
	}
	
	$response = array (
			"result" => "OK",
			"data" => array () 
	);
	
	try {
		if ($parameters ["userCasebook_id"]) {
			$query = 'SELECT ' .
			 'user_casebook_id, ' .
			 'answer_id, ' .
			 'question_id, ' .
			 'answer_text, ' .
			 'feedback ' .
			 'FROM "answer" where user_casebook_id=$1';
			
			$result = pg_query_params ( $dbconn, $query, array (
					$parameters ["userCasebook_id"] 
			) );
			
			if (! $result) {
				throw new Exception ( "Internal server error" );
			}
			
			$arr = pg_fetch_all ( $result );
			pg_free_result($result);
				
			for($i = 0; $i < count ( $arr ); $i ++) {
				$response ["data"] [$i] ["userCasebook_id"] = $arr [$i] ["user_casebook_id"];
				$response ["data"] [$i] ["answer_id"] = $arr [$i] ["answer_id"];
				$response ["data"] [$i] ["question_id"] = $arr [$i] ["question_id"];
				$response ["data"] [$i] ["text"] = $arr [$i] ["answer_text"];
				$response ["data"] [$i] ["feedback"] = $arr [$i] ["feedback"];
			}
						pg_free_result($arr);
			
		}
		else{
			$query = 'SELECT ' .
					'user_casebook_id, ' .
					'answer_id, ' .
					'question_id, ' .
					'answer_text, ' .
					'feedback ' .
					'FROM "answer" ';
			
			$whereStatement = "";
			$whereParams = array();
			
			
			

			if($parameters["answer_id"]!=null){
				if(strlen($whereStatement) > 0){
					$whereStatement = $whereStatement . " and ";
				}
				$whereStatement = $whereStatement . " answer_id = $".(count($whereParams)+1)." ";
				$whereParams[] = $parameters["answer_id"];
			} 

			
			if($parameters["question_id"]!=null){
				if(strlen($whereStatement) > 0){
					$whereStatement = $whereStatement . " and ";
				}
				$whereStatement = $whereStatement . " question_id = $".(count($whereParams)+1)." ";
				$whereParams[] = $parameters["question_id"];
			}
			
			if($parameters["text"]!=null){
				if(strlen($whereStatement) > 0){
					$whereStatement = $whereStatement . " and ";
				}
				$whereStatement = $whereStatement . " text = $".(count($whereParams)+1)." ";
				$whereParams[] = $parameters["text"];
			}
			if($parameters["feedback"]!=null){
				if(strlen($whereStatement) > 0){
					$whereStatement = $whereStatement . " and ";
				}
				$whereStatement = $whereStatement . " feedback = $".(count($whereParams)+1)." ";
				$whereParams[] = $parameters["feedback"];
			}
			
			if(strlen($whereStatement) > 0){
				$whereStatement = " where " . $whereStatement;
			}
			
			
				
			
			if($parameters["page"] != null || $parameters["pageSize"]){
				$page = ($parameters["page"]!=null)?$parameters["page"]:1;
				$pageSize = ($parameters["pageSize"]!=null)?$parameters["pageSize"]:10;
			
				$result["page"] = $page;
				$result["pageSize"] = $pageSize;
				$whereStatement = $whereStatement . " limit $".(count($whereParams)+1)." offset $".(count($whereParams)+2)." ";
				$page = ($page-1) * $pageSize;
				$whereParams[] = $pageSize;
				$whereParams[] = $page;
			}
			
			$result = pg_query_params($dbconn,$query.$whereStatement,$whereParams);
			
			$arr = pg_fetch_all ( $result );
			pg_free_result($result);

			
			for($i = 0; $i < count ( $arr ); $i ++) {
				$response ["data"] [$i] ["userCasebook_id"] = $arr [$i] ["user_casebook_id"];
				$response ["data"] [$i] ["answer_id"] = $arr [$i] ["answer_id"];
				$response ["data"] [$i] ["question_id"] = $arr [$i] ["question_id"];
				$response ["data"] [$i] ["text"] = $arr [$i] ["answer_text"];
				$response ["data"] [$i] ["feedback"] = $arr [$i] ["feedback"];
			}
		
		}
	} finally{
		pg_close ( $dbconn );
	}
	
	return $response;
}

/**
 * Processes POST Requests
 */
function POST($parameters) {
	global $sql_connectionString;
	
	
	$dbconn = pg_connect ($sql_connectionString );
	
	if (! $dbconn) {
		throw new Exception ( "Error connecting to database" );
	}
	
	$response = array (
			"result" => "OK",
			"data" => array () 
	);
	try {
		$query = 'INSERT INTO answer ' .
				 '(user_casebook_id, question_id, answer_text, feedback, modified_by) ' .
		 "VALUES ( $1, $2, $3, null, 'ocsWeb')" .
		 ' returning answer_id;';
		
		
		$result = pg_query_params ( $dbconn, $query, array (
				$parameters ["userCasebook_id"],
				$parameters ["question_id"],
				$parameters ["text"] 
		) );
		
		if (! $result) {
			throw new Exception ( "Internal server error" );
		}
		
		$row = pg_fetch_assoc ( $result );
		pg_free_result ( $result );
		
		if ($row == null or ! array_key_exists ( "answer_id", $row )) {
			return array (
					"result" => "FAILED",
					"error" => "Could not insert answer" 
			);
		}
		
		
		$query = 'SELECT ' .
		 'user_casebook_id, ' .
		 'answer_id, ' .
		 'question_id, ' .
		 'answer_text, ' .
		 'feedback ' .
		 'FROM "answer" where answer_id=$1';
		
		$result = pg_query_params ( $dbconn, $query, array (
				$row ["answer_id"] 
		) );
		
		if (! $result) {
			throw new Exception ( "Internal server error" );
		}
	
		
		$row = pg_fetch_assoc ( $result );
		pg_free_result ( $result );
		
		$response ["data"] = array( 
									"userCasebook_id" => $row ["user_casebook_id"],
									"answer_id" => $row ["answer_id"],
									"question_id" => $row ["question_id"],
									"text" => $row ["answer_text"],
									"feedback" => $row ["feedback"]
									);
		
	} finally{
		pg_close ( $dbconn );
	}
	
	return $response;
}

/**
 * Processes PUT Requests
 */
function PUT($parameters) {
	global $sql_connectionString;
	global $user;
	
	
	$dbconn = pg_connect ($sql_connectionString );
	
	if (! $dbconn) {
		throw new Exception ( "Error connecting to database" );
	}
	
	$response = array (
			"result" => "OK",
			"data" => array () 
	);
	try {
		$query = 'SELECT ' .
		 'user_casebook_id, ' .
		 'answer_id, ' .
		 'question_id, ' .
		 'answer_text, ' .
		 'feedback ' .
		 'FROM "answer" where answer_id=$1';
		
		$result = pg_query_params ( $dbconn, $query, array (
				$parameters ["answer_id"] 
		) );
		if (! $result) {
			throw new Exception ( "Internal server error" );
		}
		
		$row = pg_fetch_assoc ( $result );
		pg_free_result ( $result );
		
		$query = 'UPDATE "answer" set ' .
		 'answer_text = $2, ' .
		 'date_modified = now(), '.
		 'modified_by = $3 '.
		 'WHERE answer_id = $1 ';
		
		$result = pg_query_params ( $dbconn, $query, array (
				$parameters ["answer_id"],
				$parameters ["text"],
				$user['username']
		) );
		
		if (! $result) {
			throw new Exception ( "Internal server error" );
		}
		
		$rowsAffected = pg_affected_rows ( $result );
		pg_free_result ( $result );
		
		if ($rowsAffected != 1) {
			return array (
					"result" => "FAILED",
					"error" => "Could not insert answer" 
			);
		}
		
		$query = 'SELECT ' .
		 'user_casebook_id, ' .
		 'answer_id, ' .
		 'question_id, ' .
		 'answer_text, ' .
		 'feedback ' .
		 'FROM "answer" where answer_id=$1';
		
		$result = pg_query_params ( $dbconn, $query, array (
				$row ["answer_id"] 
		) );
		
		if (! $result) {
			throw new Exception ( "Internal server error" );
		}
	
		
		$row = pg_fetch_assoc ( $result );
		pg_free_result ( $result );
		
		$response ["data"] = array( 
									"userCasebook_id" => $row ["user_casebook_id"],
									"answer_id" => $row ["answer_id"],
									"question_id" => $row ["question_id"],
									"text" => $row ["answer_text"],
									"feedback" => $row ["feedback"]
									);
	} finally{
		pg_close ( $dbconn );
	}
	return $response;
}

/**
 * Processes DELETE Requests
 */
function DELETE($parameters) {
		global $sql_connectionString;
	
	
	$dbconn = pg_connect ($sql_connectionString );
	
	if (! $dbconn) {
		throw new Exception ( "Error connecting to database" );
	}
	
	$response = array (
			"result" => "OK",
			"data" => array () 
	);
	try {
		$query = 'delete from "answer" where answer_id = $1 ';
		
		$result = pg_query_params ( $dbconn, $query, array (
				$parameters ["answer_id"] 
		) );
		if (! $result) {
			throw new Exception ( "Internal server error" );
		}
		
		$rowsAffected = pg_affected_rows ( $result );
		pg_free_result ( $result );
		
		if ($rowsAffected != 1) {
			return array (
					"result" => "FAILED",
					"error" => "Could not delete specified answer" 
			);
		}
		
		return array (
				"result" => "OK" 
		);
	} finally{
		pg_close ( $dbconn );
	}
	
	return $response;
}

?>
