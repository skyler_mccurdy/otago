<?php
/**
*	Used as a template for API end points
*
*	Copy this file to new location and adjust include paths
*
*/
require "../../secure.php";
include "../validation.php";
include "../json.php";
include "../framework.php";


/**
*	Define the allowed parameters and their default values
*
*	Parameters to GET requests will be filled from query parameters
*	Parameters to all other requests will be loaded from a JSON body
*
*/
function defineParameters(){
	switch($_SERVER['REQUEST_METHOD']){
	case "GET":
		return array(
				"userCasebook_ID" => null,
				"casebook_ID" => null,
				"user_ID" => null,
				"page" => null,
				"pageSize" => null,
				"order" => "user_id,casebook_id",
				"search" => null
			);
		break;
	case "POST":
		return array(
				"casebook_ID" => null,
				"user_ID" => null,
				"dueDate" => null				
			);
		break;
	case "PUT":
		return array(
				"userCasebook_ID" => null,
				"dateSubmitted" => null,
				"dueDate" => null,
				"finalGrade" => null,
				
			);
		break;
	case "DELETE":
		return array(
				"userCasebook_ID" => null,
			);
		break;
	default:
		return null;
	}
}

/**
*	Validates the parameters
*
*	Should validate parameters using functions from validation.php
*	Returns an array of errors
*
*/
function validate(&$parameters){
	$errors = array();

	
	switch($_SERVER['REQUEST_METHOD']){
	case "GET":
			if($parameters["userCasebook_ID"]!=null and !Validation\Casebook::userCasebook_ID($parameters["userCasebook_ID"])){
				$errors["userCasebook_ID"]="invalid user casebook id";
			}
			if($parameters["casebook_ID"]!=null and !Validation\Casebook::casebook_ID($parameters["casebook_ID"])){
				$errors["casebook_id"]="invalid casebook id";
			}
			if($parameters["user_ID"]!=null and !Validation\Casebook::user_ID($parameters["user_ID"])){
				$errors["user_id"]="invalid user id";
			}
			if($parameters["page"]!=null and !Validation\Generic::page($parameters["page"])){
				$errors["page"]="invalid page number";
			}
			if($parameters["pageSize"]!=null and !Validation\Generic::pageSize($parameters["pageSize"])){
				$errors["pageSize"]="invalid page size";
			}
			return $errors;
		break;
	case "POST":
			if(!Validation\Casebook::casebook_ID($parameters["casebook_ID"])){
				$errors["casebook_id"]="invalid casebook id";
			}
			if(!Validation\Casebook::user_ID($parameters["user_ID"])){
				$errors["user_id"]="invalid user id";
			}
			if(!Validation\Casebook::dateDue($parameters["dueDate"])){
				$errors["dueDate"]="invalid due date";
			}
	
			return $errors;
		break;
	case "PUT":
	
			if(!Validation\Casebook::userCasebook_ID($parameters["userCasebook_ID"])){
				$errors["casebook_id"]="invalid casebook id";
			}
			if($parameters["dateSubmitted"] != null and !Validation\Casebook::dateSubmitted($parameters["dateSubmitted"])){
				$errors["dueDate"]="invalid date submitted";
			}
			if($parameters["dueDate"] != null and !Validation\Casebook::dateDue($parameters["dueDate"])){
				$errors["dueDate"]="invalid due date";
			}
			if($parameters["finalGrade"] != null and !Validation\Casebook::finalGrade($parameters["finalGrade"])){
				$errors["dueDate"]="invalid final grade";
			}
	
			return $errors;
		break;
	case "DELETE":
			if(!Validation\Casebook::userCasebook_ID($parameters["userCasebook_ID"])){
				$errors["casebook_id"]="invalid casebook id";
			}
			
			return $errors;
		break;
	default:
		return $errors["call"]="Invalid method call";
	}

}

/**
*	Processes GET Requests
*
*/
function GET($parameters){
	
	global $user;
	global $sql_connectionString;

	$response = array(
						"result" => "OK"
					);
					
					
					
					
	$db = pg_connect($sql_connectionString);
	if(!$db){
		throw new Exception("Error connecting to database");
	}				
	try{
		if($parameters["userCasebook_ID"]){
			$query = 'select '.
								'user_casebook_id,'.
								'Casebook_Type_ID,'.
								'person_ID,'.
								'Date_Started,'.
								'Date_Due,'.
								'Date_Submitted,'.
								'Final_Grade,'.
								'student_last_viewed, '.
								'instructor_last_viewed '.
						' from "user_casebook" where User_Casebook_ID=$1';
			$result = pg_query_params($db,$query,array($parameters["userCasebook_ID"]));
			if(!$result){
				throw new Exception("Internal server error");
			}
			$row = pg_fetch_assoc($result);
			pg_free_result($result);
			
			
			if($row == null or ( $user["user_id"] != $row["person_id"] and $user["privilegeLevel"] != "I" ) ){
				return array(
								"result" => "FAILED",
								"error" => "Casebook not found"
							);
			}
			
			$response["data"] = buildCasebook($db,$row);
			
			if($user["privilegeLevel"]=='S'){
				$query = 'update "user_casebook" set student_last_viewed=now() where user_casebook_id=$1';
				$result = pg_query_params($db,$query,array(
															$parameters["userCasebook_ID"]
														));
				if(!$result){
					throw new Exception("Internal server error");
				}
			}
			
			if($user["privilegeLevel"]=='I'){
				$query = 'update "user_casebook" set instructor_last_viewed=now() where user_casebook_id=$1';
				$result = pg_query_params($db,$query,array(
															$parameters["userCasebook_ID"]
														));
				if(!$result){
					throw new Exception("Internal server error");
				}
			}
		
		}else{
		
		
			$query = 'select '.
								'user_casebook_id,'.
								'uc.Casebook_Type_ID,'.
								'uc.person_ID,'.
								'Date_Started,'.
								'Date_Due,'.
								'Date_Submitted,'.
								'Final_Grade, '.
								'student_last_viewed, '.
								'instructor_last_viewed '.
						' from "user_casebook" uc '.
						' left join "person" u on uc.person_id = u.person_id '.
						' left join "casebook" c on uc.casebook_type_id = c.casebook_type_id ';
			$whereStatement = "";
			$whereParams = array();
			
			if($parameters["casebook_ID"]!=null){
				if(strlen($whereStatement) > 0){
					$whereStatement = $whereStatement . " and ";
				}
				$whereStatement = $whereStatement . " Casebook_Type_ID = $".(count($whereParams)+1)." ";
				$whereParams[] = $parameters["casebook_ID"];
			}
			
			if($parameters["user_ID"]!=null){
				if(strlen($whereStatement) > 0){
					$whereStatement = $whereStatement . " and ";
				}
				$whereStatement = $whereStatement . " person_ID = $".(count($whereParams)+1)." ";
				$whereParams[] = $parameters["user_ID"];
			}
			
			if($parameters["search"]!=null&&$parameters["search"]!="undefined"&&$parameters["search"]!=""){
				if(strlen($whereStatement) > 0){
					$whereStatement = $whereStatement . " and ";
				}
				$whereStatement .= " (";
				$whereStatement .= " c.casebook_name ilike $".(count($whereParams)+1)." or ";
				$whereStatement .= " u.first_name ilike $".(count($whereParams)+1)." or ";
				$whereStatement .= " u.last_name ilike $".(count($whereParams)+1)." ";
				$whereStatement .= ") ";
				$whereParams[] = "%".$parameters["search"]."%";
			}
			
			if($user["privilegeLevel"] != "I"){
				if(strlen($whereStatement) > 0){
					$whereStatement = $whereStatement . " and ";
				}
				$whereStatement = $whereStatement . " uc.person_ID = $".(count($whereParams)+1)." ";
				$whereParams[] = $user["user_id"];
			}
			
			
			
			if(strlen($whereStatement) > 0){
				$whereStatement = " where " . $whereStatement;
			}
			
			$result = pg_query_params($db,"select count(*) ".
				' from "user_casebook" uc '.
				' left join "person" u on uc.person_id = u.person_id '.
				' left join "casebook" c on uc.casebook_type_id = c.casebook_type_id '.
				$whereStatement,$whereParams);
			if(!$result){
				throw new Exception("Invalid username or password");
			}
			
			$response["total"] = intval(pg_fetch_array($result)[0]);
			
			pg_free_result($result);
			
			if($parameters["order"] !=null){
				$orderBys = explode(",",$parameters["order"]);
				if(count($orderBys)>0){
					$whereStatement = $whereStatement . " order by ". addOrderBy($orderBys[0]) ." ";
					for($i=1;$i<count($orderBys);$i++){
						$whereStatement = $whereStatement . ", ".addOrderBy($orderBys[$i])." ";
					}
				}
			}
			
			if($parameters["page"] != null || $parameters["pageSize"]){
				$page = ($parameters["page"]!=null)?$parameters["page"]:1;
				$pageSize = ($parameters["pageSize"]!=null)?$parameters["pageSize"]:10;
				
				$response["page"] = intval($page);
				$response["pageSize"] = intval($pageSize);
				
				$whereStatement = $whereStatement . " limit $".(count($whereParams)+1)." offset $".(count($whereParams)+2)." ";
				$page = ($page-1) * $pageSize;
				$whereParams[] = $pageSize;
				$whereParams[] = $page;
			}
			
			$result = pg_query_params($db,$query.$whereStatement,$whereParams);
			if(!$result){
				throw new Exception("Invalid username or password");
			}
			
			$response["count"] = pg_num_rows($result);
			
			$response["data"]=array();
			while($row = pg_fetch_assoc($result)){
				$response["data"][] = buildCasebook($db,$row);
			}
			
			pg_free_result($result);
			
		}
	}finally{
		pg_close($db);
	}
	

	return $response;
}

/**
*	Processes POST Requests
*
*/
function POST($parameters){
	global $user;
	global $sql_connectionString;

	$response = array(
						"result" => "OK",
						"data" => array()
					);
					
					
					
					
	$db = pg_connect($sql_connectionString);
	if(!$db){
		throw new Exception("Error connecting to database");
	}				
	try{
	
		if($user["privilegeLevel"] != "I"){
			http_response_code(301);
			return array(
					"result" => "OK",
					"error" => "User does not have the required permissions"
					);
		}
		
		$query = 'insert into "user_casebook" '.
							' (casebook_type_id,person_id,casebook_status,date_due,modified_by,date_modified ) values '.
							' ( '.
							'$1, '.
							'$2, '.
							"'New', ".
							'$3, '.
							"$4, ".
							' now() ) '.
							' returning user_casebook_id ';

		$result = pg_query_params($db,$query,array($parameters["casebook_ID"],$parameters["user_ID"],$parameters["dueDate"],$user["username"]));
		
		if(!$result){
			throw new Exception("Internal server error");
		}
		$row = pg_fetch_assoc($result);
		pg_free_result($result);
		
		if($row == null or !array_key_exists("user_casebook_id",$row)){
			return array(
							"result" => "FAILED",
							"error" => "Could not insert user casebook"
						);
		}
		
		$query = 'select '.
							'user_casebook_id,'.
							'Casebook_Type_ID,'.
							'person_ID,'.
							'Date_Started,'.
							'Date_Due,'.
							'Date_Submitted,'.
							'Final_Grade'.
					' from "user_casebook" where User_Casebook_ID=$1';
		$result = pg_query_params($db,$query,array($row["user_casebook_id"]));
		if(!$result){
			throw new Exception("Internal server error");
		}
		$row = pg_fetch_assoc($result);
		pg_free_result($result);
		
		if($row == null){
			return array(
							"result" => "FAILED",
							"error" => "Casebook not found"
						);
		}
		
		$response["data"] = buildCasebook($db,$row);
	
	
	}finally{
		pg_close($db);
	}
	

	return $response;
}

/**
*	Processes PUT Requests
*
*/
function PUT($parameters){

	global $sql_connectionString;
	global $user;

	$response = array(
						"result" => "OK",
						"data" => array()
					);
					
					
					
					
	$db = pg_connect($sql_connectionString);
	if(!$db){
		throw new Exception("Error connecting to database");
	}				
	try{
		if($user["privilegeLevel"] != "I"){
			http_response_code(301);
			return array(
					"result" => "OK",
					"error" => "User does not have the required permissions"
			);
		}
	
		$query = 'select '.
							'person_id, '.
							'user_casebook_id, '.
							'casebook_type_id, '.
							'person_id, '.
							'date_started, '.
							'date_submitted, '.
							'date_due, '.
							'final_grade, '.
							'casebook_status '.
					' from "user_casebook" where User_Casebook_ID=$1';
		$result = pg_query_params($db,$query,array($parameters["userCasebook_ID"]));
		if(!$result){
			throw new Exception("Internal server error");
		}
		$row = pg_fetch_assoc($result);
		pg_free_result($result);
		
		
		if($user["user_id"] != $row["person_id"] and $user["privilegeLevel"] != "I"){
			http_response_code(301);
			return array(
					"result" => "OK",
					"error" => "User does not have the required permissions"
					);
		}
		
		$dateSubmitted = $row["date_submitted"];
		$dateDue = $row["date_due"];
		$finalGrade = $row["final_grade"];
		
		$dateSubmitted = ($parameters["dateSubmitted"]!=null)?$parameters["dateSubmitted"]:$dateSubmitted;
		
		if($user["privilegeLevel"] == "I"){
			
			$dateDue = ($parameters["dueDate"])?$parameters["dueDate"]:$dateDue;
			$finalGrade = ($parameters["finalGrade"])?$parameters["finalGrade"]:$finalGrade;
		
		}
		
		$query = 'update "user_casebook" set '.
							'date_submitted = $1, '.
							'date_due = $2, '.
							'final_grade = $3, '.
							'casebook_status = \'Incomplete\', '.
							'modified_by = $5, '.
							'date_modified = now() '.
							'where user_casebook_id = $4 ';
		$result = pg_query_params($db,$query,array(
												$parameters["dateSubmitted"],
												$parameters["dueDate"],
												$parameters["finalGrade"],
												$parameters["userCasebook_ID"],
												$user["username"]
		
												));
		if(!$result){
			throw new Exception("Internal server error");
		}
		$rowsAffected = pg_affected_rows($result);
		pg_free_result($result);

		
		if($rowsAffected!=1){
			return array(
							"result" => "FAILED",
							"error" => "Could not insert user casebook"
						);
		}
		
		$query = 'select '.
							'user_casebook_id,'.
							'Casebook_Type_ID,'.
							'person_ID,'.
							'Date_Started,'.
							'Date_Due,'.
							'Date_Submitted,'.
							'Final_Grade'.
					' from "user_casebook" where User_Casebook_ID=$1';
		$result = pg_query_params($db,$query,array($row["user_casebook_id"]));
		if(!$result){
			throw new Exception("Internal server error");
		}
		$row = pg_fetch_assoc($result);
		pg_free_result($result);
		
		if($row == null){
			return array(
							"result" => "FAILED",
							"error" => "Casebook not found"
						);
		}
		
		$response["data"] = buildCasebook($db,$row);
	
	
	}finally{
		pg_close($db);
	}
	
	return $response;
}


/**
*	Processes DELETE Requests
*
*/
function DELETE($parameters){

	global $user;
	global $sql_connectionString;
					
	$db = pg_connect($sql_connectionString);
	if(!$db){
		throw new Exception("Error connecting to database");
	}				
	try{
	
		if($user["privilegeLevel"] != "I"){
			http_response_code(301);
			return array(
					"result" => "OK",
					"error" => "User does not have the required permissions"
					);
		}
		
		$query = 'delete from "user_casebook" where user_casebook_id = $1 ';
		$result = pg_query_params($db,$query,array(
												$parameters["userCasebook_ID"],
												));
		if(!$result){
			throw new Exception("Internal server error");
		}
		$rowsAffected = pg_affected_rows($result);
		pg_free_result($result);
		
		if($rowsAffected!=1){
			return array(
							"result" => "FAILED",
							"error" => "Could not insert user casebook"
						);
		}
		
		return array(
						"result" => "OK"
					);
	
	
	}finally{
		pg_close($db);
	}
	
	return $response;
}

function addOrderBy($orderBy){
	$parts = explode(":",$orderBy,2);
	$allowed = array(
						"userCasebook_id" => "user_casebook_id",
						"casebook_id" => "Casebook_Type_ID",
						"user_id" => "person_ID",
						"dateStarted" => "Date_Started",
						"dueDate" => "Date_Due",
						"dateSubmitted" => "Date_Submitted",
						"finalGrade" => "Final_Grade",
						"casebook.name" => "c.casebook_name",
						"user.firstName" => "u.first_name",
						"user.lastName" => "u.last_name"
					);
	if(array_key_exists($parts[0],$allowed)){
		if(count($parts)>1 and $parts[1] == "desc"){
			return $allowed[$parts[0]] ." desc ";
		}
		return " ".$allowed[$parts[0]]." ";
	}
	
	throw new Exception("Invalid order");
}


function buildCasebook($db, $db_userCasebook){

  $format = ("d-m-Y");

  $beginning = "01-01-1970";

  $date_Start = date($format, strtotime ($db_userCasebook["date_started"]));
  $due_Date	= date($format, strtotime ($db_userCasebook["date_due"]));
  $date_Sub	= date($format, strtotime ($db_userCasebook["date_submitted"]));

  if($due_Date == $beginning){
    $due_Date = null;
  }

  if($date_Sub == $beginning){
    $date_Sub = null;
  }

  if($date_Start == $beginning){
    $date_Start = null;
  }

	$userCasebook = array(
							"userCasebook_id" => intval($db_userCasebook["user_casebook_id"]),
							"casebook_id" => intval($db_userCasebook["casebook_type_id"]),
							"user_id" => intval($db_userCasebook["person_id"]),
							"dateStarted" => $date_Start,
							"dueDate" => $due_Date,
							"dateSubmitted" => $date_Sub,
							"finalGrade" => $db_userCasebook["final_grade"],
							"instructor_last_viewed" => $db_userCasebook["instructor_last_viewed"],
							"student_last_viewed" => $db_userCasebook["student_last_viewed"],
							"casebook" => array( )
						);
						
	
	
	
	$query = 'select '.
						'Casebook_Type_ID, '.
						'Casebook_Name '.
				' from "casebook" where Casebook_Type_ID=$1';
	$result = pg_query_params($db,$query,array($userCasebook["casebook_id"]));
	if(!$result){
		throw new Exception("Casebook Type not found");
	}
	$row = pg_fetch_assoc($result);
	pg_free_result($result);

	if($row == null){
		throw new Exception("Casebook Type not found");
	}

	$userCasebook["casebook"] = array(
									"casebook_id" => intval($row["casebook_type_id"]),
									"name" => $row["casebook_name"],
									"categories" => array()
								);
	$userCasebook["casebook"]["categories"] = getCategoriesFor(
																$db,
																$userCasebook["userCasebook_id"],
																$userCasebook["casebook"]["casebook_id"],
																null,
																$userCasebook
															);
	$userCasebook["user"] = getUser($db,$userCasebook["user_id"]);
	
	$query = 'select count(*) '.
				'from "answer" a '.
				'left join "user_casebook" uc on a.user_casebook_id=uc.user_casebook_id '.
				'where a.user_casebook_id=$1 '.
				'and (a.feedback_modified_on > uc.student_last_viewed or uc.student_last_viewed is null)';
	$result = pg_query_params($db,$query,array($userCasebook["userCasebook_id"]));
	if(!$result){
		throw new Exception("Could not get new feedback count");
	}
	$row = pg_fetch_array($result);
	pg_free_result($result);
	if($row[0]>0){
		$userCasebook['hasNewFeedback']=true;
	}else{
		$userCasebook['hasNewFeedback']=false;
	}
	
	$query = 'select count(*) '.
				'from "answer" a '.
				'left join "user_casebook" uc on a.user_casebook_id=uc.user_casebook_id '.
				'where a.user_casebook_id=$1 '.
				'and (a.date_modified > uc.instructor_last_viewed or uc.instructor_last_viewed is null)'.
				'and char_length(a.answer_text)>0 ';
	$result = pg_query_params($db,$query,array($userCasebook["userCasebook_id"]));
	if(!$result){
		throw new Exception("Could not get new answer count");
	}
	$row = pg_fetch_array($result);
	pg_free_result($result);
	if($row[0]>0){
		$userCasebook['hasNewAnswer']=true;
	}else{
		$userCasebook['hasNewAnswer']=false;
	}
	
	
	
	return $userCasebook;
	
}

function getCategoriesFor($db,$userCasebook_ID,$casebookType_ID,$parent_ID,$userCasebook){

	$categories = array();
	
	$query = 'select '.
						'category_id, '.
						'casebook_type_id, '.
						'category_name, '.
						'parent_category_id, '.
						'group_order, '.
						'active, '.
						'display_type, '.
						'notes '.
				" from \"question_category\" where active = true and Casebook_Type_ID=$1 ";
	if($parent_ID == null){
		$query = $query . " and parent_category_id is null ";
	}else{
		$query = $query . " and parent_category_id = $2";
	}
	$params = array(
						$casebookType_ID
					);
	if($parent_ID!=null){
		$params[1] = $parent_ID;
	}
	$result = pg_query_params($db,$query,$params);
	if(!$result){
		throw new Exception("Casebook Type not found");
	}
	
	while($row = pg_fetch_assoc($result)){
			$categories[] = array(
									"category_id" => intval($row["category_id"]),
									"casebook_id" => intval($row["casebook_type_id"]),
									"name" => $row["category_name"],
									"parent_id" => ($row["parent_category_id"]==null)?null:intval($row["parent_category_id"]),
									"order" => intval($row["group_order"]),
									"active" => $row["active"]=="t",
									"type" => $row["display_type"],
									"notes" => $row["notes"],
									"categories" => getCategoriesFor($db,$userCasebook_ID,$casebookType_ID,intval($row["category_id"]),$userCasebook),
									"questions" => getQuestionsFor($db,$userCasebook_ID,intval($row["category_id"]),$userCasebook)
			
								);
	}
	
	pg_free_result($result);
	
	return $categories;

}

function getQuestionsFor($db,$userCasebook_ID,$category_ID,$userCasebook){
	$questions = array();
	
	$query = 'select '.
						'question_id, '.
						'category_id, '.
						'question_order, '.
						'active, '.
						'question_text, '.
						'display_type, '.
						'notes '.
						
				" from \"question_text\" where active = true and category_id = $1";
	
	$result = pg_query_params($db,$query,array($category_ID));
	if(!$result){
		throw new Exception("Casebook Type not found");
	}
	
	while($row = pg_fetch_assoc($result)){
			$questions[] = array(
									"question_id" => intval($row["question_id"]),
									"category_id" => intval($row["category_id"]),
									"type" => $row["display_type"],
									"order" => intval($row["question_order"]),
									"active" => $row["active"]=="t",
									"text" => $row["question_text"],
									"notes" => $row["notes"],
                                                                        "type" => $row["display_type"],
									"answers" => getAnswersFor($db,$userCasebook_ID,intval($row["question_id"]),$userCasebook)
								);
	}
	
	pg_free_result($result);
	
	return $questions;
}

function getAnswersFor($db,$userCasebook_ID,$question_ID,$userCasebook){
	$answers = array();
	
	$query = 'select '.
						'answer_id, '.
						'user_casebook_id, '.
						'question_id, '.
						'answer_text, '.
						'feedback, '.
						'date_modified, '.
						'feedback_modified_on '.
						
				" from \"answer\" where question_id = $1 and user_casebook_id = $2".
				" order by answer_id";
	
	$result = pg_query_params($db,$query,array($question_ID,$userCasebook_ID));
	if(!$result){
		throw new Exception("Casebook Type not found");
	}
	
	while($row = pg_fetch_assoc($result)){
	
			$newAnswer = false;
			if(array_key_exists('instructor_last_viewed',$userCasebook)){
				if(strtotime($userCasebook['instructor_last_viewed']) > 0){
					$newAnswer = strtotime($row['date_modified']) > strtotime($userCasebook['instructor_last_viewed'])  && strlen($row['answer_text']) > 0;
				}else{
					$newAnswer=true;
				}
			}else{
				if(array_key_exists('answer_text',$row) && strlen($row['answer_text']) > 0){
					$newAnswer=true;
				}else{
					$newAnswer=false;
				}
			}
			
			$newFeedback = false;
			if(array_key_exists('student_last_viewed',$userCasebook)){
				if(strtotime($userCasebook['student_last_viewed']) > 0){
					
					$newFeedback = strtotime($row['feedback_modified_on']) > strtotime($userCasebook['student_last_viewed']) && strlen($row['feedback']) > 0;
					
				}else{
					$newFeedback=true;
				}
			}else{
				if(array_key_exists('feedback',$row) && strlen($row['feedback']) > 0){
					$newFeedback=true;
				}else{
					$newFeedback=false;
				}
			}
			
			
			$answers[] = array(
									"answer_id" => intval($row["answer_id"]),
									"userCasebook_id" => intval($row["user_casebook_id"]),
									"question_id" => intval($row["question_id"]),
									"text" => $row["answer_text"],
									"feedback" => $row["feedback"],
									"newAnswer" => $newAnswer,
									"newFeedback" => $newFeedback,
									
								);
	}
	
	pg_free_result($result);
	
	return $answers;
}

function getUser($db,$user_ID){
	$answers = array();
	
	$query = 'select '.
						'person_id, '.
						'privilege_level, '.
						'user_name, '.
						'first_name, '.
						'last_name, '.
						'email, '.
						'phone, '.
						'date_last_login '.
						
				" from \"person\" where person_id = $1";
	
	$result = pg_query_params($db,$query,array($user_ID));
	if(!$result){
		throw new Exception("User not found");
	}
	
	$row=null;
	if(!($row = pg_fetch_assoc($result))){
		throw new Exception("User not found");
	}
	
	pg_free_result($result);
	
	return array(
					"user_id" => intval($row["person_id"]),
					"privilegeLevel" => $row["privilege_level"],
					"userName" => $row["user_name"],
					"firstName" => $row["first_name"],
					"lastName" => $row["last_name"],
					"email" => $row["email"],
					"phone" => $row["phone"],
					"dateLastLogin" => $row["date_last_login"],
	
				);
}

?>
