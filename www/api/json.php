<?php

/**
*	Converts object to json. 
*
*	Also incloses JSON in a callback for JSONP requests if callback was specified on request
*/
function toJson($obj){

	$str = json_encode($obj);
	
	if(array_key_exists("callback",$_REQUEST)){
		$str = $_REQUEST["callback"] . "(" . $str .");";
	}
	
	return $str;

}


/**
*	Returns JSON as an associative array. Limits max recursion depth to 100 levels
*/
function fromJson($str){

	return json_decode($str,true,100);
}

?>
