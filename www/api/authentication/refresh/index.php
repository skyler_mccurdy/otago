<?php
/**
*	Logs user into system.
*
*	Supported Operations
*
*	POST
*		parameters
*			username
*			password
*		response
*			sets the authentication token
*/

include "../../../secure.php";
include "../../validation.php";
include "../../json.php";
include "../config.php";

header('Content-Type: application/json');

$db = null;
try{
	
	if(!array_key_exists('X-XSRF-TOKEN',$_COOKIE)){
		throw new Exception("User not logined in");
	}
	
	$token = $_COOKIE['X-XSRF-TOKEN'];
	
	$db = pg_connect($sql_connectionString);
	if(!$db){
		throw new Exception("Error connecting to database");
	}
	
	
	
	$query = 'update "token" set expires = $2 '.
							'where token = $1 and expires >= now()';
	$result = pg_query_params($db,$query,array(
												$token,
												date('Y-m-d H:i:s',time()+$tokenDuration)
												));
	if(!$result){
		throw new Exception("Internal server error");
	}
	$rowsAffected = pg_affected_rows($result);
	pg_free_result($result);

	
	if($rowsAffected!=1){
		throw new Exception("User not logined in");
	}
	
	
	$query = 'select p.person_id,user_name, first_name, last_name, privilege_level, email, phone, expires from "token" t join "person" p on t.person_id = p.person_id where token=$1 and expires >= now()';
	$result = pg_query_params($db,$query,array($token));
	if(!$result){
		throw new Exception("Internal Server Error");
	}
	
	$row = pg_fetch_assoc($result);
	
	if(!$row){
		throw new Exception("User not logined in");
	}
	
	pg_free_result($result);
	
	
	
	
	
	
	
	
	setcookie("X-XSRF-TOKEN",$token,strtotime($row["expires"]),"/",$_SERVER["SERVER_NAME"],false,true);
	
	echo toJson(array(
					"result" => "OK",
					"data" => array(
							"user_id" => $row["person_id"],
							"username" => $row["user_name"],
							"privilege_level" => $row["privilege_level"],
							"firstName" => $row["first_name"],
							"lastName" => $row["last_name"],
							"email" => $row["email"],
							"phone" => $row["phone"]
					
						)
				));
	
}catch(Exception $e){
	echo toJson( array(
					"result" => "FAILED",
					"error" => $e->getMessage()
	
				));
}finally{
	if($db){
		pg_close($db);
	}
}

?>
