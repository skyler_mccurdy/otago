<?php
/**
*	Logs user into system.
*
*	Supported Operations
*
*	POST
*		parameters
*			username
*			password
*		response
*			sets the authentication token
*/

include "../../../secure.php";
include "../../validation.php";
include "../../json.php";
include "../config.php";

header('Content-Type: application/json');


$db = null;
try{


	if(!array_key_exists('X-XSRF-TOKEN',$_COOKIE)){
		throw new Exception("User not logined in");
	}
	
	$token = $_COOKIE['X-XSRF-TOKEN'];
	
	$db = pg_connect($sql_connectionString);
	if(!$db){
		throw new Exception("Error connecting to database");
	}
	
	$query = 'delete from  "token" '.
							'where token = $1';
	pg_query_params($db,$query,array($token));
	

	
	setcookie("X-XSRF-TOKEN","",time()-100000,"/",$_SERVER["SERVER_NAME"],false,true);
	
	echo toJson(array(
					"result" => "OK"
					
				));
	
}catch(Exception $e){
	echo toJson( array(
					"result" => "FAILED",
					"error" => $e->getMessage()
	
				));
}finally{
	if($db){
		pg_close($db);
	}
}

?>
