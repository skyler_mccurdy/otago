<?php
/**
*	Logs user into system.
*
*	Supported Operations
*
*	POST
*		parameters
*			username
*			password
*		response
*			sets the authentication token
*/

include "../../../secure.php";
include "../../validation.php";
include "../../json.php";
include "../config.php";

header('Content-Type: application/json');

$inputJSON = file_get_contents('php://input');
$input= fromJson( $inputJSON);

$username = $input["username"];
$password = $input["password"];

$db = null;
try{
	if(!Validation\Authentication::username($username)){
		throw new Exception("Invalid username or password");
	}
	if(!Validation\Authentication::password($password)){
		throw new Exception("Invalid username or password");
	}
	
	$db = pg_connect($sql_connectionString);
	if(!$db){
		throw new Exception("Error connecting to database");
	}
	
	$query = 'select * from "person" where user_name=$1';
	$result = pg_query_params($db,$query,array($username));
	if(!$result){
		throw new Exception("Invalid username or password");
	}
	
	$row = pg_fetch_assoc($result);
	
	
	pg_free_result($result);
	
	$str = sha1($password . $row["salt"]);
	
	if($str != $row["password"]){
		throw new Exception("Invalid username or password");
	}
	
	$user_id = $row["person_id"];
	$token = substr(base64_encode(openssl_random_pseudo_bytes(40)),0,40);
	$expires = date_format(
							date_add( 
									date_create(date('Y-m-d H:i:s')),
									date_interval_create_from_date_string($tokenDuration . " seconds")
									),
							'Y-m-d H:i:s');
	
	$query = "INSERT INTO \"token\" (token,person_id,expires) VALUES ($1,$2,$3)";
	$result = pg_query_params($db,$query,array($token,$user_id,$expires));
	if(!$result){
		throw new Exception("Error creating token");
	}
	
	pg_free_result($result);
	
	setcookie("X-XSRF-TOKEN",$token,time()+$tokenDuration,"/",$_SERVER["SERVER_NAME"],false,true);
	
	echo toJson(array(
					"result" => "OK",
					"data" => array(
							"user_id" => $row["person_id"],
							"username" => $row["user_name"],
							"privilege_level" => $row["privilege_level"],
							"token" => $token
					
						)
				));
	
}catch(Exception $e){
	echo toJson( array(
					"result" => "FAILED",
					"error" => $e->getMessage()
	
				));
}finally{
	if($db){
		pg_close($db);
	}
}

?>
