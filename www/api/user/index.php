<?php
/**
*	API endpoint for person table
*
*/
include "../../secure.php";
include "../validation.php";
include "../json.php";
include "../framework.php";


/**
*	Define the allowed parameters and their default values
*
*	Parameters to GET requests will be filled from query parameters
*	Parameters to all other requests will be loaded from a JSON body
*
*/
function defineParameters(){
	switch($_SERVER['REQUEST_METHOD']){
	case "GET":
		return array(
				"user_id" => null,
				"userName" => null,
				"privilegeLevel" => null,
				"firstName" => null,
				"lastName" => null,
				"email" => null,
				"phone" => null,
				"page" => null,
				"pageSize" => null,
				"order" => "userName",
				"search" => null
			);
		break;
	case "POST":
		return array(
				"userName" => null,
				"password" => null,
				"privilegeLevel" => null,
				"firstName" => null,
				"lastName" => null,
				"email" => null,
				"phone" => null
			);
		break;
	case "PUT":
		return array(
				"user_id" => null,
				"userName" => null,
				"password" => null,
				"privilegeLevel" => null,
				"firstName" => null,
				"lastName" => null,
				"email" => null,
				"phone" => null
			);
		break;
	case "DELETE":
		return array(
				"user_id" => null
			);
		break;
	default:
		return null;
	}
}

/**
*	Validates the parameters
*
*	Should validate parameters using functions from validation.php
*	Returns an array of errors
*
*/
function validate(&$parameters){
	$errors = array();

	switch($_SERVER['REQUEST_METHOD']){
	case "GET":
			if($parameters["user_id"]!=null and !Validation\User::user_id($parameters["user_id"])){
				$errors["user_id"]="invalid user id";
			}
			
			if($parameters["userName"]!=null and !Validation\User::userName($parameters["userName"])){
				$errors["userName"]="invalid username";
			}
			
			if($parameters["privilegeLevel"]!=null and !Validation\User::privilegeLevel($parameters["privilegeLevel"])){
				$errors["privilegeLevel"]="invalid privilegeLevel";
			}
			
			if($parameters["firstName"]!=null and !Validation\User::firstName($parameters["firstName"])){
				$errors["firstName"]="invalid first name";
			}
			
			if($parameters["lastName"]!=null and !Validation\User::lastName($parameters["lastName"])){
				$errors["lastName"]="invalid last name";
			}
			
			//if($parameters["email"]!=null and !Validation\User::email($parameters["email"])){
			//	$errors["email"]="invalid e-mail";
			//}
			
			//if($parameters["phone"]!=null and !Validation\User::phone($parameters["phone"])){
			//	$errors["phone"]="invalid phone";
			//}
			
			if($parameters["page"]!=null and !Validation\Generic::page($parameters["page"])){
				$errors["page"]="invalid page";
			}
			
			if($parameters["pageSize"]!=null and !Validation\Generic::pageSize($parameters["pageSize"])){
				$errors["pageSize"]="invalid page size";
			}
			
			
			return $errors;
		break;
	case "POST":
			
			if(!Validation\User::userName($parameters["userName"])){
				$errors["userName"]="invalid username";
			}
			
			if(!Validation\User::password($parameters["password"])){
				$errors["password"]="invalid password";
			}
			
			if(!Validation\User::privilegeLevel($parameters["privilegeLevel"])){
				$errors["privilegeLevel"]="invalid privilegeLevel";
			}
			
			if(!Validation\User::firstName($parameters["firstName"])){
				$errors["firstName"]="invalid first name";
			}
			
			if(!Validation\User::lastName($parameters["lastName"])){
				$errors["lastName"]="invalid last name";
			}
			
			if(!Validation\User::email($parameters["email"])){
				$errors["email"]="invalid e-mail";
			}
			
			if(!Validation\User::phone($parameters["phone"])){
				$errors["phone"]="invalid phone";
			}
	
			return $errors;
		break;
	case "PUT":
	
			if($parameters["user_id"]!=null and !Validation\User::user_id($parameters["user_id"])){
				$errors["user_id"]="invalid user id";
			}
	
			if($parameters["userName"]!=null and !Validation\User::userName($parameters["userName"])){
				$errors["userName"]="invalid username";
			}
			
			if($parameters["password"]!=null and !Validation\User::password($parameters["password"])){
				$errors["password"]="invalid password";
			}
			
			if($parameters["privilegeLevel"]!=null and !Validation\User::privilegeLevel($parameters["privilegeLevel"])){
				$errors["privilegeLevel"]="invalid privilegeLevel";
			}
			
			if($parameters["firstName"]!=null and !Validation\User::firstName($parameters["firstName"])){
				$errors["firstName"]="invalid first name";
			}
			
			if($parameters["lastName"]!=null and !Validation\User::lastName($parameters["lastName"])){
				$errors["lastName"]="invalid last name";
			}
			
			if($parameters["email"]!=null and !Validation\User::email($parameters["email"])){
				$errors["email"]="invalid e-mail";
			}
			
			if($parameters["phone"]!=null and !Validation\User::phone($parameters["phone"])){
				$errors["phone"]="invalid phone";
			}
	
			
	
			return $errors;
		break;
	case "DELETE":
	
			if($parameters["user_id"]!=null and !Validation\User::user_id($parameters["user_id"])){
				$errors["user_id"]="invalid user id";
			}
			
			
			return $errors;
		break;
	default:
		return $errors["call"]="Invalid method call";
	}

	return $errors;
}

/**
*	Processes GET Requests
*
*/
function GET($parameters){

	global $user;
	global $sql_connectionString;

	$response = array(
						"result" => "OK"
					);
					
					
					
					
	$db = pg_connect($sql_connectionString);
	if(!$db){
		throw new Exception("Error connecting to database");
	}				
	try{
		if($parameters["user_id"]){
			$query = 'select '.
						' person_id, '.
						' privilege_level, '.
						' user_name, '.
						' first_name, '.
						' last_name, '.
						' email, '.
						' phone, '.
						' date_last_login '.
						
						' from "person" where person_id=$1';
			$result = pg_query_params($db,$query,array($parameters["user_id"]));
			if(!$result){
				throw new Exception("Internal server error");
			}
			$row = pg_fetch_assoc($result);
			pg_free_result($result);
			
			
			if($row == null or ( $user["user_id"] != $row["person_id"] and $user["privilegeLevel"] != "I" ) ){
				return array(
								"result" => "FAILED",
								"error" => "user not found"
							);
			}
			
      $format = ("d-m-Y");

      $beginning = "01-01-1970";

     $date_login = date($format, strtotime ($row["date_last_login"]));
     
     if($date_login == $beginning){
        $date_login = null;
    }
      
			$response["data"] = array(
									"user_id" => $row["person_id"],
									"privilegeLevel" => $row["privilege_level"],
									"userName" => $row["user_name"],
									"firstName" => $row["first_name"],
									"lastName" => $row["last_name"],
									"email" => $row["email"],
									"phone" => $row["phone"],
									"dateLastLogin" => $date_login,
			
								);
		
		}else{
		
		
			$query = 'select '.
						' person_id, '.
						' privilege_level, '.
						' user_name, '.
						' first_name, '.
						' last_name, '.
						' email, '.
						' phone, '.
						' date_last_login '.
						
						' from "person" ';
			$whereStatement = "";
			$whereParams = array();
			
			if($parameters["user_id"]!=null){
				if(strlen($whereStatement) > 0){
					$whereStatement = $whereStatement . " and ";
				}
				$whereStatement = $whereStatement . " person_id = $".(count($whereParams)+1)." ";
				$whereParams[] = $parameters["user_id"];
			}
			
			
			
			if($parameters["privilegeLevel"]!=null){
				if(strlen($whereStatement) > 0){
					$whereStatement = $whereStatement . " and ";
				}
				$whereStatement = $whereStatement . " privilege_level = $".(count($whereParams)+1)." ";
				$whereParams[] = $parameters["privilegeLevel"];
			}
			
		
			
			if($parameters["userName"]!=null){
				if(strlen($whereStatement) > 0){
					$whereStatement = $whereStatement . " and ";
				}
				$whereStatement = $whereStatement . " user_name ilike $".(count($whereParams)+1)." ";
				$whereParams[] = "%" . $parameters["userName"] . "%";
			}
			
			if($parameters["firstName"]!=null){
				if(strlen($whereStatement) > 0){
					$whereStatement = $whereStatement . " and ";
				}
				$whereStatement = $whereStatement . " first_name ilike $".(count($whereParams)+1)." ";
				$whereParams[] = "%" . $parameters["firstName"] . "%";
			}
			
			if($parameters["lastName"]!=null){
				if(strlen($whereStatement) > 0){
					$whereStatement = $whereStatement . " and ";
				}
				$whereStatement = $whereStatement . " last_name ilike $".(count($whereParams)+1)." ";
				$whereParams[] = "%" . $parameters["lastName"] . "%"; 
			}
			
			if($parameters["email"]!=null){
				if(strlen($whereStatement) > 0){
					$whereStatement = $whereStatement . " and ";
				}
				$whereStatement = $whereStatement . " email ilike $".(count($whereParams)+1)." ";
				$whereParams[] = "%" . $parameters["email"] . "%";
			}
			
			if($parameters["phone"]!=null){
				if(strlen($whereStatement) > 0){
					$whereStatement = $whereStatement . " and ";
				}
				$whereStatement = $whereStatement . " phone ilike $".(count($whereParams)+1)." ";
				$whereParams[] = "%" . $parameters["phone"] . "%";
			}
			
			if($parameters["search"]!=null && $parameters["search"]!="null" && $parameters["search"]!="undefined" && $parameters["search"]!=""){
				if(strlen($whereStatement) > 0){
					$whereStatement = $whereStatement . " and ";
				}
				$whereStatement .= '(';
				$whereStatement = $whereStatement . " user_name ilike $".(count($whereParams)+1)."  or ";
				$whereStatement = $whereStatement . " first_name ilike $".(count($whereParams)+1)."  or ";
				$whereStatement = $whereStatement . " last_name ilike $".(count($whereParams)+1)."  ";
				$whereStatement .= ')';
				$whereParams[] = "%" . $parameters["search"] . "%";
			}
			
			if(strlen($whereStatement) > 0){
				$whereStatement = " where " . $whereStatement;
			}
			
			$result = pg_query_params($db,"select count(*) from \"person\" " .$whereStatement,$whereParams);
			if(!$result){
				throw new Exception("Internal server error");
			}
			
			$response["total"] = intval(pg_fetch_array($result)[0]);
			
			pg_free_result($result);
			
			if($parameters["order"] !=null){
				$orderBys = explode(",",$parameters["order"]);
				if(count($orderBys)>0){
					$whereStatement = $whereStatement . " order by ". addOrderBy($orderBys[0]) ." ";
					for($i=1;$i<count($orderBys);$i++){
						$whereStatement = $whereStatement . ", ".addOrderBy($orderBys[$i])." ";
					}
				}
			}
			
			if($parameters["page"] != null || $parameters["pageSize"]){
				$page = ($parameters["page"]!=null)?$parameters["page"]:1;
				$pageSize = ($parameters["pageSize"]!=null)?$parameters["pageSize"]:10;
				
				$response["page"] = intval($page);
				$response["pageSize"] = intval($pageSize);
				
				$whereStatement = $whereStatement . " limit $".(count($whereParams)+1)." offset $".(count($whereParams)+2)." ";
				$page = ($page-1) * $pageSize;
				$whereParams[] = $pageSize;
				$whereParams[] = $page;
			}
			
			$result = pg_query_params($db,$query.$whereStatement,$whereParams);
			if(!$result){
				throw new Exception("Internal server error");
			}
			
			$response["count"] = pg_num_rows($result);
			
      $format = ("d-m-Y");

      $beginning = "01-01-1970";

      
      
			$response["data"]=array();
			while($row = pg_fetch_assoc($result)){
      
      $date_login = date($format, strtotime ($row["date_last_login"]));
      
      if($date_login == $beginning){
        $date_login = null;
      }
      
				$response["data"][] = array(
									"user_id" => $row["person_id"],
									"privilegeLevel" => $row["privilege_level"],
									"userName" => $row["user_name"],
									"firstName" => $row["first_name"],
									"lastName" => $row["last_name"],
									"email" => $row["email"],
									"phone" => $row["phone"],
									"dateLastLogin" => $date_login,
			
								);
			}
			
			pg_free_result($result);
			
		}
	}finally{
		pg_close($db);
	}
	

	return $response;
}

/**
*	Processes POST Requests
*
*/
function POST($parameters){

	global $user;
	global $sql_connectionString;

	$response = array(
						"result" => "OK",
						"data" => array()
					);
					
					
					
					
	$db = pg_connect($sql_connectionString);
	if(!$db){
		throw new Exception("Error connecting to database");
	}				
	try{
	
		if($user["privilegeLevel"] != "I"){
			http_response_code(301);
			return array(
					"result" => "OK",
					"error" => "User does not have the required permissions"
					);
		}
		
		$salt = substr(base64_encode(openssl_random_pseudo_bytes(40)),0,40);
		$password = sha1($parameters["password"] . $salt);
		
		$query = 'insert into "person" '.
							' (user_name,password,salt,privilege_level,first_name,last_name,email,phone,modified_by,date_modified ) values '.
							' ( '.
							'$1, '.
							'$2, '.
							'$3, '.
							"$4, ".
							"$5, ".
							"$6, ".
							"$7, ".
							"$8, ".
							"$9, ".
							' now() ) '.
							' returning person_id ';

		$result = pg_query_params($db,$query,array(
													$parameters["userName"],
													$password,
													$salt,
													$parameters["privilegeLevel"],
													$parameters["firstName"],
													$parameters["lastName"],
													$parameters["email"],
													$parameters["phone"],
													$user["username"])
												);

		if(!$result){
			throw new Exception("Internal server error");
		}
		$id = pg_fetch_assoc($result);
		pg_free_result($result);
		
		if($id == null or !array_key_exists("person_id",$id)){
			return array(
							"result" => "FAILED",
							"error" => "Could not create user"
						);
		}
		
		$query = 'select '.
						' person_id, '.
						' privilege_level, '.
						' user_name, '.
						' first_name, '.
						' last_name, '.
						' email, '.
						' phone, '.
						' date_last_login '.
						
						' from "person" where person_id=$1';
			$result = pg_query_params($db,$query,array($id["person_id"]));
			if(!$result){
				throw new Exception("Internal server error");
			}
			$row = pg_fetch_assoc($result);
			pg_free_result($result);
			
			if($row == null){
				return array(
								"result" => "FAILED",
								"error" => "user not created"
							);
			}
		
			$format = ("d-m-Y");

      $beginning = "01-01-1970";

      $date_login = date($format, strtotime ($row["date_last_login"]));
      
      if($date_login == $beginning){
        $date_login = null;
      }
      
      
			$response["data"] = array(
									"user_id" => $row["person_id"],
									"privilegeLevel" => $row["privilege_level"],
									"userName" => $row["user_name"],
									"firstName" => $row["first_name"],
									"lastName" => $row["last_name"],
									"email" => $row["email"],
									"phone" => $row["phone"],
									"dateLastLogin" => $date_login,
			
								);
	
	
	}finally{
		pg_close($db);
	}
	

	return $response;
}

/**
*	Processes PUT Requests
*
*/
function PUT($parameters){

	global $user;
	global $sql_connectionString;

	$response = array(
						"result" => "OK",
						"data" => array()
					);
					
					
					
					
	$db = pg_connect($sql_connectionString);
	if(!$db){
		throw new Exception("Error connecting to database");
	}				
	try{
	
		
		
		$query = 'select '.
					' person_id, '.
					' privilege_level, '.
					' user_name, '.
					' password, '.
					' salt, '.
					' first_name, '.
					' last_name, '.
					' email, '.
					' phone, '.
					' date_last_login '.
					
					' from "person" where person_id=$1';
		$result = pg_query_params($db,$query,array($parameters["user_id"]));
		if(!$result){
			throw new Exception("Internal server error");
		}
		$row = pg_fetch_assoc($result);
		pg_free_result($result);
		
		if($row == null){
			return array(
							"result" => "FAILED",
							"error" => "user not updated"
						);
		}
		
		if($user["privilegeLevel"] == "I"){
		
			if($parameters["userName"]){
				$row["user_name"] = $parameters["userName"];
			}
			
			if($parameters["privilegeLevel"]){
				$row["privilege_level"] = $parameters["privilegeLevel"];
			}
			
		}
		
		if($parameters["password"]){
			$row["salt"] = substr(base64_encode(openssl_random_pseudo_bytes(40)),0,40);
			$row["password"] = sha1($parameters["password"] . $row["salt"]);
		}
		
		if($parameters["firstName"]){
			$row["first_name"] = $parameters["firstName"];
		}
		
		if($parameters["lastName"]){
			$row["last_name"] = $parameters["lastName"];
		}
		
		if($parameters["email"]){
			$row["email"] = $parameters["email"];
		}
		
		if($parameters["phone"]){
			$row["phone"] = $parameters["phone"];
		}
		
		$query = 'update "person" set '.
							'user_name = $1, '.
							'password = $2, '.
							'salt = $3, '.
							"privilege_level = $4, ".
							"first_name = $5, ".
							"last_name = $6, ".
							"email = $7, ".
							"phone = $8, ".
							"modified_by = $9, ".
							'date_modified=  now() '.
							"where person_id = $10";

		$result = pg_query_params($db,$query,array(
													$row["user_name"],
													$row["password"],
													$row["salt"],
													$row["privilege_level"],
													$row["first_name"],
													$row["last_name"],
													$row["email"],
													$row["phone"],
													$user["username"],
													$row["person_id"])
												);

		if(!$result){
			throw new Exception("Internal server error");
		}
		pg_free_result($result);
		
		
		$query = 'select '.
						' person_id, '.
						' privilege_level, '.
						' user_name, '.
						' first_name, '.
						' last_name, '.
						' email, '.
						' phone, '.
						' date_last_login '.
						
						' from "person" where person_id=$1';
			$result = pg_query_params($db,$query,array($row["person_id"]));
			if(!$result){
				throw new Exception("Internal server error");
			}
			$row = pg_fetch_assoc($result);
			pg_free_result($result);
			
			if($row == null){
				return array(
								"result" => "FAILED",
								"error" => "user not update"
							);
			}
		
			$format = ("d-m-Y");

      $beginning = "01-01-1970";

      $date_login = date($format, strtotime ($row["date_last_login"]));
      
      if($date_login == $beginning){
        $date_login = null;
      }
      
      
			$response["data"] = array(
									"user_id" => $row["person_id"],
									"privilegeLevel" => $row["privilege_level"],
									"userName" => $row["user_name"],
									"firstName" => $row["first_name"],
									"lastName" => $row["last_name"],
									"email" => $row["email"],
									"phone" => $row["phone"],
									"dateLastLogin" => $date_login,
			
								);
	
	
	}finally{
		pg_close($db);
	}
	

	return $response;
}


/**
*	Processes DELETE Requests
*
*/
function DELETE($parameters){

	global $user;
	global $sql_connectionString;
					
	$db = pg_connect($sql_connectionString);
	if(!$db){
		throw new Exception("Error connecting to database");
	}				
	try{
	
		if($user["privilegeLevel"] != "I"){
			http_response_code(301);
			return array(
					"result" => "OK",
					"error" => "User does not have the required permissions"
					);
		}
		
		$query = 'delete from "token" where person_id = $1 ';
		$result = pg_query_params($db,$query,array(
												$parameters["user_id"],
												));
		if(!$result){
			throw new Exception("Internal server error");
		}
		$rowsAffected = pg_affected_rows($result);
		pg_free_result($result);
		
		
		$query = 'delete from "person" where person_id = $1 ';
		$result = pg_query_params($db,$query,array(
												$parameters["user_id"],
												));
		if(!$result){
			throw new Exception("Internal server error");
		}
		$rowsAffected = pg_affected_rows($result);
		pg_free_result($result);
		
		if($rowsAffected!=1){
			return array(
							"result" => "FAILED",
							"error" => "Could not delete user"
						);
		}
		
		return array(
						"result" => "OK"
					);
	
	
	}finally{
		pg_close($db);
	}
	
	return $response;
}


function addOrderBy($orderBy){
	$parts = explode(":",$orderBy,2);
	$allowed = array(
						"user_id" => "person_id",
						"privilegeLevel" => "privilege_level",
						"userName" => "user_name",
						"firstName" => "first_name",
						"lastName" => "last_name",
						"email" => "email",
						"phone" => "phone",
						"dateLastLogin" => "date_last_login",
					);
	if(array_key_exists($parts[0],$allowed)){
		if(count($parts)>1 and $parts[1] == "desc"){
			return $allowed[$parts[0]] ." desc ";
		}
		return " ".$allowed[$parts[0]]." ";
	}
	
	throw new Exception("Invalid order");
}


?>
