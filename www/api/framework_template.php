<?php
/**
*	Used as a template for API end points
*
*	Copy this file to new location and adjust include paths
*
*/
include "../secure.php";
include "validation.php";
include "json.php";
include "framework.php";


/**
*	Define the allowed parameters and their default values
*
*	Parameters to GET requests will be filled from query parameters
*	Parameters to all other requests will be loaded from a JSON body
*
*/
function defineParameters(){
	switch($_SERVER['REQUEST_METHOD']){
	case "GET":
		return array(
				"id" => null,
				"page" => null,
				"pageSize" => null,
			);
		break;
	case "POST":
		return array(
				
			);
		break;
	case "PUT":
		return array(
				
			);
		break;
	case "DELETE":
		return array(
				
			);
		break;
	default:
		return null;
	}
}

/**
*	Validates the parameters
*
*	Should validate parameters using functions from validation.php
*	Returns an array of errors
*
*/
function validate(&$parameters){
	$errors = array();

	//if(!Validation\Demo::id($parameters["id"])){
	//	$errors["id"]="invalid id";
	//}

	return $errors;
}

/**
*	Processes GET Requests
*
*/
function GET($parameters){

	$response = array(
						"result" => "OK",
						"data" => array()
					);

	$response["data"]["someData"] = "stuff";
	$response["data"]["otherData"] = array(
										"and" => "things",
										"parameters" => $parameters
									);

	return $response;
}

/**
*	Processes POST Requests
*
*/
function POST($parameters){

	$response = array(
						"result" => "OK",
						"data" => array()
					);

	$response["data"]["someData"] = "stuff";
	$response["data"]["otherData"] = array(
										"and" => "things"
									);

	return $response;
}

/**
*	Processes PUT Requests
*
*/
function PUT($parameters){

	$response = array(
						"result" => "OK",
						"data" => array()
					);

	$response["data"]["someData"] = "stuff";
	$response["data"]["otherData"] = array(
										"and" => "things"
									);

	return $response;
}


/**
*	Processes DELETE Requests
*
*/
function DELETE($parameters){

	$response = array(
						"result" => "OK",
						"data" => array()
					);

	$response["data"]["someData"] = "stuff";
	$response["data"]["otherData"] = array(
										"and" => "things"
									);

	return $response;
}



?>
