<?php

/**
 *	Provides functions for validating data from users.
 *
 *	Functions may also modify the data if nessisary.
 *	For example converting HTML special characters to the proper encoding
 *
 */
namespace Validation;

class Generic {
	public static function page(&$page) {
		return preg_match ( "/^[0-9]{1,10}$/i", $page ) && $page > 0 && $page < 2147483647;
	}
	public static function pageSize(&$pageSize) {
		return preg_match ( "/^[0-9]{1,10}$/i", $pageSize ) && $pageSize > 0 && $pageSize < 2147483647;
	}
}

/**
 * Validations for api/authentication/*
 */
class Authentication {
	public static function username(&$username) {
		return preg_match ( "/^[A-Z0-9_\-\.]{1,30}$/i", $username );
	}
	public static function password(&$password) {
		return preg_match ( '/^[\w\d\s\.!@#$%^&*()_\-+={}\[\]?\,\/`~<>\\\\\'"]{4,30}$/i', $password );
	}
	
	public static function token(&$token){
	
		return preg_match( "/^[A-Z0-9+/=]{40,40}$/i",
							$token
						);
	}
}
class Template {
	public static function casebook_id(&$casebook_id) {
		if ($casebook_id == null)
			return true;
		
		return preg_match ( // '/^[0-9]{1,10}$/i',
'/^[0-9]{1,10}$/i', $casebook_id );
	}
	public static function casebookName(&$casebookName) {
		return preg_match ( // '/^([A-Z 0-9.]){0,100}$/i',
'/^([A-Z 0-9.]){0,100}$/i', $casebookName );
	}
	public static function page(&$page) {
		if ($page == null)
			return true;
		
		return preg_match ( // '/^([0-9]){0,20}$/i',
'/^([0-9]){0,20}$/i', $page );
	}
	public static function pageSize(&$pageSize) {
		if ($pageSize == null)
			return true;
		
		return preg_match ( // '/^([0-9]){0,20}$/i',
'/^([0-9]){0,20}$/i', $pageSize );
	}
	public static function type(&$type) {
		if ($type == null)
			return true;
		
		return preg_match ( // '/^[GST]$/i',
'/^[GST]$/i', $type );
	}
	public static function order(&$order) {
		if ($order == null)
			return true;
		
		return preg_match ( // '/^[0-9]{1,10}$/i',
'/^[0-9]{1,10}$/i', $order );
	}
	public static function active(&$active) {
		if ($active == null)
			return true;
		
		return preg_match ( // '/^((true)|(false))$/i',
'/^((true)|(false))$/i', $active );
	}
	public static function notes(&$notes) {
		if ($notes == null)
			return true;
		
		return preg_match ( // /^([^<>])*$/i,
'/^([^<>])*$/i', $notes );
	}
}

/**
 * Validation for api/casebook/
 */
class Casebook {
	public static function userCasebook_ID(&$userCasebook_ID) {
		return preg_match ( "/^[0-9]{1,10}$/i", $userCasebook_ID ) && $userCasebook_ID > 0 && $userCasebook_ID < 2147483647;
	}
	public static function casebook_ID(&$casebook_ID) {
		return preg_match ( "/^[0-9]{1,10}$/i", $casebook_ID ) && $casebook_ID > 0 && $casebook_ID < 2147483647;
	}
	public static function user_ID(&$user_ID) {
		return preg_match ( "/^[0-9]{1,10}$/i", $user_ID ) && $user_ID > 0 && $user_ID < 2147483647;
	}
	public static function dateStarted(&$dateStarted) {
		return preg_match ( "/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}Z$/", $dateStarted );
	}
	
	public static function dateDue(&$dateDue) {
		return preg_match ( "/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}Z$/", $dateDue );
	}
	public static function dateSubmitted(&$dateSubmitted) {
		return preg_match ( "/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}Z$/", $dateSubmitted );
	}
	public static function finalGrade(&$finalGrade) {
		return preg_match ( "/^(([A-D][+-]?)|F)$/", $finalGrade );
	}
}
class Answer {
	public static function answer_ID(&$answer_ID) {
		return preg_match ( "/^[0-9]{1,10}$/i", $answer_ID ) && $answer_ID > 0 && $answer_ID < 2147483647;
	}
	public static function userCasebook_ID(&$userCasebook_ID) {
		return preg_match ( "/^[0-9]{1,10}$/i", $userCasebook_ID ) && $userCasebook_ID > 0 && $userCasebook_ID < 2147483647;
	}
	public static function question_ID(&$question_ID) {
		return preg_match ( "/^[0-9]{1,10}$/i", $question_ID ) && $question_ID > 0 && $question_ID < 2147483647;
	}
	public static function answer_text(&$answer_text) {
		$answer_text = htmlentities ( $answer_text, ENT_QUOTES | ENT_IGNORE, "UTF-8" );
		return preg_match ( "/^([^<>])*$/i", $answer_text );
	}
	public static function feedback(&$feedback) {
		$feedback = htmlentities ( $feedback, ENT_QUOTES | ENT_IGNORE, "UTF-8" );
		
		return preg_match ( "/^([^<>])*$/i", $feedback );
	}
}

class CasebookQuestion {
	public static function category_ID(&$category_ID) {
		return preg_match ( "/^[0-9]{1,10}$/i", $category_ID ) && $category_ID > 0 && $category_ID < 2147483647;
	}
	public static function question_ID(&$question_ID) {
		return preg_match ( "/^[0-9]{1,10}$/i", $question_ID ) && $question_ID > 0 && $question_ID < 2147483647;
	}
	public static function order(&$orderNum) {
		return preg_match ( "/^-?[0-9]{1,10}$/i", $orderNum );
	}
	
	public static function active(&$active) {
	
		return is_bool($active);
	}
	
	public static function type(&$type) {
		return preg_match ( "/^[M1DC]$/", $type );
	}
	
	public static function text(&$text) {
		$text = htmlentities ( $text, ENT_QUOTES | ENT_IGNORE, "UTF-8" );
		return preg_match ( "/^([^<>])*$/i", $text );
	}
	
	public static function notes(&$notes) {
		$notes = htmlentities ( $notes, ENT_QUOTES | ENT_IGNORE, "UTF-8" );
		return preg_match ( "/^([^<>])*$/i", $notes );
	}
}


class User {

	public static function user_id(&$user_id) {
		return preg_match ( "/^[0-9]{1,10}$/i", $user_id ) && $user_id > 0 && $user_id < 2147483647;
	}

	public static function username(&$username) {
		return preg_match ( "/^[A-Z0-9_\-\.]{1,30}$/i", $username );
	}
	public static function password(&$password) {
		return preg_match ( '/^[\w\d\s\.!@#$%^&*()_\-+={}\[\]?\,\/`~<>\\\\\'"]{4,30}$/i', $password );
	}
	
	public static function privilegeLevel(&$privilegeLevel) {
		return preg_match ( "/^[SI]$/i", $privilegeLevel );
	}
	
	public static function firstName(&$firstName) {
		return preg_match ( "/^[A-Z0-9_\-\.'` ]{1,100}$/i", $firstName );
	}

	public static function lastName(&$lastName) {
		return preg_match ( "/^[A-Z0-9_\-\.'` ]{1,100}$/i", $lastName );
	}	
	
	public static function email(&$email) {
		return preg_match ( "/^[A-Z0-9\.]{1,100}@[A-Z0-9\.\-]{1,97}\.[A-Z]{2,3}$/i", $email );
	}
	
	public static function phone(&$phone) {
		return preg_match ( "/^[\d \(\)\-+]{3,50}$/", $phone );
	}
}

?>
