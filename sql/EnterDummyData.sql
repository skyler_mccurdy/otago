﻿INSERT INTO Person (Privilege_Level, User_Name, Password, Salt, First_Name, Last_Name, Email, Phone, Date_Last_Login, Modified_By)
    VALUES ('I', 'tylerondi', 'password', 'salt', 'Tyler', 'Ondricek', 'tyler@tyler.com', '5555555555', NULL, 'tylerondi');

INSERT INTO Person (Privilege_Level, User_Name, Password, Salt, First_Name, Last_Name, Email, Phone, Date_Last_Login, Modified_By)
    VALUES ('S', 'bigal', 'password', 'salt', 'Big', 'Al', 'bigal@gmail.com', '5555555555', NULL, 'tylerondi');

INSERT INTO Casebook (Casebook_Name, Active, Modified_By)
    VALUES ('Child Casebook', TRUE, 'tylerondi');

INSERT INTO User_Casebook (Casebook_Type_ID, Person_ID, Date_Started, Date_Submitted, Date_Due, Final_Grade, Casebook_Status, Modified_By)
    VALUES (1, 2, CURRENT_TIMESTAMP, NULL, NULL, NULL, 'Incomplete', 'tylerondi');

INSERT INTO User_Casebook (Casebook_Type_ID, Person_ID, Date_Started, Date_Submitted, Date_Due, Final_Grade, Casebook_Status, Modified_By)
    VALUES (1, 1, CURRENT_TIMESTAMP, NULL, NULL, NULL, 'Incomplete', 'tylerondi');

INSERT INTO Question_Category (Casebook_Type_ID, Category_Name, Parent_Category_ID, Group_Order, Active, Display_Type, Notes, Modified_By)
    VALUES (1, 'Patient Questions', NULL, 1, TRUE, 'G', NULL, 'tylerondi');

INSERT INTO Question_Category (Casebook_Type_ID, Category_Name, Parent_Category_ID, Group_Order, Active, Display_Type, Notes, Modified_By)
    VALUES (1, 'Patient Medical Questions', 1, 2, TRUE, 'S', 'Please fill out the patient medical information', 'tylerondi');

INSERT INTO Question_Text (Category_ID, Question_Order, Active, Question_Text, Notes, Display_Type, Modified_By)
    VALUES (2, 1, true, 'List allergies if any.', 'Allergies', 'M', 'tylerondi');

INSERT INTO Question_Text (Category_ID, Question_Order, Active, Question_Text, Notes, Display_Type, Modified_By)
    VALUES (2, 2, true, 'What is the patient bloodtype', 'Allergies', '1', 'tylerondi');

INSERT INTO Answer (User_Casebook_ID, Question_ID, Answer_Text, Feedback, Modified_By)
    VALUES (1, 1, 'None', NULL, 'tylerondi');

INSERT INTO Answer (User_Casebook_ID, Question_ID, Answer_Text, Feedback, Modified_By)
    VALUES (1, 2, 'B', NULL, 'tylerondi');

INSERT INTO Answer (User_Casebook_ID, Question_ID, Answer_Text, Feedback, Modified_By)
    VALUES (2, 1, 'None', NULL, 'tylerondi');

INSERT INTO Answer (User_Casebook_ID, Question_ID, Answer_Text, Feedback, Modified_By)
    VALUES (2, 2, 'B', NULL, 'tylerondi');