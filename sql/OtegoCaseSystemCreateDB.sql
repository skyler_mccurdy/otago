﻿CREATE TABLE Person ( 
	Person_ID	bigserial	NOT NULL, 
	Privilege_Level	char		NOT NULL, 
	User_Name	varchar		NOT NULL, 
	Password 	varchar 	NOT NULL,
	Salt		varchar		NOT NULL,
	First_Name	varchar 	NOT NULL,
	Last_Name 	varchar		NOT NULL,
	Email 		varchar 	NOT NULL,
	Phone 		varchar		NOT NULL,
	Date_Last_Login	TIMESTAMP WITH TIME ZONE NULL,
	Modified_By	varchar		NOT NULL,
	Date_Modified TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE TABLE Token (
	Token_ID	bigserial		NOT NULL,
	Token		varchar(40)		NOT NULL,
	Person_ID	bigint			NOT NULL,
	expires TIMESTAMP WITH TIME ZONE NOT NULL);

CREATE TABLE Casebook ( 
	Casebook_Type_ID bigserial	NOT NULL,
	Casebook_Name	 varchar	NOT NULL,
	Active		 bool		NOT NULL,
	Modified_By 	 varchar	NOT NULL,
	Date_Modified TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE TABLE User_Casebook ( 
	User_Casebook_ID bigserial 	NOT NULL, 
	Casebook_Type_ID bigint		NOT NULL,
	Person_ID 	 bigint		NOT NULL,
	Date_Started TIMESTAMP WITH TIME ZONE NOT NULL,
	Date_Submitted TIMESTAMP WITH TIME ZONE NULL,
	Date_due TIMESTAMP WITH TIME ZONE NULL,
	Final_Grade	 char		NULL,
	Casebook_Status	 varchar	NOT NULL,
	Modified_By	 varchar	NOT NULL,
	Date_Modified TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE TABLE Question_Category ( 
	Category_ID	 bigserial	NOT NULL,
	Casebook_Type_ID bigint		NOT NULL,
	Category_Name 	 text		NOT NULL,
	Parent_Category_ID bigint 	NULL,
	Group_Order	 int		NOT NULL,
	Active		 bool		NOT NULL,
	Display_Type	 char		NOT NULL,
	Notes		 varchar	NULL,
	Modified_By	 varchar	NOT NULL,
	Date_Modified TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE TABLE Question_Text ( 
	Question_ID	 bigserial	NOT NULL,
	Category_ID	 bigint		NOT NULL,
	Question_Order	 int		NOT NULL,
	Active		 bool		NOT NULL,
	Question_Text	 varchar	NOT NULL,
	Notes		 varchar	NULL,
	Display_Type	 char		NOT NULL,
	Modified_By 	 varchar 	NOT NULL,
	Date_Modified TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE TABLE Answer ( 
	Answer_ID	 bigserial	NOT NULL,
	User_Casebook_ID bigint		NOT NULL,
	Question_ID	 bigint		NOT NULL,
	Answer_Text	 varchar	NOT NULL,
	Order_By	 int		NULL,
	Feedback 	 varchar	NULL,
	Modified_By 	 varchar	NOT NULL,
	Date_Modified TIMESTAMP WITH TIME ZONE NOT NULL
);

/*Create Primary Key Constraints*/

ALTER TABLE Person
ADD CONSTRAINT PK_Person PRIMARY KEY (Person_ID);

ALTER TABLE Token
ADD CONSTRAINT PK_Token PRIMARY KEY (Token_ID);

ALTER TABLE Casebook
ADD CONSTRAINT PK_Casebook PRIMARY KEY (Casebook_Type_ID);

ALTER TABLE User_Casebook
ADD CONSTRAINT PK_User_Casebook PRIMARY KEY (User_Casebook_ID);

ALTER TABLE Answer
ADD CONSTRAINT PK_Answer PRIMARY KEY (Answer_ID);

ALTER TABLE Question_Category
ADD CONSTRAINT PK_Question_Category PRIMARY KEY (Category_ID);

ALTER TABLE Question_Text
ADD CONSTRAINT PK_Question_Text PRIMARY KEY (Question_ID);

/*Create Foreign Key Constraints*/

ALTER TABLE Token
ADD CONSTRAINT FK_Token FOREIGN KEY (Person_ID) REFERENCES Person (Person_ID);

ALTER TABLE User_Casebook
ADD CONSTRAINT FK_User_Casebook FOREIGN KEY (Casebook_Type_ID) REFERENCES Casebook (Casebook_Type_ID);

ALTER TABLE User_Casebook
ADD CONSTRAINT FK2_User_Casebook FOREIGN KEY (Person_ID) REFERENCES Person (Person_ID);

ALTER TABLE Answer
ADD CONSTRAINT FK_Answer FOREIGN KEY (User_Casebook_ID) REFERENCES User_Casebook (User_Casebook_ID);

ALTER TABLE Question_Category
ADD CONSTRAINT FK_Question_Category FOREIGN KEY (Casebook_Type_ID) REFERENCES Casebook (Casebook_Type_ID);

ALTER TABLE Question_Category
ADD CONSTRAINT FK2_Question_Category FOREIGN KEY (Parent_Category_ID) REFERENCES Question_Category (Category_ID);

ALTER TABLE Question_Text
ADD CONSTRAINT FK_Question_Text FOREIGN KEY (Category_ID) REFERENCES Question_Category (Category_ID);

/*Create Unique Constraints*/

ALTER TABLE Person ADD CONSTRAINT UC_Person UNIQUE (Email);

ALTER TABLE Person ADD CONSTRAINT UC2_Person UNIQUE (User_Name);

ALTER TABLE Token ADD CONSTRAINT UC_Token UNIQUE (Token);

ALTER TABLE Casebook ADD CONSTRAINT UC_Casebook UNIQUE (Casebook_Name);

ALTER TABLE Question_Category ADD CONSTRAINT UC_Question_Category UNIQUE (Category_Name);

ALTER TABLE Question_Text ADD CONSTRAINT UC_Question_Text UNIQUE (Question_Text);

/*Create Check Constraints*/

/*Person Table*/
ALTER TABLE Person
ADD CONSTRAINT CK_Person_Privilege_Level CHECK (Privilege_Level ~* '^[SI]$');

ALTER TABLE Person
ADD CONSTRAINT CK_Person_User_Name CHECK (User_Name ~* '^[A-Za-z0-9_-]{3,50}$');

ALTER TABLE Person
ADD CONSTRAINT CK_Person_First_Name CHECK (First_Name ~* '^[^ -\+/:-@[-^{-~]{1,100}$');

ALTER TABLE Person
ADD CONSTRAINT CK_Person_Last_Name CHECK (Last_Name ~* '^[^ -\+/:-@[-^{-~]{1,100}$');

ALTER TABLE Person
ADD CONSTRAINT CK_Person_Email CHECK (Email ~* '^[^ -\+/:-@[-^{-~]+@[^ -\+/:-@[-^{-~]+[.][A-Za-z]+$');

ALTER TABLE Person
ADD CONSTRAINT CK_Person_Phone CHECK (Phone ~* '^[0-9\(\)\+\.xX -]+$');

ALTER TABLE Person ALTER COLUMN Date_Modified SET DEFAULT CURRENT_TIMESTAMP;

/*Casebook Table*/
ALTER TABLE Casebook
ADD CONSTRAINT CK_Casebook_Casebook_Name CHECK (Casebook_Name ~* '^[^!-\+/:-@[-^{-~]{1,100}$');

ALTER TABLE Casebook ALTER COLUMN Date_Modified SET DEFAULT CURRENT_TIMESTAMP;

/*User_Casebook Table*/
ALTER TABLE User_Casebook ALTER COLUMN Date_Started SET DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE User_Casebook
ADD CONSTRAINT CK_User_Casebook_Final_Grade CHECK (Final_Grade ~* '^[A-Za-z0-9 ]$');

ALTER TABLE User_Casebook
ADD CONSTRAINT CK_User_Casebook_Casebook_Status CHECK (Casebook_Status ~* '^[A-Za-z0-9 _\.-]+$');

ALTER TABLE User_Casebook ALTER COLUMN Date_Modified SET DEFAULT CURRENT_TIMESTAMP;

/*Answer Table*/
ALTER TABLE Answer
ADD CONSTRAINT CK_Answer_Answer_Text CHECK (Answer_Text ~* '^[A-Za-z0-9 _\.\(\)-]+$');

ALTER TABLE Answer
ADD CONSTRAINT CK_Answer_Feedback CHECK (Feedback ~* '^[A-Za-z0-9 _\.\(\)-]+$');

ALTER TABLE Answer ALTER COLUMN Date_Modified SET DEFAULT CURRENT_TIMESTAMP;

/*Question_Category Table*/
ALTER TABLE Question_Category
ADD CONSTRAINT CK_Question_Category_Category_Name CHECK (Category_Name ~* '^[A-Za-z0-9 _\.\(\)-]+$');

ALTER TABLE Question_Category
ADD CONSTRAINT CK_Question_Category_Group_Order CHECK (Group_Order > -1);

ALTER TABLE Question_Category
ADD CONSTRAINT CK_Question_Category_Display_Type CHECK (Display_Type ~* '^[GST]$');

ALTER TABLE Question_Category
ADD CONSTRAINT CK_Question_Category_Notes CHECK (Notes ~* '^[A-Za-z0-9 _\.\(\)-]+$');

ALTER TABLE Question_Category ALTER COLUMN Date_Modified SET DEFAULT CURRENT_TIMESTAMP;

/*Question_Text Table*/
ALTER TABLE Question_Text
ADD CONSTRAINT CK_Question_Text_Question_Order CHECK (Question_Order > -1);

ALTER TABLE Question_Text
ADD CONSTRAINT CK_Question_Text_Question_Text CHECK (Question_Text ~* '^[A-Za-z0-9 _\.\(\)\?-]+$');

ALTER TABLE Question_Text
ADD CONSTRAINT CK_Question_Text_Notes CHECK (Notes ~* '^[A-Za-z0-9 _\.\(\)-]+$');

ALTER TABLE Question_Text
ADD CONSTRAINT CK_Question_Text_Display_Type CHECK (Display_Type ~* '^[1M]$');

ALTER TABLE Question_Text ALTER COLUMN Date_Modified SET DEFAULT CURRENT_TIMESTAMP;