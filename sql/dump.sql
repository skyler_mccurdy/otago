--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'LATIN1';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: answer; Type: TABLE; Schema: public; Owner: ocsAdmin; Tablespace: 
--

CREATE TABLE answer (
    answer_id bigint NOT NULL,
    user_casebook_id bigint NOT NULL,
    question_id bigint NOT NULL,
    answer_text character varying NOT NULL,
    feedback character varying,
    modified_by character varying NOT NULL,
    date_modified timestamp with time zone DEFAULT now() NOT NULL,
    feedback_modified_by character varying(30),
    feedback_modified_on timestamp with time zone,
    CONSTRAINT ck_answer_feedback CHECK (((feedback)::text ~* '^[A-Za-z0-9 _\.\(\)-]+$'::text))
);


ALTER TABLE public.answer OWNER TO "ocsAdmin";

--
-- Name: answer_answer_id_seq; Type: SEQUENCE; Schema: public; Owner: ocsAdmin
--

CREATE SEQUENCE answer_answer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.answer_answer_id_seq OWNER TO "ocsAdmin";

--
-- Name: answer_answer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ocsAdmin
--

ALTER SEQUENCE answer_answer_id_seq OWNED BY answer.answer_id;


--
-- Name: casebook; Type: TABLE; Schema: public; Owner: ocsAdmin; Tablespace: 
--

CREATE TABLE casebook (
    casebook_type_id bigint NOT NULL,
    casebook_name character varying NOT NULL,
    active boolean NOT NULL,
    modified_by character varying NOT NULL,
    date_modified timestamp with time zone DEFAULT now() NOT NULL,
    CONSTRAINT ck_casebook_casebook_name CHECK (((casebook_name)::text ~* '^[A-Za-z0-9 _\.-]+$'::text))
);


ALTER TABLE public.casebook OWNER TO "ocsAdmin";

--
-- Name: casebook_casebook_type_id_seq; Type: SEQUENCE; Schema: public; Owner: ocsAdmin
--

CREATE SEQUENCE casebook_casebook_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.casebook_casebook_type_id_seq OWNER TO "ocsAdmin";

--
-- Name: casebook_casebook_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ocsAdmin
--

ALTER SEQUENCE casebook_casebook_type_id_seq OWNED BY casebook.casebook_type_id;


--
-- Name: person; Type: TABLE; Schema: public; Owner: ocsAdmin; Tablespace: 
--

CREATE TABLE person (
    person_id bigint NOT NULL,
    privilege_level character varying NOT NULL,
    user_name character varying NOT NULL,
    password character varying NOT NULL,
    salt character varying NOT NULL,
    first_name character varying NOT NULL,
    last_name character varying NOT NULL,
    email character varying NOT NULL,
    phone character varying NOT NULL,
    date_last_login timestamp with time zone,
    modified_by character varying NOT NULL,
    date_modified timestamp with time zone DEFAULT now() NOT NULL,
    CONSTRAINT ck_person_email CHECK (((email)::text ~* '^[^ -\+/:-@[-^{-~]+@[^ -\+/:-@[-^{-~]+[.][A-Za-z]+$'::text)),
    CONSTRAINT ck_person_first_name CHECK (((first_name)::text ~* '^[^ -\+/:-@[-^{-~]{1,100}$'::text)),
    CONSTRAINT ck_person_last_name CHECK (((last_name)::text ~* '^[^ -\+/:-@[-^{-~]{1,100}$'::text)),
    CONSTRAINT ck_person_phone CHECK (((phone)::text ~* '^[0-9\(\)\+\.xX -]+$'::text)),
    CONSTRAINT ck_person_privilege_level CHECK (((privilege_level)::text ~* '^[A-Za-z0-9 _\.-]+$'::text)),
    CONSTRAINT ck_person_user_name CHECK (((user_name)::text ~* '^[A-Za-z0-9_-]{3,50}$'::text))
);


ALTER TABLE public.person OWNER TO "ocsAdmin";

--
-- Name: person_person_id_seq; Type: SEQUENCE; Schema: public; Owner: ocsAdmin
--

CREATE SEQUENCE person_person_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.person_person_id_seq OWNER TO "ocsAdmin";

--
-- Name: person_person_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ocsAdmin
--

ALTER SEQUENCE person_person_id_seq OWNED BY person.person_id;


--
-- Name: question_category; Type: TABLE; Schema: public; Owner: ocsAdmin; Tablespace: 
--

CREATE TABLE question_category (
    category_id bigint NOT NULL,
    casebook_type_id bigint NOT NULL,
    category_name text NOT NULL,
    parent_category_id bigint,
    group_order integer NOT NULL,
    active boolean NOT NULL,
    display_type character(1) NOT NULL,
    notes character varying,
    modified_by character varying NOT NULL,
    date_modified timestamp with time zone DEFAULT now() NOT NULL,
    CONSTRAINT ck_question_category_display_type CHECK ((display_type ~* '^[GST]$'::text)),
    CONSTRAINT ck_question_category_group_order CHECK ((group_order > (-1)))
);


ALTER TABLE public.question_category OWNER TO "ocsAdmin";

--
-- Name: question_category_category_id_seq; Type: SEQUENCE; Schema: public; Owner: ocsAdmin
--

CREATE SEQUENCE question_category_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.question_category_category_id_seq OWNER TO "ocsAdmin";

--
-- Name: question_category_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ocsAdmin
--

ALTER SEQUENCE question_category_category_id_seq OWNED BY question_category.category_id;


--
-- Name: question_text; Type: TABLE; Schema: public; Owner: ocsAdmin; Tablespace: 
--

CREATE TABLE question_text (
    question_id bigint NOT NULL,
    category_id bigint NOT NULL,
    question_order integer NOT NULL,
    active boolean NOT NULL,
    question_text character varying NOT NULL,
    notes character varying,
    display_type character(1) NOT NULL,
    modified_by character varying NOT NULL,
    date_modified timestamp with time zone DEFAULT now() NOT NULL,
    CONSTRAINT ck_question_text_question_order CHECK ((question_order > (-1))),
    CONSTRAINT display_type_check CHECK ((display_type ~* '^[1MDC]$'::text))
);


ALTER TABLE public.question_text OWNER TO "ocsAdmin";

--
-- Name: question_text_question_id_seq; Type: SEQUENCE; Schema: public; Owner: ocsAdmin
--

CREATE SEQUENCE question_text_question_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.question_text_question_id_seq OWNER TO "ocsAdmin";

--
-- Name: question_text_question_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ocsAdmin
--

ALTER SEQUENCE question_text_question_id_seq OWNED BY question_text.question_id;


--
-- Name: token; Type: TABLE; Schema: public; Owner: ocsAdmin; Tablespace: 
--

CREATE TABLE token (
    token_id bigint NOT NULL,
    token character varying(40) NOT NULL,
    person_id bigint NOT NULL,
    expires timestamp with time zone NOT NULL
);


ALTER TABLE public.token OWNER TO "ocsAdmin";

--
-- Name: token_token_id_seq; Type: SEQUENCE; Schema: public; Owner: ocsAdmin
--

CREATE SEQUENCE token_token_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.token_token_id_seq OWNER TO "ocsAdmin";

--
-- Name: token_token_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ocsAdmin
--

ALTER SEQUENCE token_token_id_seq OWNED BY token.token_id;


--
-- Name: user_casebook; Type: TABLE; Schema: public; Owner: ocsAdmin; Tablespace: 
--

CREATE TABLE user_casebook (
    user_casebook_id bigint NOT NULL,
    casebook_type_id bigint NOT NULL,
    person_id bigint NOT NULL,
    date_started timestamp without time zone DEFAULT now() NOT NULL,
    date_submitted timestamp without time zone,
    date_due timestamp without time zone,
    final_grade character(1),
    casebook_status character varying NOT NULL,
    modified_by character varying NOT NULL,
    date_modified timestamp with time zone DEFAULT now() NOT NULL,
    student_last_viewed timestamp with time zone,
    instructor_last_viewed timestamp with time zone,
    CONSTRAINT ck_user_casebook_casebook_status CHECK (((casebook_status)::text ~* '^[A-Za-z0-9 _\.-]+$'::text)),
    CONSTRAINT ck_user_casebook_final_grade CHECK ((final_grade ~* '^[A-Za-z0-9 ]$'::text))
);


ALTER TABLE public.user_casebook OWNER TO "ocsAdmin";

--
-- Name: user_casebook_user_casebook_id_seq; Type: SEQUENCE; Schema: public; Owner: ocsAdmin
--

CREATE SEQUENCE user_casebook_user_casebook_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_casebook_user_casebook_id_seq OWNER TO "ocsAdmin";

--
-- Name: user_casebook_user_casebook_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ocsAdmin
--

ALTER SEQUENCE user_casebook_user_casebook_id_seq OWNED BY user_casebook.user_casebook_id;


--
-- Name: answer_id; Type: DEFAULT; Schema: public; Owner: ocsAdmin
--

ALTER TABLE ONLY answer ALTER COLUMN answer_id SET DEFAULT nextval('answer_answer_id_seq'::regclass);


--
-- Name: casebook_type_id; Type: DEFAULT; Schema: public; Owner: ocsAdmin
--

ALTER TABLE ONLY casebook ALTER COLUMN casebook_type_id SET DEFAULT nextval('casebook_casebook_type_id_seq'::regclass);


--
-- Name: person_id; Type: DEFAULT; Schema: public; Owner: ocsAdmin
--

ALTER TABLE ONLY person ALTER COLUMN person_id SET DEFAULT nextval('person_person_id_seq'::regclass);


--
-- Name: category_id; Type: DEFAULT; Schema: public; Owner: ocsAdmin
--

ALTER TABLE ONLY question_category ALTER COLUMN category_id SET DEFAULT nextval('question_category_category_id_seq'::regclass);


--
-- Name: question_id; Type: DEFAULT; Schema: public; Owner: ocsAdmin
--

ALTER TABLE ONLY question_text ALTER COLUMN question_id SET DEFAULT nextval('question_text_question_id_seq'::regclass);


--
-- Name: token_id; Type: DEFAULT; Schema: public; Owner: ocsAdmin
--

ALTER TABLE ONLY token ALTER COLUMN token_id SET DEFAULT nextval('token_token_id_seq'::regclass);


--
-- Name: user_casebook_id; Type: DEFAULT; Schema: public; Owner: ocsAdmin
--

ALTER TABLE ONLY user_casebook ALTER COLUMN user_casebook_id SET DEFAULT nextval('user_casebook_user_casebook_id_seq'::regclass);


--
-- Data for Name: answer; Type: TABLE DATA; Schema: public; Owner: ocsAdmin
--

COPY answer (answer_id, user_casebook_id, question_id, answer_text, feedback, modified_by, date_modified, feedback_modified_by, feedback_modified_on) FROM stdin;
\.


--
-- Name: answer_answer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ocsAdmin
--

SELECT pg_catalog.setval('answer_answer_id_seq', 215, true);


--
-- Data for Name: casebook; Type: TABLE DATA; Schema: public; Owner: ocsAdmin
--

COPY casebook (casebook_type_id, casebook_name, active, modified_by, date_modified) FROM stdin;
\.


--
-- Name: casebook_casebook_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ocsAdmin
--

SELECT pg_catalog.setval('casebook_casebook_type_id_seq', 11, true);


--
-- Data for Name: person; Type: TABLE DATA; Schema: public; Owner: ocsAdmin
--

COPY person (person_id, privilege_level, user_name, password, salt, first_name, last_name, email, phone, date_last_login, modified_by, date_modified) FROM stdin;
4	I	mrbean	f4f075007dd237f9927583c18dbd973d6d0e84c2	ecda630c45b0c0cca1f657245516f01005ce3db8	Rowan	Atkinson	mr@bean.net	(890)567-1234	\N	dsmccurdy	2015-02-11 04:34:03.819552+00
3	S	adent	80956634d4571448dd23a4eae6b8ad5a605ef503	7fec0731b3b28e1309d23893d5919ba27a8be30b	Arthur	Dent	arthur@dent.com	(123)456-7890	2015-02-11 04:29:14.740481+00	dsmccurdy	2015-02-11 04:29:14.740481+00
\.


--
-- Name: person_person_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ocsAdmin
--

SELECT pg_catalog.setval('person_person_id_seq', 7, true);


--
-- Data for Name: question_category; Type: TABLE DATA; Schema: public; Owner: ocsAdmin
--

COPY question_category (category_id, casebook_type_id, category_name, parent_category_id, group_order, active, display_type, notes, modified_by, date_modified) FROM stdin;
\.


--
-- Name: question_category_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ocsAdmin
--

SELECT pg_catalog.setval('question_category_category_id_seq', 43, true);


--
-- Data for Name: question_text; Type: TABLE DATA; Schema: public; Owner: ocsAdmin
--

COPY question_text (question_id, category_id, question_order, active, question_text, notes, display_type, modified_by, date_modified) FROM stdin;
\.


--
-- Name: question_text_question_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ocsAdmin
--

SELECT pg_catalog.setval('question_text_question_id_seq', 28, true);


--
-- Data for Name: token; Type: TABLE DATA; Schema: public; Owner: ocsAdmin
--

COPY token (token_id, token, person_id, expires) FROM stdin;
\.


--
-- Name: token_token_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ocsAdmin
--

SELECT pg_catalog.setval('token_token_id_seq', 114, true);


--
-- Data for Name: user_casebook; Type: TABLE DATA; Schema: public; Owner: ocsAdmin
--

COPY user_casebook (user_casebook_id, casebook_type_id, person_id, date_started, date_submitted, date_due, final_grade, casebook_status, modified_by, date_modified, student_last_viewed, instructor_last_viewed) FROM stdin;
\.


--
-- Name: user_casebook_user_casebook_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ocsAdmin
--

SELECT pg_catalog.setval('user_casebook_user_casebook_id_seq', 13, true);


--
-- Name: pk_answer; Type: CONSTRAINT; Schema: public; Owner: ocsAdmin; Tablespace: 
--

ALTER TABLE ONLY answer
    ADD CONSTRAINT pk_answer PRIMARY KEY (answer_id);


--
-- Name: pk_casebook; Type: CONSTRAINT; Schema: public; Owner: ocsAdmin; Tablespace: 
--

ALTER TABLE ONLY casebook
    ADD CONSTRAINT pk_casebook PRIMARY KEY (casebook_type_id);


--
-- Name: pk_person; Type: CONSTRAINT; Schema: public; Owner: ocsAdmin; Tablespace: 
--

ALTER TABLE ONLY person
    ADD CONSTRAINT pk_person PRIMARY KEY (person_id);


--
-- Name: pk_question_category; Type: CONSTRAINT; Schema: public; Owner: ocsAdmin; Tablespace: 
--

ALTER TABLE ONLY question_category
    ADD CONSTRAINT pk_question_category PRIMARY KEY (category_id);


--
-- Name: pk_question_text; Type: CONSTRAINT; Schema: public; Owner: ocsAdmin; Tablespace: 
--

ALTER TABLE ONLY question_text
    ADD CONSTRAINT pk_question_text PRIMARY KEY (question_id);


--
-- Name: pk_token; Type: CONSTRAINT; Schema: public; Owner: ocsAdmin; Tablespace: 
--

ALTER TABLE ONLY token
    ADD CONSTRAINT pk_token PRIMARY KEY (token_id);


--
-- Name: pk_user_casebook; Type: CONSTRAINT; Schema: public; Owner: ocsAdmin; Tablespace: 
--

ALTER TABLE ONLY user_casebook
    ADD CONSTRAINT pk_user_casebook PRIMARY KEY (user_casebook_id);


--
-- Name: uc2_person; Type: CONSTRAINT; Schema: public; Owner: ocsAdmin; Tablespace: 
--

ALTER TABLE ONLY person
    ADD CONSTRAINT uc2_person UNIQUE (user_name);


--
-- Name: uc_casebook; Type: CONSTRAINT; Schema: public; Owner: ocsAdmin; Tablespace: 
--

ALTER TABLE ONLY casebook
    ADD CONSTRAINT uc_casebook UNIQUE (casebook_name);


--
-- Name: uc_person; Type: CONSTRAINT; Schema: public; Owner: ocsAdmin; Tablespace: 
--

ALTER TABLE ONLY person
    ADD CONSTRAINT uc_person UNIQUE (email);


--
-- Name: uc_token; Type: CONSTRAINT; Schema: public; Owner: ocsAdmin; Tablespace: 
--

ALTER TABLE ONLY token
    ADD CONSTRAINT uc_token UNIQUE (token);


--
-- Name: fk2_question_category; Type: FK CONSTRAINT; Schema: public; Owner: ocsAdmin
--

ALTER TABLE ONLY question_category
    ADD CONSTRAINT fk2_question_category FOREIGN KEY (parent_category_id) REFERENCES question_category(category_id);


--
-- Name: fk2_user_casebook; Type: FK CONSTRAINT; Schema: public; Owner: ocsAdmin
--

ALTER TABLE ONLY user_casebook
    ADD CONSTRAINT fk2_user_casebook FOREIGN KEY (person_id) REFERENCES person(person_id);


--
-- Name: fk_answer; Type: FK CONSTRAINT; Schema: public; Owner: ocsAdmin
--

ALTER TABLE ONLY answer
    ADD CONSTRAINT fk_answer FOREIGN KEY (user_casebook_id) REFERENCES user_casebook(user_casebook_id);


--
-- Name: fk_question_category; Type: FK CONSTRAINT; Schema: public; Owner: ocsAdmin
--

ALTER TABLE ONLY question_category
    ADD CONSTRAINT fk_question_category FOREIGN KEY (casebook_type_id) REFERENCES casebook(casebook_type_id);


--
-- Name: fk_question_text; Type: FK CONSTRAINT; Schema: public; Owner: ocsAdmin
--

ALTER TABLE ONLY question_text
    ADD CONSTRAINT fk_question_text FOREIGN KEY (category_id) REFERENCES question_category(category_id);


--
-- Name: fk_token; Type: FK CONSTRAINT; Schema: public; Owner: ocsAdmin
--

ALTER TABLE ONLY token
    ADD CONSTRAINT fk_token FOREIGN KEY (person_id) REFERENCES person(person_id);


--
-- Name: fk_user_casebook; Type: FK CONSTRAINT; Schema: public; Owner: ocsAdmin
--

ALTER TABLE ONLY user_casebook
    ADD CONSTRAINT fk_user_casebook FOREIGN KEY (casebook_type_id) REFERENCES casebook(casebook_type_id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: answer; Type: ACL; Schema: public; Owner: ocsAdmin
--

REVOKE ALL ON TABLE answer FROM PUBLIC;
REVOKE ALL ON TABLE answer FROM "ocsAdmin";
GRANT ALL ON TABLE answer TO "ocsAdmin";
GRANT ALL ON TABLE answer TO "ocsWeb";


--
-- Name: answer_answer_id_seq; Type: ACL; Schema: public; Owner: ocsAdmin
--

REVOKE ALL ON SEQUENCE answer_answer_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE answer_answer_id_seq FROM "ocsAdmin";
GRANT ALL ON SEQUENCE answer_answer_id_seq TO "ocsAdmin";
GRANT SELECT,USAGE ON SEQUENCE answer_answer_id_seq TO "ocsWeb";


--
-- Name: casebook; Type: ACL; Schema: public; Owner: ocsAdmin
--

REVOKE ALL ON TABLE casebook FROM PUBLIC;
REVOKE ALL ON TABLE casebook FROM "ocsAdmin";
GRANT ALL ON TABLE casebook TO "ocsAdmin";
GRANT ALL ON TABLE casebook TO "ocsWeb";


--
-- Name: casebook_casebook_type_id_seq; Type: ACL; Schema: public; Owner: ocsAdmin
--

REVOKE ALL ON SEQUENCE casebook_casebook_type_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE casebook_casebook_type_id_seq FROM "ocsAdmin";
GRANT ALL ON SEQUENCE casebook_casebook_type_id_seq TO "ocsAdmin";
GRANT SELECT,USAGE ON SEQUENCE casebook_casebook_type_id_seq TO "ocsWeb";


--
-- Name: person; Type: ACL; Schema: public; Owner: ocsAdmin
--

REVOKE ALL ON TABLE person FROM PUBLIC;
REVOKE ALL ON TABLE person FROM "ocsAdmin";
GRANT ALL ON TABLE person TO "ocsAdmin";
GRANT ALL ON TABLE person TO "ocsWeb";


--
-- Name: person_person_id_seq; Type: ACL; Schema: public; Owner: ocsAdmin
--

REVOKE ALL ON SEQUENCE person_person_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE person_person_id_seq FROM "ocsAdmin";
GRANT ALL ON SEQUENCE person_person_id_seq TO "ocsAdmin";
GRANT SELECT,USAGE ON SEQUENCE person_person_id_seq TO "ocsWeb";


--
-- Name: question_category; Type: ACL; Schema: public; Owner: ocsAdmin
--

REVOKE ALL ON TABLE question_category FROM PUBLIC;
REVOKE ALL ON TABLE question_category FROM "ocsAdmin";
GRANT ALL ON TABLE question_category TO "ocsAdmin";
GRANT ALL ON TABLE question_category TO "ocsWeb";


--
-- Name: question_category_category_id_seq; Type: ACL; Schema: public; Owner: ocsAdmin
--

REVOKE ALL ON SEQUENCE question_category_category_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE question_category_category_id_seq FROM "ocsAdmin";
GRANT ALL ON SEQUENCE question_category_category_id_seq TO "ocsAdmin";
GRANT SELECT,USAGE ON SEQUENCE question_category_category_id_seq TO "ocsWeb";


--
-- Name: question_text; Type: ACL; Schema: public; Owner: ocsAdmin
--

REVOKE ALL ON TABLE question_text FROM PUBLIC;
REVOKE ALL ON TABLE question_text FROM "ocsAdmin";
GRANT ALL ON TABLE question_text TO "ocsAdmin";
GRANT ALL ON TABLE question_text TO "ocsWeb";


--
-- Name: question_text_question_id_seq; Type: ACL; Schema: public; Owner: ocsAdmin
--

REVOKE ALL ON SEQUENCE question_text_question_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE question_text_question_id_seq FROM "ocsAdmin";
GRANT ALL ON SEQUENCE question_text_question_id_seq TO "ocsAdmin";
GRANT SELECT,USAGE ON SEQUENCE question_text_question_id_seq TO "ocsWeb";


--
-- Name: token; Type: ACL; Schema: public; Owner: ocsAdmin
--

REVOKE ALL ON TABLE token FROM PUBLIC;
REVOKE ALL ON TABLE token FROM "ocsAdmin";
GRANT ALL ON TABLE token TO "ocsAdmin";
GRANT ALL ON TABLE token TO "ocsWeb";


--
-- Name: token_token_id_seq; Type: ACL; Schema: public; Owner: ocsAdmin
--

REVOKE ALL ON SEQUENCE token_token_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE token_token_id_seq FROM "ocsAdmin";
GRANT ALL ON SEQUENCE token_token_id_seq TO "ocsAdmin";
GRANT SELECT,USAGE ON SEQUENCE token_token_id_seq TO "ocsWeb";


--
-- Name: user_casebook; Type: ACL; Schema: public; Owner: ocsAdmin
--

REVOKE ALL ON TABLE user_casebook FROM PUBLIC;
REVOKE ALL ON TABLE user_casebook FROM "ocsAdmin";
GRANT ALL ON TABLE user_casebook TO "ocsAdmin";
GRANT ALL ON TABLE user_casebook TO "ocsWeb";


--
-- Name: user_casebook_user_casebook_id_seq; Type: ACL; Schema: public; Owner: ocsAdmin
--

REVOKE ALL ON SEQUENCE user_casebook_user_casebook_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE user_casebook_user_casebook_id_seq FROM "ocsAdmin";
GRANT ALL ON SEQUENCE user_casebook_user_casebook_id_seq TO "ocsAdmin";
GRANT SELECT,USAGE ON SEQUENCE user_casebook_user_casebook_id_seq TO "ocsWeb";


--
-- PostgreSQL database dump complete
--

